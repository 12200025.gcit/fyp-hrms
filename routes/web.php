<?php

use App\Events\Test;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AlertController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ShiftController;
use App\Http\Controllers\RosterController;
use App\Http\Controllers\IssuanceController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LeaveTypeController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\FilledFormController;
use App\Http\Controllers\RaisedFormController;
use App\Http\Controllers\FormRequestController;
use App\Http\Controllers\AssignShiftsController;
use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\FormTemplateController;
use App\Http\Controllers\LeaveRequestController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\ObservationNoteController;
use App\Http\Controllers\LeaveApplicationController;

Route::get('/test', function () {
  Test::dispatch();
  return 'success';
});

Route::get('/index', [IndexController::class, 'index']);
Route::get('/login', [AuthController::class, 'create'])->name('login');
Route::post('/login', [AuthController::class, 'store']);
Route::delete('/login', [AuthController::class, 'destroy']);
Route::delete('/logout', [AuthController::class, 'destroy']);
Route::get('/back', [AuthController::class, 'back']);

Route::get('/forgot-password', [ForgotPasswordController::class, 'forgotPasswordShow'])->name('password.request');
Route::post('/forgot-password', [ForgotPasswordController::class, 'sentEmail'])->name('password.email');
Route::get('/reset-password/{token}', [ForgotPasswordController::class, 'resetPasswordShow'])->name('password.reset');
Route::post('/reset-password', [ForgotPasswordController::class, 'resetPassword'])->name('password.update');


//general routes for authorized users
Route::middleware('auth')->group(function () {
  Route::get('/notification', [NotificationController::class, 'index']);
  Route::put('notification/{notification}/read', [NotificationController::class, 'read']);
  Route::put('notification/read-all', [NotificationController::class, 'readAll']);
  Route::get('/alert', [AlertController::class, 'index']);
});

Route::prefix('dashboard')
  ->middleware('auth')
  ->name('dashboard.')
  ->group(function () {
    Route::get('/', [DashboardController::class, 'index']);
  });


Route::delete('/users/{user}', [UserController::class, 'destroy'])->name('users.destroy');

Route::prefix('employee')
  ->name('employee.')
  ->middleware('auth')
  ->group(function () {
    Route::get('/', function () {
      return redirect('/employee/user');
    });
    Route::get('/user/export/', [UserController::class, 'export']);
    Route::get('/user/{user}/download', [UserController::class, 'download']);
    Route::resource('/user', UserController::class);
    Route::delete('/user', [UserController::class, 'destroyMany']);
    Route::put('/user/{user}/startingBalance', [UserController::class, 'updateStartingBalance']);
    Route::get('/user/{user}/observationNote/{observationNote}', [ObservationNoteController::class, 'show']);
    Route::get('/user/{user}/filledForm/{filledForm}', [FilledFormController::class, 'show']);

    Route::resource('/role', RoleController::class);
    Route::delete('/role', [RoleController::class, 'destroyMany']);

    Route::get('/createuser', [IndexController::class, 'employees_createuser']);
    Route::get('/edituser', [IndexController::class, 'employees_edituser']);
    Route::get('/createrole', [IndexController::class, 'employees_createrole']);
    Route::get('/editrole', [IndexController::class, 'employees_editrole']);
  });

Route::prefix('creationhub')
  ->name('creationhub.')
  ->middleware('auth')
  ->group(function () {
    Route::get('/', function () {
      return redirect('/creationhub/leaveType');
    });
    Route::resource('/leaveType', LeaveTypeController::class);
    Route::delete('/leaveType', [LeaveTypeController::class, 'destroyMany']);
    Route::resource('/role', RoleController::class);
    Route::delete('/role', [RoleController::class, 'destroyMany']);

    Route::resource('/formTemplate', FormTemplateController::class);
    Route::delete('/formTemplate', [FormTemplateController::class, 'destroyMany']);


  });

  Route::prefix('department')
  ->name('department.')
  ->middleware('auth')
  ->group(function () {
    Route::get('/', function () {
      return redirect('/department/department');
    });

    Route::resource('/department', DepartmentController::class);
    Route::delete('/department', [DepartmentController::class, 'destroyMany']);
  });

  Route::prefix('leave')
  ->name('leave.')
  ->middleware('auth')
  ->group(function () {
    Route::get('/', function () {
      $user = Auth::user();
      $role = $user->role;

      if ($role->read_leave_type) {
        return redirect('/leave/leaveType');
      } else if ($role->leaveTypes->isNotEmpty() || $user->leaveApplications->isNotEmpty()) {
        return redirect('/leave/leaveApplication');
      } else {
        return redirect('/leave/leaveRequest');
      }
    });
    // Route::resource('/leaveType', LeaveTypeController::class);
    // Route::delete('/leaveType', [LeaveTypeController::class, 'destroyMany']);

    Route::resource('/leaveApplication', LeaveApplicationController::class);
    Route::post('/leaveApplication/{leaveApplication}/doc', [LeaveApplicationController::class, 'uploadDoc']);
    Route::post('/leaveApplication/{leaveApplication}/docApprovals', [LeaveApplicationController::class, 'approveDoc']);

    Route::resource('/leaveRequest', LeaveRequestController::class)->parameters([
      'leaveRequest' => 'leaveApplication'
    ]);
  });

  Route::prefix('myLeave')
  ->name('myLeave.')
  ->middleware('auth')
  ->group(function () {
    Route::get('/', function () {
      return redirect('/myLeave/leaveApplication');
    });
    Route::resource('/leaveApplication', LeaveApplicationController::class);
    Route::post('/leaveApplication/{leaveApplication}/doc', [LeaveApplicationController::class, 'uploadDoc']);
    Route::post('/leaveApplication/{leaveApplication}/docApprovals', [LeaveApplicationController::class, 'approveDoc']);

  });


  Route::prefix('performance')
  ->name('performance.')
  ->middleware('auth')
  ->group(function () {
    Route::get('/', function () {
      $user = Auth::user();
      $role = $user->role;

      if ($role->observer_eligibility) {
        return redirect('/performance/observationNote');
      } else if ($role->performance_eligibility) {
        return redirect('/performance/myPerformance');
      }
    });

    Route::resource('/observationNote', ObservationNoteController::class);
    Route::delete('/observationNote', [ObservationNoteController::class, 'destroyMany']);

    Route::get('/myPerformance', [ObservationNoteController::class, 'indexPerformance']);
    Route::get('/myPerformance/{observationNote}', [ObservationNoteController::class, 'show']);
  });

Route::prefix('department')
  ->name('department.')
  ->middleware('auth')
  ->group(function () {
    Route::get('/', function () {
      return redirect('/department/department');
    });

    Route::resource('/department', DepartmentController::class);
    Route::delete('/department', [DepartmentController::class, 'destroyMany']);
  });


Route::prefix('profile')
  ->name('profile.')
  ->group(function () {
    Route::get('/', function () {
      return redirect('/profile/personalDetails');
    });
    Route::get('/personalDetails', [UserController::class, 'myProfile']);
    Route::get('/changePassword', [IndexController::class, 'profile_changePassword']);
    Route::post('/changeProfilePicture', [UserController::class, 'changeProfilePicture']);
    Route::put('/changePassword', [UserController::class, 'changePassword']);
  });

  Route::prefix('roster')
  ->name('roster.')
  ->middleware('auth')
  ->group(function () {
    Route::get('/', function () {
      $user = Auth::user();
      $role = $user->role;

      if ($role->view_overall_roster || $user->department ? True : False) {
        return redirect('/roster/roster');
      } else if ($role->manage_department_shift) {
        return redirect('/roster/shift');
      } else {
        return redirect('/roster/myRoster');
      }
    });
    Route::resource('/shift', ShiftController::class);
    Route::delete('/shift', [ShiftController::class, 'destroyMany']);

    Route::get('/roster/download', [RosterController::class, 'download']);
    
    Route::resource('/roster', RosterController::class);
    Route::get('/myRoster', [RosterController::class, 'myRoster']);
  });

Route::prefix('attendance')
  ->name('attendance.')
  ->middleware('auth')
  ->group(function () {
    Route::get('/', function () {
      return redirect('/attendance/report');
    });
    Route::get('/report', [AttendanceController::class, 'index']);
  });


  Route::prefix('issuance')
  ->name('issuance.')
  ->middleware('auth')
  ->group(function () {
    Route::get('/', function () {
      return redirect('/issuance/staff');
    });

    Route::resource('/staff', IssuanceController::class)->parameters([
      'staff' => 'user'
    ]);
  });
//index redirect
Route::get('/', function () {
  $user = Auth::user();
  if ($user) {
    return redirect('/dashboard');
  } else {
    return redirect('/login');
  }
});

Route::prefix('form')
  ->name('form.')
  ->middleware('auth')
  ->group(function () {
    Route::get('/', function () {
      $user = Auth::user();
      $role = $user->role;

      if ($user->filledForms->isNotEmpty() || $user->fillables->isNotEmpty()) {
        return redirect('/form/filledForm');
      } else if ($user->filledForms->isNotEmpty()) {
        return redirect('/form/myForm');
      } else if ($user->sharedFilledForms->isNotEmpty()) {
        return redirect('/form/sharedForm');
      } else {
        return redirect('/form/raisedForm');
      }
    });

    Route::resource('/formTemplate', FormTemplateController::class);
    Route::delete('/formTemplate', [FormTemplateController::class, 'destroyMany']);

    Route::resource('/raisedForm', RaisedFormController::class);
    Route::delete('/raisedForm', [RaisedFormController::class, 'destroyMany']);

    Route::resource('/filledForm', FilledFormController::class);
    Route::resource('/formRequest', FormRequestController::class)->parameters([
      'formRequest' => 'filledForm'
    ]);

    Route::get('/sharedForm', [FilledFormController::class, 'sharedIndex']);
    Route::get('/sharedForm/{filledForm}', [FilledFormController::class, 'show']);
    Route::put('/sharedForm/{filledForm}', [FilledFormController::class, 'share']);

    Route::get('/myForm', [FilledFormController::class, 'myForms']);
    Route::get('/myForm/{filledForm}', [FilledFormController::class, 'show']);
  });