<?php

use App\Helper\Helper;
use Illuminate\Support\Facades\Schedule;

// Schedule::call(function () {
//     Helper::sendAlert('Any');
// })->everyTenSeconds();

Schedule::call(function () {
    Helper::sendAlert('Twice a Day');
})->twiceDaily(9, 21);

Schedule::call(function () {
    Helper::sendAlert('Daily');
})->dailyAt('9:00');

Schedule::call(function () {
    Helper::sendAlert('Weekly');
})->weeklyOn(1, '9:00');

Schedule::call(function () {
    Helper::sendAlert('Monthly');
})->monthlyOn(4, '9:00');
