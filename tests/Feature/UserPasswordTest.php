<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;

class UserPasswordTest extends TestCase
{
    use RefreshDatabase;

   public function testChangePasswordSuccessful()
    {
        // Create a user with a known password
        $user = User::factory()->create([
            'password' => 'oldPassword#123'
        ]);

        // Act as the created user
        $this->actingAs($user);

        
        // Send a request to change the password
        $response = $this->put('/profile/changePassword', [
            'oldPassword' => 'oldPassword#123',
            'newPassword' => 'NewPassword#123!',
            'confirmPassword' => 'NewPassword#123!'
        ]);

        // Assert the user is redirected to the correct route with a success message
        $response->assertRedirect('/profile/personalDetails');
        $response->assertSessionHas('success', 'Password updated!');


        // Assert the password was changed
        $this->assertTrue(Hash::check('NewPassword#123!', $user->fresh()->password));
    }

    public function testChangePasswordInvalidOldPassword()
    {
        // Create a user with a known password
        $user = User::factory()->create([
            'password' => 'oldPassword#123'
        ]);

        // Act as the created user
        $this->actingAs($user);

        // Send a request to change the password with an invalid old password
        $response = $this->put('/profile/changePassword', [
            'oldPassword' => 'wrongOldPassword',
            'newPassword' => 'NewPassword#123!',
            'confirmPassword' => 'NewPassword#123!'
        ]);

        // Assert the user is redirected back with an error message
        $response->assertRedirect();
        $response->assertSessionHasErrors(['oldPassword' => 'The provided credentials do not match our records.']);
    }

    public function testChangePasswordConfirmationDoesNotMatch()
    {
        // Create a user with a known password
        $user = User::factory()->create([
            'password' => 'oldPassword#123'
        ]);

        // Act as the created user
        $this->actingAs($user);

        // Send a request to change the password with a mismatched confirmation password
        $response = $this->put('/profile/changePassword', [
            'oldPassword' => 'oldPassword#123',
            'newPassword' => 'NewPassword#123!',
            'confirmPassword' => 'DifferentPassword#123!'
        ]);

        // Assert the user is redirected back with an error message
        $response->assertRedirect();
        $response->assertSessionHasErrors(['confirmPassword']);
    }

    public function testChangePasswordValidationFails()
    {
        // Create a user with a known password
        $user = User::factory()->create([
            'password' => Hash::make('oldPassword#123')
        ]);

        // Act as the created user
        $this->actingAs($user);

        // Send a request to change the password with a new password that does not meet validation rules
        $response = $this->put('/profile/changePassword', [
            'oldPassword' => 'oldPassword#123',
            'newPassword' => 'short',
            'confirmPassword' => 'short'
        ]);

        // Assert the user is redirected back with validation errors
        $response->assertRedirect();
        $response->assertSessionHasErrors(['newPassword']);
    }
}
