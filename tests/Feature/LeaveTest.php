<?php

namespace Tests\Feature;

use App\Models\LeaveType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LeaveTest extends TestCase
{
    use RefreshDatabase;

    public function testApplyLeave()
    {
        // Create a user for testing
        $user = User::factory()->create();
        $leaveType = LeaveType::factory()->create();
        $user->role->leaveTypes()->attach($leaveType->id);

        // Mock request data
        $requestData = [
            'leave_type' => $leaveType,
            'start_date' => '2024-06-01',
            'end_date' => '2024-06-05',
            'description' => 'Vacation',
            'public_holidays' => []
        ];

        // Perform a POST request to the store method
        $response = $this->actingAs($user)->post('/leave/leaveApplication', $requestData);

        // Assert that the request was successful and redirected to the index page
        $response->assertRedirect('/leave/leaveApplication');
    }
}