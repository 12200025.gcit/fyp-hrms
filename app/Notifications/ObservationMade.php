<?php

namespace App\Notifications;

use App\Models\ObservationNote;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ObservationMade extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(private ObservationNote $observationNote)
    {
        $this->observationNote->load('observer.role:id,name');

        $this->afterCommit();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->line("A ({$this->observationNote->type}) Observation is made for you by {$this->observationNote->observer->name} ({$this->observationNote->observer->role->name}).")
            ->action('Click for more info!', url("/performance/myPerformance/{$this->observationNote->id}"))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            'type' => $this->observationNote->type,
            'id' => $this->observationNote->id,
            'observer_name' => $this->observationNote->observer->name,
            'observer_role' => $this->observationNote->observer->role->name
        ];
    }
}
