<?php

namespace App\Notifications;

use App\Models\LeaveApplication;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class LeaveRejected extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(private LeaveApplication $leaveApplication)
    {
        $this->leaveApplication->load('leaveType:id,type');

        $this->afterCommit();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->line("Your {$this->leaveApplication->leaveType->type} application from {$this->leaveApplication->start_date} to {$this->leaveApplication->end_date} is rejected.")
            ->action('Click for more info!', url("/leave/leaveApplication/{$this->leaveApplication->id}"))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            'leave_type' => $this->leaveApplication->leaveType->type,
            'start_date' => $this->leaveApplication->start_date,
            'end_date' => $this->leaveApplication->end_date,
            'leave_application_id' => $this->leaveApplication->id
        ];
    }
}
