<?php

namespace App\Notifications;

use App\Models\FilledForm;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class FormShared extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(private FilledForm $filledForm)
    {
        $this->filledForm->load('raisedForm:id,name', 'user.role:id,name');

        $this->afterCommit();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->line("{$this->filledForm->raisedForm->name} for User ({$this->filledForm->user->role->name}) {$this->filledForm->user->name} is shared with you.")
            ->action('Click for more info!', url("/form/sharedForm/{$this->filledForm->id}"))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            'name' => $this->filledForm->raisedForm->name,
            'user_name' => $this->filledForm->user->name,
            'role_name' => $this->filledForm->user->role->name,
            'id' => $this->filledForm->id
        ];
    }
}
