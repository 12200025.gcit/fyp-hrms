<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class FilledRoleField extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function roleField(): BelongsTo
    {
        return $this->belongsTo(RoleField::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function selectedOptions(): BelongsToMany
    {
        return $this->belongsToMany(Option::class);
    }

    public function file(): MorphOne
    {
        return $this->morphOne(FieldFile::class, 'field_fileable');
    }

    public function alert(): MorphOne
    {
        return $this->morphOne(Alert::class, 'alertable');
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($filledRoleField) {
            $filledRoleField->file?->delete();
            $filledRoleField->alert?->delete();
        });
    }
}
