<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RoleField extends Model
{
    use HasFactory;

    protected $guarded = [];


    public function field(): BelongsTo{
        return $this->belongsTo(Field::class);
    }

    public function role(): BelongsTo{
        return $this->belongsTo(Role::class);
    }


    public function filledRoleFields(): HasMany{
        return $this->hasMany(FilledRoleField::class);
    }

    public function issuers(): BelongsToMany{
        return $this->belongsToMany(User::class, 'issuance_pivot');
    }

    public function alertSubjects(): BelongsToMany{
        return $this->belongsToMany(User::class, 'alert_pivot');
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($roleField) {
            $roleField->filledRoleFields->each(function ($filledRoleField){
                $filledRoleField->delete();
            });
        });
    }
}
