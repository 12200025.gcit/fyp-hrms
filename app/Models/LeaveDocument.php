<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LeaveDocument extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = ['src'];

    public function leaveApplication(): BelongsTo
    {
        return $this->belongsTo(LeaveApplication::class);
    }

    public function getSrcAttribute()
    {
        return asset("storage/{$this->filename}");
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($fieldFile) {
            Storage::disk('public')->delete($fieldFile->filename);
        });
    }
}
