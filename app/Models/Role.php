<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Role extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }

    public function fields(): HasMany
    {
        return $this->hasMany(RoleField::class);
    }

    public function leaveTypes(): BelongsToMany
    {
        return $this->belongsToMany(LeaveType::class);
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($role) {
            $role->users->each(function ($user) {
                $user->delete();
            });
            $role->fields->each(function ($roleField) {
                $roleField->delete();
            });
        });
    }
}
