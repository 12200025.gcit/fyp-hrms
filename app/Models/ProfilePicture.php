<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProfilePicture extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = ['src'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getSrcAttribute()
    {
        return asset("storage/{$this->filename}");
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($profilePicture) {
            Storage::disk('public')->delete($profilePicture->filename);
        });
    }
}
