<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Approval extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function approvalable(): MorphTo
    {
        return $this->morphTo();
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function alert(): MorphOne
    {
        return $this->morphOne(Alert::class, 'alertable');
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($approval) {
            $approval->alert?->delete();
        });
    }
}
