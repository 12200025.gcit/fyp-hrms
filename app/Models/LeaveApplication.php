<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class LeaveApplication extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected function casts(): array
    {
        return [
            'public_holidays' => 'array',
            'doc_approvals' => 'array'
        ];
    }

    public function leaveType(): BelongsTo
    {
        return $this->belongsTo(LeaveType::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function leaveDocument(): HasOne
    {
        return $this->hasOne(LeaveDocument::class);
    }

    public function approvals(): MorphMany
    {
        return $this->morphMany(Approval::class, 'approvalable');
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($leaveApplication) {
            $leaveApplication->leaveDocument?->delete();
            $leaveApplication->alert?->delete();
            $leaveApplication->approvals->each(function ($la) {
                $la->delete();
            });
        });
    }

    public function alert(): MorphOne
    {
        return $this->morphOne(Alert::class, 'alertable');
    }
}
