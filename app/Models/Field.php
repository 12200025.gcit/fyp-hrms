<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Field extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function roleField(): HasOne{
        return $this->hasOne(RoleField::class);
    }

    public function options(): HasMany{
        return $this->hasMany(Option::class);
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($field) {
            $field->roleField->delete();
            $field->options->each(function ($option){
                $option->delete();
            });
        });

        self::updating(function ($field) {
            if($field->isDirty('type')){
                $typesWithOptions = ['Checkbox', 'Radio Button', 'Dropdown'];

                //updating from type with options to without options
                if(in_array($field->getOriginal('type'), $typesWithOptions) && !in_array($field->type, $typesWithOptions)){
                    $field->options->each(function ($option){
                        $option->delete();
                    });     
                } 
                //updating from file type to non file type
                else if($field->getOriginal('type') === 'File'){
                    $field->roleField->filledRoleFields->each(function ($filledRoleField){
                        $filledRoleField->file?->delete();
                    });
                }
            }
        });
    }
}
