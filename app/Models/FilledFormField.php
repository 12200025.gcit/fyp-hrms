<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class FilledFormField extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function formField(): BelongsTo
    {
        return $this->belongsTo(FormField::class);
    }

    public function filledFormField(): BelongsTo
    {
        return $this->belongsTo(FilledFormField::class);
    }

    public function file(): MorphOne
    {
        return $this->morphOne(FieldFile::class, 'field_fileable');
    }

    public function selectedOptions(): BelongsToMany
    {
        return $this->belongsToMany(Option::class);
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($filledFormField) {
            $filledFormField->file?->delete();
        });
    }
}
