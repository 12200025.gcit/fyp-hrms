<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Option extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function field(): BelongsTo
    {
        return $this->belongsTo(Field::class);
    }

    public function filledRoleFields(): BelongsToMany
    {
        return $this->belongsToMany(Option::class);
    }

    public function filledFormFields(): BelongsToMany
    {
        return $this->belongsToMany(Option::class);
    }

    public static function boot()
    {
        parent::boot();
    }
}
