<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FormTemplate extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function raisedForms(): HasMany
    {
        return $this->hasMany(RaisedForm::class);
    }

    public function formFields(): HasMany
    {
        return $this->hasMany(FormField::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($formTemplate) {
            $formTemplate->formFields->each(function ($formField) {
                $formField->delete();
            });

            $formTemplate->raisedForms->each(function ($raisedForm) {
                $raisedForm->delete();
            });
        });
    }
}
