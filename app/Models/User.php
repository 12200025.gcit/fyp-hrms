<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $guarded = [];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
            'carry_forward_starting_balances' => 'array'
        ];
    }

    public function profilePicture(): HasOne
    {
        return $this->hasOne(ProfilePicture::class);
    }

    public function department(): HasOne
    {
        return $this->hasOne(Department::class, 'hod_id');
    }

    public function departmentStaffDetail(): HasOne
    {
        return $this->hasOne(DepartmentStaffDetail::class);
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    public function issuables(): BelongsToMany
    {
        return $this->belongsToMany(RoleField::class, 'issuance_pivot');
    }

    public function alertables(): BelongsToMany
    {
        return $this->belongsToMany(RoleField::class, 'alert_pivot');
    }

    public function filledRoleFields(): HasMany
    {
        return $this->hasMany(FilledRoleField::class);
    }

    public function leaveApplications(): HasMany
    {
        return $this->hasMany(leaveApplication::class);
    }

    public function approvalStages(): HasMany
    {
        return $this->hasMany(ApprovalStage::class);
    }

    public function approvals(): HasMany
    {
        return $this->hasMany(Approval::class);
    }

    public function leaveCredits(): HasMany
    {
        return $this->hasMany(LeaveCredit::class);
    }

    public function alerts(): BelongsToMany
    {
        return $this->belongsToMany(Alert::class);
    }

    public function fillables(): HasMany
    {
        return $this->hasMany(RaisedForm::class);
    }

    public function raisedForms(): BelongsToMany
    {
        return $this->belongsToMany(RaisedForm::class);
    }

    public function formTemplates(): BelongsToMany
    {
        return $this->belongsToMany(FormTemplate::class);
    }

    public function filledForms(): HasMany
    {
        return $this->hasMany(FilledForm::class);
    }

    public function sharedFilledForms(): BelongsToMany
    {
        return $this->belongsToMany(FilledForm::class);
    }

    public function observationNotesAsSubject(): HasMany
    {
        return $this->hasMany(ObservationNote::class, 'subject_id');
    }

    public function observationNotesAsObserver(): HasMany
    {
        return $this->hasMany(ObservationNote::class, 'observer_id');
    }

    public function sharedObservationNotes(): BelongsToMany
    {
        return $this->belongsToMany(ObservationNote::class);
    }

    public function scopeByName(Builder $query, string $name): void
    {
        if ($name) {
            $query->where('name', 'ilike', "%$name%");
        }
    }

    public function scopeByDepartment(Builder $query, string $department): void
    {
        if ($department !== 'All Department') {
            $query->whereRelation('departmentStaffDetail', 'department_id', $department);
        }
    }

    public function scopeSortBy(Builder $query, string $sortBy): void
    {
        if ($sortBy) {
            if ($sortBy === 'nameAsc') {
                $query->orderBy('name', 'asc');
            } else if ($sortBy === 'nameDesc') {
                $query->orderBy('name', 'desc');
            } else if ($sortBy === 'dojAsc') {
                $query->orderBy('doj', 'asc');
            } else if ($sortBy === 'dojDesc') {
                $query->orderBy('doj', 'desc');
            }
        } else {
            $query->orderBy('updated_at', 'desc');
        }
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($user) {
            $user->filledRoleFields->each(function ($filledRoleField) {
                $filledRoleField->delete();
            });

            $user->profilePicture?->delete();

            $user->leaveApplications->each(function ($leaveApplication) {
                $leaveApplication->delete();
            });
        });
    }
}
