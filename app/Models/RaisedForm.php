<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class RaisedForm extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function formTemplate(): BelongsTo
    {
        return $this->belongsTo(FormTemplate::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function notificationSubjects(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'raised_form_user_notifcation_pivot');
    }

    public function filledForms(): HasMany
    {
        return $this->hasMany(FilledForm::class);
    }

    public function approvalStages(): MorphMany
    {
        return $this->morphMany(ApprovalStage::class, 'approval_stageable');
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($raisedForm) {
            $raisedForm->filledForms->each(function ($filledForm) {
                $filledForm->delete();
            });
        });
    }
}
