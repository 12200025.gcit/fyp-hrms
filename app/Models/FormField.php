<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FormField extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function filledFormFields(): HasMany
    {
        return $this->hasMany(FilledFormField::class);
    }

    public function field(): BelongsTo
    {
        return $this->belongsTo(Field::class);
    }

    public function formTemplate(): BelongsTo
    {
        return $this->belongsTo(FormTemplate::class);
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($formField) {
            $formField->filledFormFields->each(function ($frf) {
                $frf->delete();
            });
        });
    }
}
