<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class LeaveType extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected function casts(): array
    {
        return [
            'public_holidays' => 'array',
        ];
    }

    public function leaveApplications(): HasMany
    {
        return $this->hasMany(LeaveApplication::class);
    }

    public function approvalStages(): MorphMany
    {
        return $this->morphMany(ApprovalStage::class, 'approval_stageable');
    }

    public function leaveCredits(): HasMany
    {
        return $this->hasMany(LeaveCredit::class);
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($leaveType) {
            $leaveType->leaveApplications->each(function ($leaveApplication) {
                $leaveApplication->delete();
            });
        });
    }
}
