<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class FilledForm extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function filledFormFields(): HasMany
    {
        return $this->hasMany(FilledFormField::class);
    }

    public function raisedForm(): BelongsTo
    {
        return $this->belongsTo(RaisedForm::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function sharedUsers(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function alert(): MorphOne
    {
        return $this->morphOne(Alert::class, 'alertable');
    }

    public function approvals(): MorphMany
    {
        return $this->morphMany(Approval::class, 'approvalable');
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($filledForm) {
            $filledForm->alert?->delete();
            $filledForm->filledFormFields->each(function ($frf) {
                $frf->delete();
            });
        });
    }
}
