<?php

namespace App\Models;

use App\Models\AssignedShifts;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Department extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function hod(): BelongsTo
    {
        return $this->belongsTo(User::class, 'hod_id');
    }

    public function departmentStaffDetails(): HasMany
    {
        return $this->hasMany(DepartmentStaffDetail::class);
    }

    public function assignedShifts(): HasMany
    {
        return $this->hasMany(AssignedShift::class);
    }
}
