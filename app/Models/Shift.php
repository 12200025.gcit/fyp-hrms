<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Shift extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class);
    }

    public function assignedShifts(): HasMany
    {
        return $this->hasMany(AssignedShift::class);
    }

    public function scopeByName(Builder $query, string $name): void
    {
        if ($name) {
            $query->where('name', 'ilike', '%' . $name . '%');
        }
    }

    public function scopeByDepartment(Builder $query, string $department): void
    {
        if ($department !== 'All Department') {
            $query->whereRelation('department', 'id', $department);
        }
    }
}
