<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FieldFile extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = ['src'];

    public function fieldFileable(): MorphTo
    {
        return $this->morphTo();
    }

    public function getSrcAttribute()
    {
        return asset("storage/{$this->filename}");
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($fieldFile) {
            Storage::disk('public')->delete($fieldFile->filename);
        });
    }
}
