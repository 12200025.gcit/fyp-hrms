<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Middleware;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     */
    public function version(Request $request): ?string
    {
        return parent::version($request);
    }

    /**
     * Define the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     *
     * @return array<string, mixed>
     */
    public function share(Request $request): array
    {
        $alertCount = 0;
        if ($request->user()) {
            $alertCount = $request->user()->alerts()->count();
            $request->user()->alertables->each(function ($roleField) use (&$alertCount) {
                $alertCount += $roleField->filledRoleFields()->has('alert')->count();
            });
        }

        return array_merge(parent::share($request), [
            'flash' => [
                'success' => $request->session()->get('success')
            ],
            'currentUser' => $request->user() ? [
                'id' => $request->user()->id,
                'name' => $request->user()->name,
                'email' => $request->user()->email,
                'role' => $request->user()->role,
                'head_of' => $request->user()->department?->id ?? null,
                'issue_staff' => $request->user()->issuables->isNotEmpty(),
                'has_shared_forms' => $request->user()->sharedFilledForms->isNotEmpty(),
                'has_forms' => $request->user()->filledForms->isNotEmpty(),
                'can_fill_forms' => $request->user()->filledForms->isNotEmpty() || $request->user()->fillables->isNotEmpty(),
                'can_raise_forms' => $request->user()->formTemplates->isNotEmpty(),
                'can_view_employees' => $request->user()->role->read_user || $request->user()->department ? True : False,
                'can_view_roster' =>  $request->user()->role->view_overall_roster || $request->user()->department ? True : False,
                'can_approve_leave' => $request->user()->approvalStages->isNotEmpty() || $request->user()->department ? True : False,
                'can_apply_leave' => $request->user()->role->leaveTypes->isNotEmpty(),
                'view_my_leaves' => $request->user()->role->leaveTypes->isNotEmpty() || $request->user()->leaveApplications->isNotEmpty(),
                'pfp_src' => $request->user()->profilePicture ? $request->user()->profilePicture->src : null,
                'unread_notification_count' => $request->user()->unreadNotifications()->count(),
                'alert_count' => $alertCount,
                'sharedAt' => Carbon::now()->toDateTimeString()
            ] : null
        ]);
    }
}
