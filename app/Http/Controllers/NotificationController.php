<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Gate;

class NotificationController extends Controller
{
    public function index(Request $request)
    {
        return inertia('Others/Notification', [
            'notifications' => $request->user()
                ->notifications()->paginate(10)
        ]);
    }

    public function read(DatabaseNotification $notification)
    {
        // Gate::authorize('read', $notification);

        $notification->markAsRead();

        return redirect()
            ->back()->with('success', 'Notification marked as read');
    }

    public function readAll(Request $request)
    {
        $request->user()->unreadNotifications->markAsRead();

        return redirect('/notification')->with('success', 'All new notifications marked as read');
    }
}
