<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Models\Alert as ModelsAlert;
use App\Models\LeaveApplication;
use App\Models\Approval;
use App\Models\LeaveDocument;
use App\Models\LeaveType;
use App\Notifications\Alert;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\File;

class LeaveApplicationController extends Controller
{
    use HasFactory;

    protected $guarded = [];
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        Gate::authorize('viewAny', LeaveApplication::class);

        return inertia('Leave/MyLeaves', [
            'leaveApplications' => $request->user()
                ->leaveApplications()
                ->with('leaveType:id,type')
                ->orderBy('updated_at', 'desc')
                ->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        Gate::authorize('create', LeaveApplication::class);

        $user = $request->user();
        $user->balances = Helper::getLeaveBalance($user);

        return inertia('Leave/ApplyForLeave', [
            'user' => $user,
            'leaveTypes' => $request->user()->role->leaveTypes()->with('approvalStages.user.role:id,name')->get(),
            'hod' => $request->user()->departmentStaffDetail?->department?->hod
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Gate::authorize('store', [LeaveApplication::class, $request->all()['leave_type']['id']]);

        $year = Carbon::now()->year;

        $phs = [];
        $user = $request->user();
        $all = $request->all();
        if ($all['leave_type']['balance_type'] === 'Public Holiday') {
            $user
                ->leaveApplications()
                ->where('approval_status', 'Approved')
                ->whereRelation('leaveType', 'balance_type', 'Public Holiday')
                ->whereYear('created_at', $year)->get()->each(function ($la) use (&$phs) {
                    $phs = [...$phs, ...array_map(fn ($laph) => $laph['name'], $la->public_holidays)];
                });
        }

        $validated = $request->validate([
            'leave_type' => 'required',
            'start_date' => 'required|date|nullable',
            'end_date' => 'required|date|after_or_equal:start_date|nullable',
            'description' => 'required',
            'public_holidays' => 'required_if:leave_type.balance_type,Public Holiday|array',
            'public_holidays.*.name' => 'required|string|not_in:' . implode(",", $phs),
            'public_holidays.*.date' => 'required|date',
        ], [
            'public_holidays.*.name' => "You have used :input for this year!"
        ]);

        if ($all['leave_type']['balance_type'] !== 'Unlimited') {
            $sdt = new DateTime($all['start_date']);
            $edt = new DateTime($all['end_date']);
            $days = $edt->diff($sdt)->days + 1;
            $calBalance = Helper::getLeaveBalance($user, $all['leave_type']['id'])->first()['balance'] - $days;
            if ($calBalance < 0) {
                return redirect()->back()->withErrors(['leave_type' => "Not enough leave balance!"]);
            }
        }

        DB::transaction(function () use ($request, $validated) {
            $leaveType = LeaveType::find($validated['leave_type']['id']);
            $approvalStages = $leaveType->approvalStages->sortBy('stage');
            $dsd = $request->user()->departmentStaffDetail;
            $hod = $dsd?->department?->hod;

            $leaveApplication = $request->user()->leaveApplications()->save(new LeaveApplication(
                [
                    ...$request->except('leave_type'),
                    "requires_doc" => $validated['leave_type']['requires_doc'],
                    "leave_type_id" => $validated['leave_type']['id'],
                    "approval_status" => $approvalStages->isNotEmpty() ? "Pending" : "Approved",
                    "current_stage" => $leaveType->requires_hod_approval ? ($hod ? 0 : $approvalStages->first()?->stage ?? null) : $approvalStages->first()?->stage ?? null
                ]
            ));

            //create leave approvals & alert
            if ($approvalStages->isNotEmpty()) {
                $firstStage = $approvalStages->first()->stage;

                $user = $leaveApplication->user;
                $message = "A {$leaveApplication->leaveType->type} Application applied by ({$user->role->name}) {$user->name} is ready for approval.";
                $action_url = "/leave/leaveRequest/{$leaveApplication->id}";

                $approvalStages->each(function ($approvalStage) use ($leaveApplication, $message, $action_url, $firstStage, $request, $leaveType, $hod) {
                    $approval = new Approval(["user_id" => $approvalStage->user_id, "status" => "Pending", "stage" => $approvalStage->stage]);
                    $leaveApplication->approvals()->save($approval);

                    if ((!$leaveType->requires_hod_approval && $approvalStage->stage === $firstStage) || ($leaveType->requires_hod_approval && !$hod && $approvalStage->stage === $firstStage)) {
                        $user = $approvalStage->user;
                        if ($request->user()->id !== $user->id) {
                            $user->notify(new Alert($message, $action_url, true));
                        }

                        $alert = new ModelsAlert(["alert_period" => "Twice a Day", "message" => $message, "action_url" => $action_url]);
                        $approval->alert()->save($alert);
                        $user->alerts()->attach($alert->id);
                    }
                });
            }

            //hod approval
            if ($leaveType->requires_hod_approval) {
                //has hod
                if ($hod) {
                    $approval = new Approval(["user_id" => $hod->id, "status" => "Pending", "stage" => 0]);
                    $leaveApplication->approvals()->save($approval);
                    $message = "A {$leaveApplication->leaveType->type} Application applied by ({$user->role->name}) {$user->name} is ready for approval.";
                    $action_url = "/leave/leaveRequest/{$leaveApplication->id}";

                    if ($request->user()->id !== $hod->id) {
                        $hod->notify(new Alert($message, $action_url, true));
                    }

                    $alert = new ModelsAlert(["alert_period" => "Twice a Day", "message" => $message, "action_url" => $action_url]);
                    $approval->alert()->save($alert);
                    $hod->alerts()->attach($alert->id);
                }
            }

            //doc alert
            if ($validated['leave_type']['requires_doc']) {
                $message = "The {$leaveApplication->leaveType->type} Application applied by ({$user->role->name}) {$user->name} needs document and approval.";
                $action_url = "/leave/leaveApplication/{$leaveApplication->id}";

                $alert = new ModelsAlert(["alert_period" => "Weekly", "message" => $message, "action_url" => $action_url]);
                $leaveApplication->alert()->save($alert);
                $alert->users()->sync([$user->id, ...$leaveApplication->approvals->map(fn ($la) => $la->user_id)]);
            }                                                                                                                                         
        });

        return redirect()->route('leave.leaveApplication.index')->with('success', 'Leave applied successfuly!');
    }

    /**
     * Display the specified resource.
     */
    public function show(LeaveApplication $leaveApplication)
    {
        Gate::authorize('view', $leaveApplication);

        $leaveApplication->load('user', 'leaveType:id,type,balance_type', 'approvals.user.role:id,name', 'leaveDocument');

        return inertia('Leave/LeaveApplication', [
            'leaveApplication' => $leaveApplication
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(LeaveApplication $leaveApplication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, LeaveApplication $leaveApplication)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(LeaveApplication $leaveApplication)
    {
        //
    }

    public function uploadDoc(Request $request, LeaveApplication $leaveApplication)
    {
        $validated = $request->validate([
            'file' => ['nullable', File::image()
                ->max('5mb')]
        ]);

        DB::transaction(function () use ($validated, $leaveApplication) {
            if ($validated['file']) {
                $leaveApplication->leaveDocument?->delete();
                $path = $validated['file']->store('leaveApplicationDocs', 'public');
                $leaveApplication->leaveDocument()->save(new LeaveDocument(['filename' => $path]));
            }
        });

        return redirect()->route('leave.leaveApplication.index')->with('success', 'Leave document updated successfuly!');
    }

    public function approveDoc(Request $request, LeaveApplication $leaveApplication)
    {

        $validated = $request->validate([
            'doc_approvals' => 'array'
        ]);

        DB::transaction(function () use ($validated, $leaveApplication) {
            $leaveApplication->update($validated);

            $approved = true;
            foreach ($validated['doc_approvals'] as $da) {
                if ($da['approval'] === false) {
                    $approved = false;
                }
            }

            if ($approved) {
                $leaveApplication->alert()->delete();
            }
        });

        return redirect('/alert')->with('success', 'Leave document approval updated successfuly!');
    }
}
