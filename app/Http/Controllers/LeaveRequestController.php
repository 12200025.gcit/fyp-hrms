<?php

namespace App\Http\Controllers;

use App\Models\Alert as ModelsAlert;
use App\Models\LeaveApplication;
use App\Models\Approval;
use App\Models\LeaveType;
use App\Notifications\Alert;
use App\Notifications\LeaveApproved;
use App\Notifications\LeaveRejected;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class LeaveRequestController extends Controller
{
    use HasFactory;

    protected $guarded = [];
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        Gate::authorize('viewLeaveRequests', LeaveApplication::class);

        return inertia('Leave/LeaveRequests', [


            'approvals' => $request->user()
                ->approvals()
                ->where('approvalable_type', 'App\Models\LeaveApplication')
                ->leftJoin('leave_applications', 'approvals.approvalable_id', '=', 'leave_applications.id')
                ->where('leave_applications.approval_status', 'Pending')
                ->whereColumn('leave_applications.current_stage', 'approvals.stage')
                ->with('user:id,name,role_id', 'user.role:id,name', 'approvalable.leaveType:id,type')
                ->orderBy('leave_applications.updated_at', 'desc')
                ->paginate(10)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(LeaveApplication $leaveApplication)
    {
        Gate::authorize('approveLeave', $leaveApplication);

        $leaveApplication->load('user.role:id,name', 'user.departmentStaffDetail.department:id,name', 'leaveType:id,type', 'approvals.user.role:id,name');

        return inertia('Leave/LeaveApplication', [
            'leaveApplication' => $leaveApplication,
            'approvingLeave' => True
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(LeaveApplication $leaveApplication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, LeaveApplication $leaveApplication)
    {
        Gate::authorize('approveLeave', $leaveApplication);

        $validated = $request->validate([
            'status' => 'required|in:Approved,Rejected',
            'remarks' => 'nullable'
        ]);

        DB::transaction(function () use ($request, $validated, $leaveApplication) {
            $user = $request->user();
            $approvals = $leaveApplication->approvals->sortBy('stage');

            $approval = $approvals->where('status', 'Pending')->where('user_id', $user->id)->first();
            $approval->update($validated);
            //action taken
            $approval->alert()?->delete();

            //update application status
            if ($validated['status'] === 'Rejected') {
                $leaveApplication->approval_status = 'Rejected';
                $leaveApplication->alert?->delete();

                $leaveApplication->user->notify(new LeaveRejected($leaveApplication));
            } else if ($approvals->where('status', '!=', 'Approved')->isEmpty()) {
                $leaveApplication->approval_status = 'Approved';

                $leaveApplication->user->notify(new LeaveApproved($leaveApplication));
            }


            //update current stage if approved
            if ($validated['status'] === 'Approved') {
                $la = $approvals->where('status', 'Pending')->first();
                if ($la && $la->stage > $leaveApplication->current_stage) {
                    $leaveApplication->current_stage = $la->stage;
                    $message = "A {$leaveApplication->leaveType->type} Application applied by ({$user->role->name}) {$user->name} is ready for approval.";
                    $action_url = "/leave/leaveRequest/{$leaveApplication->id}";

                    $approvals->where('stage', $la->stage)->where('status', 'Pending')->each(function ($approval) use ($message, $action_url, $request) {
                        $user = $approval->user;
                        if ($user) {
                            if ($request->user()->id !== $user->id) {
                                $user->notify(new Alert($message, $action_url, true));
                            }

                            $alert = new ModelsAlert(["alert_period" => "Twice a Day", "message" => $message, "action_url" => $action_url]);
                            $approval->alert()->save($alert);
                            $user->alerts()->attach($alert->id);
                        }
                    });
                }
            }

            $leaveApplication->save();
        });

        return redirect()->route('leave.leaveRequest.index')->with('success', 'Leave request action taken successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(LeaveApplication $leaveApplication)
    {
        //
    }
}
