<?php

namespace App\Http\Controllers;
use App\Exports\UsersExport;
use App\Helper\Helper;
use App\Models\Alert;
use App\Models\Department;
use App\Models\DepartmentStaffDetail;
use App\Models\Role;
use App\Models\User;
use App\Models\FieldFile;
use Illuminate\Http\Request;
use App\Models\ProfilePicture;
use App\Models\FilledRoleField;
use App\Notifications\UserDeleted;
use App\Notifications\UserRegistered;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\Rules\File;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        Gate::authorize('viewAny', User::class);

        $type = null;
        $user = User::find($request->user()->id);
        if ($user->role->read_user) {
            $type = 'All';
        } else {
            $type = 'Department';
        }

        $deparment_id = $user->department?->id;
        $filters = $request->all();
        return inertia('Employee/Users', [
            'users' => $type === 'All' ? User::with('role:id,name', 'departmentStaffDetail.department:id,name', 'department:id,name,hod_id')
                ->byName($filters['name'] ?? '')
                ->byDepartment($filters['department'] ?? 'All Department')
                ->sortBy($filters['sortBy'] ?? '')
                ->paginate(10) :
                User::with('role:id,name', 'departmentStaffDetail.department:id,name', 'department:id,name,hod_id')
                ->whereRelation('departmentStaffDetail', 'department_id', $deparment_id)
                ->byName($filters['name'] ?? '')
                ->sortBy($filters['sortBy'] ?? '')
                ->paginate(10)
                ->withQueryString(),
            'departments' => Department::all(),
            'filters' => $filters,
            'type' => $type
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        Gate::authorize('create', User::class);

        return inertia('Employee/CreateUser', [
            'roles' => Role::with('fields', 'fields.field', 'fields.field.options')->select('id', 'name')->get(),
            'departments' => Department::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Gate::authorize('create', User::class);

        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:App\Models\User,email',
            'gender' => 'required|in:Male,Female',
            'dob' => 'required|date',
            'cid' => 'required|integer|unique:App\Models\User,cid',
            'nationality' => 'required|in:Bhutanese,Foreigner',
            'foreign_nationality' => 'required_if:nationality,Foreigner',
            'doj' => 'required|date',
            'role_id' => 'required|exists:roles,id',
            'department' => 'required',
            'designation' => 'required_unless:department,No Department',
            'filled_role_fields' => 'array'
        ]);

        //validating role fields with a validator
        $requiredRoleFields = array_filter($validated['filled_role_fields'], function ($filledRoleField) {
            return $filledRoleField['roleField']['field']['required'];
        });
        $newRequiredRoleFields = [];
        $requiredRoleFieldsValidator = [];
        $validationMessages = [];
        foreach ($requiredRoleFields as $filledRoleField) {
            // dd($filledRoleField);
            if ((($validated['nationality'] === 'Bhutanese' && !$filledRoleField['roleField']['foreigner_field']) || $validated['nationality'] === 'Foreigner') && $filledRoleField['roleField']['field']['type'] !== 'Title' && $filledRoleField['roleField']['field']['type'] !== 'Description') {
                $newId = $filledRoleField['roleField']['id'] . "id";
                $newRequiredRoleFields[$newId] = $filledRoleField['value'];

                if ($filledRoleField['roleField']['field']['type'] !== 'File') {
                    $requiredRoleFieldsValidator[$newId] = 'required';
                    $validationMessages[$newId . '.required'] = 'The ' . $filledRoleField['roleField']['field']['name'] . ' field is required!';
                } else {
                    $requiredRoleFieldsValidator[$newId] = ['required', File::types(['jpg', 'jpeg', 'png', 'bmp', 'webp', 'pdf', 'docx', 'doc'])
                        ->max('5mb')];
                    $validationMessages[$newId . '.required'] = 'The ' . $filledRoleField['roleField']['field']['name'] . ' field is required!';
                    $validationMessages[$newId . '.mimes'] = 'The ' . $filledRoleField['roleField']['field']['name'] . ' field must be a file of type: jpg, jpeg, png, bmp, webp, pdf, docx, doc.';
                    $validationMessages[$newId . '.max'] = 'The ' . $filledRoleField['roleField']['field']['name'] . ' field must not be greater than 5000 kilobytes.';
                }
            }
        }
        $roleFieldValidator = Validator::make($newRequiredRoleFields, $requiredRoleFieldsValidator, $validationMessages);
        if ($roleFieldValidator->fails()) {
            return back()
                ->withErrors($roleFieldValidator)
                ->withInput();
        }

        DB::transaction(function () use ($request, $validated) {
            $user = User::create([...$request->except(['filled_role_fields', 'department', 'designation']), 'password' => $validated['cid'], 'carry_forward_starting_balances' => []]);
            if ($validated['department'] !== 'No Department') {
                $user->departmentStaffDetail()->save(new DepartmentStaffDetail(['designation' => $validated['designation'], 'department_id' => $validated['department']]));
            }

            $role = $user->role;
            foreach ($validated['filled_role_fields'] as $filledRoleField) {
                if ($filledRoleField['roleField']['field']['type'] === 'Title' || $filledRoleField['roleField']['field']['type'] === 'Description') {
                    continue;
                }

                $frf = null;
                if ($filledRoleField['roleField']['field']['type'] === 'Checkbox') {
                    $frf = FilledRoleField::create(['role_field_id' => $filledRoleField['roleField']['id'], 'user_id' => $user->id]);
                    $frf->selectedOptions()->sync($filledRoleField['value']);
                } else if ($filledRoleField['roleField']['field']['type'] === 'File' && $filledRoleField['value']) {
                    $frf = FilledRoleField::create(['role_field_id' => $filledRoleField['roleField']['id'], 'user_id' => $user->id]);
                    $path = $filledRoleField['value']->store('fieldFiles', 'public');
                    $frf->file()->save(new FieldFile(['filename' => $path]));
                } else if ($filledRoleField['roleField']['field']['type'] === 'Date') {
                    $frf = FilledRoleField::create(['date_value' => $filledRoleField['value'], 'role_field_id' => $filledRoleField['roleField']['id'], 'user_id' => $user->id]);
                } else {
                    $frf = FilledRoleField::create(['value' => $filledRoleField['value'], 'role_field_id' => $filledRoleField['roleField']['id'], 'user_id' => $user->id]);
                }

                //alert
                $fn = $filledRoleField['roleField']['field']['name'];
                if ($filledRoleField['roleField']['alertable'] && !$filledRoleField['value']) {
                    $alert = new Alert(["alert_period" => "Daily", "message" => "({$role->name}) {$user->name}'s {$fn} is missing!", "action_url" => "/employee/user/{$user->id}/edit"]);
                    $frf->alert()->save($alert);
                }
            }

            //notify users who can view/read other users
            $users = User::whereRelation('role', 'read_user', true)->get()->except(Auth::id());
            Notification::send($users, new UserRegistered($user));
        });

        return redirect()->route('employee.user.index')->with('success', 'User created successfuly!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, User $user)
    {
        $now = Carbon::now();

        $page = $request->all()['page'] ?? 'Profile';
        $page = in_array($page, ['Profile', 'Relative', 'Role', 'Balance', 'Document', 'Form', 'Performance']) ? $page : 'Profile';

        if ($page === 'Profile') {
            $user->load('role', 'profilePicture', 'departmentStaffDetail.department', 'department');
        } else if ($page === 'Role') {
            $user->load('role', 'filledRoleFields.roleField.field', 'filledRoleFields.selectedOptions');
        } else if ($page === 'Balance') {
            $user->balances = Helper::getLeaveBalance($user);
        } else if ($page === 'Document') {
            $user->load('role', 'filledRoleFields.roleField.field', 'filledRoleFields.file');
        } else if ($page === 'Form') {
            $user->load('role', 'filledForms.raisedForm', 'filledForms.sharedUsers.role:id,name');
        } else if ($page === 'Performance') {
            $user->load('role', 'observationNotesAsSubject.observer.role:id,name');
        }

        Gate::authorize('view', [User::class, $user]);

        return inertia('Employee/ViewUser', [
            'user' => $user,
            'page' => $page,
            'users' => User::with('role:id,name')->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        Gate::authorize('update', [User::class, $user]);

        $user->load('departmentStaffDetail', 'filledRoleFields', 'filledRoleFields.selectedOptions', 'filledRoleFields.roleField', 'filledRoleFields.roleField.field', 'filledRoleFields.file');

        return inertia('Employee/EditUser', [
            'roles' => Role::with('fields', 'fields.field', 'fields.field.options')->select('id', 'name')->get(),
            'departments' => Department::all(),
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        Gate::authorize('update', [User::class, $user]);

        $validated = $request->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->email, 'email')],
            'gender' => 'required|in:Male,Female',
            'dob' => 'required|date',
            'cid' => ['required', 'integer', Rule::unique('users')->ignore($user->cid, 'cid')],
            'nationality' => 'required|in:Bhutanese,Foreigner',
            'foreign_nationality' => 'required_if:nationality,Foreigner',
            'doj' => 'required|date',
            'role_id' => 'required|exists:roles,id',
            'department' => 'required',
            'designation' => 'required_unless:department,No Department',
            'filled_role_fields' => 'array'
        ]);

        //validating role fields with a validator
        $requiredRoleFields = array_filter($validated['filled_role_fields'], function ($filledRoleField) {
            return $filledRoleField['roleField']['field']['required'];
        });
        $newRequiredRoleFields = [];
        $requiredRoleFieldsValidator = [];
        $validationMessages = [];
        foreach ($requiredRoleFields as $filledRoleField) {
            // dd($filledRoleField);
            if ((($validated['nationality'] === 'Bhutanese' && !$filledRoleField['roleField']['foreigner_field']) || $validated['nationality'] === 'Foreigner') && $filledRoleField['roleField']['field']['type'] !== 'Title' && $filledRoleField['roleField']['field']['type'] !== 'Description') {
                if (($filledRoleField['roleField']['field']['type'] === 'File' && (($filledRoleField['file'] && $filledRoleField['delete']) || !$filledRoleField['file'])) || $filledRoleField['roleField']['field']['type'] !== 'File') {
                    $newId = $filledRoleField['roleField']['id'] . "id";
                    $newRequiredRoleFields[$newId] = $filledRoleField['value'];

                    if ($filledRoleField['roleField']['field']['type'] !== 'File') {
                        $requiredRoleFieldsValidator[$newId] = 'required';
                        $validationMessages[$newId . '.required'] = 'The ' . $filledRoleField['roleField']['field']['name'] . ' field is required!';
                    } else {
                        $requiredRoleFieldsValidator[$newId] = ['required', File::types(['jpg', 'jpeg', 'png', 'bmp', 'webp', 'pdf', 'docx', 'doc'])
                            ->max('5mb')];
                        $validationMessages[$newId . '.required'] = 'The ' . $filledRoleField['roleField']['field']['name'] . ' field is required!';
                        $validationMessages[$newId . '.mimes'] = 'The ' . $filledRoleField['roleField']['field']['name'] . ' field must be a file of type: jpg, jpeg, png, bmp, webp, pdf, docx, doc.';
                        $validationMessages[$newId . '.max'] = 'The ' . $filledRoleField['roleField']['field']['name'] . ' field must not be greater than 5000 kilobytes.';
                    }
                }
            }
        }
        $roleFieldValidator = Validator::make($newRequiredRoleFields, $requiredRoleFieldsValidator, $validationMessages);
        if ($roleFieldValidator->fails()) {
            return back()
                ->withErrors($roleFieldValidator)
                ->withInput();
        }


        DB::transaction(function () use ($request, $validated, $user) {
            $isRoleChanged = $user->role_id !== $request->all()['role_id'];
            $user->update(([...$request->except(['role_id', 'filled_role_fields', 'department', 'designation'])]));
            Role::where('id', $validated['role_id'])->first()->users()->save($user);

            $user->departmentStaffDetail()?->delete();
            if ($validated['department'] !== 'No Department') {
                $user->departmentStaffDetail()->save(new DepartmentStaffDetail(['designation' => $validated['designation'], 'department_id' => $validated['department']]));
            }

            $user->filledRoleFields->each(function (FilledRoleField $filledRoleField) {
                if ($filledRoleField->roleField->field->type !== 'File') {
                    $filledRoleField->delete();
                } else {
                    $filledRoleField->alert?->delete();
                }
            });

            foreach ($validated['filled_role_fields'] as $filledRoleField) {
                if ($filledRoleField['roleField']['field']['type'] === 'Checkbox') {
                    $frf = FilledRoleField::create(['role_field_id' => $filledRoleField['roleField']['id'], 'user_id' => $user->id]);
                    $frf->selectedOptions()->sync($filledRoleField['value'] ?? []);
                } else if ($filledRoleField['roleField']['field']['type'] === 'File') {
                    if (!$isRoleChanged) {
                        if ($filledRoleField['delete']) {
                            FilledRoleField::find($filledRoleField['id'])->delete();
                        }

                        if ($filledRoleField['value']) {
                            FilledRoleField::find($filledRoleField['id'])?->delete();

                            $frf = FilledRoleField::create(['role_field_id' => $filledRoleField['roleField']['id'], 'user_id' => $user->id]);
                            $path = $filledRoleField['value']->store('fieldFiles', 'public');
                            $frf->file()->save(new FieldFile(['filename' => $path]));
                        }
                    } else {
                        if ($filledRoleField['delete']) {
                            FilledRoleField::find($filledRoleField['id'])->delete();
                        }

                        if ($filledRoleField['value']) {
                            FilledRoleField::find($filledRoleField['id'])?->delete();

                            $frf = FilledRoleField::create(['role_field_id' => $filledRoleField['roleField']['id'], 'user_id' => $user->id]);
                            $path = $filledRoleField['value']->store('fieldFiles', 'public');
                            $frf->file()->save(new FieldFile(['filename' => $path]));
                        } else {
                            FilledRoleField::find($filledRoleField['id'])?->update(['role_field_id' => $filledRoleField['roleField']['id']]);
                        }
                    }
                } else if ($filledRoleField['roleField']['field']['type'] === 'Date') {
                    $frf = FilledRoleField::create(['date_value' => $filledRoleField['value'], 'role_field_id' => $filledRoleField['roleField']['id'], 'user_id' => $user->id]);
                } else {
                    $frf = FilledRoleField::create(['value' => $filledRoleField['value'], 'role_field_id' => $filledRoleField['roleField']['id'], 'user_id' => $user->id]);
                }
            }

            //populate alerts
            $role = $user->role;
            $user->filledRoleFields()->with('roleField', 'roleField.field', 'file')->get()->each(function (FilledRoleField $filledRoleField) use ($user, $role) {
                if ($filledRoleField->roleField->alertable) {
                    if ($filledRoleField->roleField->field->type === 'File' && !$filledRoleField->file) {
                        $alert = new Alert(["alert_period" => "Daily", "message" => "({$role->name}) {$user->name}'s {$filledRoleField->roleField->field->name} is missing!", "action_url" => "/employee/user/{$user->id}/edit"]);
                        $filledRoleField->alert()->save($alert);
                    } else if ($filledRoleField->roleField->field->type === 'Checkbox' && $filledRoleField->selectedOptions->isEmpty()) {
                        $alert = new Alert(["alert_period" => "Daily", "message" => "({$role->name}) {$user->name}'s {$filledRoleField->roleField->field->name} is missing!", "action_url" => "/employee/user/{$user->id}/edit"]);
                        $filledRoleField->alert()->save($alert);
                    } else if ($filledRoleField->roleField->field->type === 'Date' && !$filledRoleField->date_value) {
                        $alert = new Alert(["alert_period" => "Daily", "message" => "({$role->name}) {$user->name}'s {$filledRoleField->roleField->field->name} is missing!", "action_url" => "/employee/user/{$user->id}/edit"]);
                        $filledRoleField->alert()->save($alert);
                    } else if (!$filledRoleField->value && $filledRoleField->roleField->field->type !== 'File' && $filledRoleField->roleField->field->type !== 'Checkbox' && $filledRoleField->roleField->field->type !== 'Date') {
                        $alert = new Alert(["alert_period" => "Daily", "message" => "({$role->name}) {$user->name}'s {$filledRoleField->roleField->field->name} is missing!", "action_url" => "/employee/user/{$user->id}/edit"]);
                        $filledRoleField->alert()->save($alert);
                    }
                }
            });
        });

        return redirect()->route('employee.user.index')->with('success', 'User updated successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        Gate::authorize('delete', [User::class, $user]);
    }

    public function destroyMany(Request $request)
    {
        Gate::authorize('deleteMany', User::class);

        DB::transaction(function () use ($request) {
            $selected_users = $request->all()['selected_users'];
            $notiSubs = User::whereRelation('role', 'read_user', true)->get()->except(Auth::id());

            User::whereIn('id', $selected_users)->get()->each(function ($user) use ($notiSubs) {
                Notification::send($notiSubs, new UserDeleted($user));
                $user->delete();
            });
        });

        return redirect()->route('employee.user.index')->with('success', 'User(s) deleted successfuly!');
    }

    public function myProfile(Request $request)
    {
        $user = $request->user();
        $user->load('role', 'profilePicture', 'departmentStaffDetail.department', 'department');

        return inertia('Profile/PersonalDetails', [
            "user" => $user
        ]);
    }

    public function changeProfilePicture(Request $request)
    {
        $user = $request->user();
        $validated = $request->validate([
            'delete_picture' => 'required|boolean',
            'profile_picture' => ['nullable', File::image()
                ->max('5mb')]
        ]);

        DB::transaction(function () use ($validated, $user) {
            if ($validated['delete_picture'] || $validated['profile_picture']) {
                $user->profilePicture?->delete();
            }

            if ($validated['profile_picture']) {
                $path = $validated['profile_picture']->store('profilePictures', 'public');
                $user->profilePicture()->save(new ProfilePicture(['filename' => $path]));
            }

            return redirect('/profile/personalDetails')->with('success', 'Profile picture changed successfuly!');
        });
    }

    public function changePassword(Request $request)
    {
        $user = $request->user();

        $request->validate([
            'oldPassword' => 'required',
            'newPassword' => 'required|string|min:8',
            'confirmPassword' => 'required|same:newPassword'
        ]);

        $result = Auth::attempt([
            'email' => $user->email,
            'password' => $request['oldPassword']
        ], true);

        $password = Hash::make($request['newPassword']);

        if ($result) {
            $user->update(['password' => $password]);

            return redirect('/profile/personalDetails')->with('success', 'Password updated!');
        } else {
            return back()->withErrors([
                'oldPassword' => 'The provided credentials do not match our records.',
            ])->onlyInput('oldPassword');
        }
    }

    public function updateStartingBalance(Request $request, User $user)
    {
        Gate::allowIf(fn (User $user) => $user->role->update_user);

        $validated = $request->validate([
            'balance' => 'required|decimal:0,2',
            'id' => 'required'
        ]);

        $updated = $user->carry_forward_starting_balances;
        $updated[$validated['id']] = $validated['balance'];
        User::find($user->id)->update(['carry_forward_starting_balances' => $updated]);

        return redirect()->back()->with('success', 'Starting Balance updated!');
    }

    public function export()
    {
        Gate::allowIf(fn (User $user) => $user->role->read_user);

        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function download(User $user)
    {
        Gate::allowIf(fn (User $user) => $user->role->read_user || $user->department ? True : False);
        $user->load('role', 'profilePicture', 'departmentStaffDetail.department', 'department', 'filledRoleFields.roleField.field', 'filledRoleFields.selectedOptions', 'filledRoleFields.file', 'filledForms.raisedForm', 'filledForms.sharedUsers.role:id,name', 'observationNotesAsSubject.observer.role:id,name');
        $user->load('filledForms.raisedForm', 'filledForms.filledFormFields.formField.field.options', 'filledForms.filledFormFields.selectedOptions', 'filledForms.filledFormFields.file');
        $user->balances = Helper::getLeaveBalance($user);
        $data = [
            'title' => 'Test',
            'user' => $user
        ];
        // dd($user);
        $pdf = Pdf::loadView('user', $data);
        return $pdf->download("{$user->name}_{$user->role->name}_Details.pdf");
    }
}
