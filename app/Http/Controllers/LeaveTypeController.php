<?php

namespace App\Http\Controllers;

use App\Models\ApprovalStage;
use App\Models\LeaveType;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class LeaveTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        Gate::authorize('viewAny', LeaveType::class);

        return inertia('Leave/LeaveTypes', [
            'leaveTypes' => LeaveType::orderBy('updated_at', 'desc')->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        Gate::authorize('create', LeaveType::class);

        return inertia('Leave/CreateLeaveType', [
            'users' => User::with('role:id,name')->select('id', 'name', 'cid', 'role_id')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Gate::authorize('create', LeaveType::class);

        $validated = $request->validate([
            'type' => 'required|unique:App\Models\LeaveType,type',
            'requires_hod_approval' => 'required|boolean',
            'requires_doc' => 'required|boolean',
            'carry_forward' => 'required|boolean',
            'balance_type' => 'required|in:Number,Public Holiday,Unlimited,Fixed',
            'public_holidays' => 'array',
            'public_holidays.*.name' => 'required|string',
            'public_holidays.*.date' => 'required|date',
            'monthly_balance_accumulation_rate' => 'required_if:balance_type,Number|decimal:0,2',
            'fixed_balance' => 'required_if:balance_type,Fixed,integer',
            'approvers' => 'required_if:requires_hod_approval,false|array'
        ]);

        //validating approvers with a validator
        $validationSubjects = [];
        $validators = [];
        $validationMessages = [];
        foreach ($validated['approvers'] as $approver) {
            $newId = $approver['user']['id'] . "id";
            $validationSubjects[$newId] = $approver['stage'];
            $validators[$newId] = 'required|integer';
            $validationMessages[$newId . '.required'] = 'Approver ' . $approver['user']['name'] . "'s approval stage is required!";
            $validationMessages[$newId . '.integer'] = 'Approver ' . $approver['user']['name'] . "'s approval stage must an integer!";
        }

        $validator = Validator::make($validationSubjects, $validators, $validationMessages);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        DB::transaction(function () use ($validated, $request) {
            $leaveType = LeaveType::create($request->except(['approvers']));

            foreach ($validated['approvers'] as $approver) {
                $leaveType->approvalStages()->save(new ApprovalStage(['user_id' => $approver['user']['id'], 'stage' => $approver['stage']]));
            }
        });

        return redirect()->route('creationhub.leaveType.index')->with('success', 'Leave Type created successfuly!');
    }

    /**
     * Display the specified resource.
     */
    public function show(LeaveType $leaveType)
    {
        Gate::authorize('view', [LeaveType::class, $leaveType]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(LeaveType $leaveType)
    {
        Gate::authorize('update', [LeaveType::class, $leaveType]);

        $leaveType->load('approvalStages', 'approvalStages.user', 'approvalStages.user.role:id,name');

        return inertia('Leave/EditLeaveType', [
            'users' => User::with('role:id,name')->select('id', 'name', 'cid', 'role_id')->get(),
            'leaveType' => $leaveType
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, LeaveType $leaveType)
    {
        Gate::authorize('update', [LeaveType::class, $leaveType]);

        $validated = $request->validate([
            'type' => ['required', Rule::unique('leave_types')->ignore($leaveType->type, 'type')],
            'requires_hod_approval' => 'required|boolean',
            'requires_doc' => 'required|boolean',
            'carry_forward' => 'required|boolean',
            'balance_type' => 'required|in:Number,Public Holiday,Unlimited,Fixed',
            'monthly_balance_accumulation_rate' => 'required_if:balance_type,Number|decimal:0,2',
            'fixed_balance' => 'required_if:balance_type,Fixed,integer',
            'approvers' => 'required_if:requires_hod_approval,false|array'
        ]);

        //validating approvers with a validator
        $validationSubjects = [];
        $validators = [];
        $validationMessages = [];
        foreach ($validated['approvers'] as $approver) {
            $newId = $approver['user']['id'] . "id";
            $validationSubjects[$newId] = $approver['stage'];
            $validators[$newId] = 'required|integer';
            $validationMessages[$newId . '.required'] = 'Approver ' . $approver['user']['name'] . "'s approval stage is required!";
            $validationMessages[$newId . '.integer'] = 'Approver ' . $approver['user']['name'] . "'s approval stage must an integer!";
        }

        $validator = Validator::make($validationSubjects, $validators, $validationMessages);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        DB::transaction(function () use ($validated, $request, $leaveType) {
            $leaveType->update($request->except(['approvers']));

            //delete old stages
            $leaveType->approvalStages()->whereNotIn('id', array_map(fn ($approver) => $approver['id'], $validated['approvers']))->get()->each(function ($leaveStageApprover) {
                $leaveStageApprover->delete();
            });

            foreach ($validated['approvers'] as $approver) {
                if (!$approver['id']) {
                    ApprovalStage::create(['leave_type_id' => $leaveType->id, 'user_id' => $approver['user']['id'], 'stage' => $approver['stage']]);
                } else {
                    ApprovalStage::find($approver['id'])->update(['leave_type_id' => $leaveType->id, 'user_id' => $approver['user']['id'], 'stage' => $approver['stage']]);
                }
            }
        });

        return redirect()->route('creationhub.leaveType.index')->with('success', 'Leave Type updated successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(LeaveType $leaveType)
    {
        Gate::authorize('delete', [LeaveType::class, $leaveType]);
    }

    public function destroyMany(Request $request)
    {
        Gate::authorize('deleteMany', LeaveType::class);
        
        DB::transaction(function () use ($request) {
            $selected = $request->all()['selected'];

            LeaveType::whereIn('id', $selected)->get()->each(function ($leaveType) {
                $leaveType->delete();
            });

            return redirect()->route('creationhub.leaveType.index')->with('success', 'Leave Type(s) deleted successfuly!');
        });
    }
}
