<?php

namespace App\Http\Controllers;

use App\Models\Alert;
use App\Models\Approval;
use App\Models\ApprovalStage;
use App\Models\Department;
use App\Models\FilledForm;
use App\Models\FormTemplate;
use App\Models\RaisedForm;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class RaisedFormController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        Gate::authorize('viewAny', RaisedForm::class);

        return inertia('Form/RaisedForms', [
            'raisedForms' => RaisedForm::with('formTemplate')
                ->whereRelation('formTemplate.users', 'users.id', $request->user()->id)
                ->orderBy('updated_at', 'desc')
                ->paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        Gate::authorize('create', RaisedForm::class);

        return inertia('Form/CreateRaisedForm', [
            'formTemplates' => FormTemplate::all(),
            'users' => User::with('role:id,name', 'departmentStaffDetail')
                ->select('id', 'name', 'cid', 'role_id')
                ->get(),
            'roles' => Role::all(),
            'departments' => Department::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Gate::authorize('create', RaisedForm::class);

        $validated = $request->validate([
            'name' => 'required',
            'form_template' => 'required',
            'notification_subjects' => 'array',
            'users' => 'array',
            'approvers' => 'array',
            'self_fill_out' => 'required|boolean',
            'form_filler' => 'required_if:self_fill_out,false'
        ]);

        //validating approvers with a validator
        $validationSubjects = [];
        $validators = [];
        $validationMessages = [];
        foreach ($validated['approvers'] as $approver) {
            $newId = $approver['user']['id'] . "id";
            $validationSubjects[$newId] = $approver['stage'];
            $validators[$newId] = 'required|integer';
            $validationMessages[$newId . '.required'] = 'Approver ' . $approver['user']['name'] . "'s approval stage is required!";
            $validationMessages[$newId . '.integer'] = 'Approver ' . $approver['user']['name'] . "'s approval stage must an integer!";
        }

        $validator = Validator::make($validationSubjects, $validators, $validationMessages);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }


        DB::transaction(function () use ($request, $validated) {
            $raisedForm = RaisedForm::create([...$request->except('form_template', 'notification_subjects', 'users', 'approvers', 'form_filler'), 'form_template_id' => $validated['form_template']['id'], 'user_id' => $validated['form_filler'] && !$validated['self_fill_out'] ? $validated['form_filler']['id'] : null]);

            foreach ($validated['approvers'] as $approver) {
                $raisedForm->approvalStages()->save(new ApprovalStage(['user_id' => $approver['user']['id'], 'stage' => $approver['stage']]));
            }

            $raisedForm->users()->sync(array_map(fn ($user) => $user['id'], $validated['users']));
            $raisedForm->notificationSubjects()->sync(array_map(fn ($user) => $user['id'], $validated['notification_subjects']));

            $approvalStages = $raisedForm->approvalStages->sortBy('stage');
            //Create filled forms
            $raisedForm->users->each(function ($user) use ($raisedForm, $approvalStages, $validated) {
                $filledForm = new FilledForm(['raised_form_id' => $raisedForm->id, 'submitted' => false, 'verified_remarks' => 'as']);
                $user->filledForms()->save($filledForm);

                //create approvals
                if ($approvalStages->isNotEmpty()) {
                    $approvalStages->each(function ($approvalStage) use ($filledForm) {
                        $approval = new Approval(["user_id" => $approvalStage->user_id, "status" => "Pending", "stage" => $approvalStage->stage]);
                        $filledForm->approvals()->save($approval);
                    });
                }

                if ($validated['self_fill_out']) {
                    $alert = new Alert(["alert_period" => "Weekly", "message" => "A form is in pending! Please fill out the {$raisedForm->name}.", "action_url" => "/form/filledForm/{$filledForm->id}/edit"]);
                    $filledForm->alert()->save($alert);
                    $alert->users()->sync([$user->id]);
                } else {
                    $alert = new Alert(["alert_period" => "Weekly", "message" => "A form is in pending! Please fill out the {$raisedForm->name} for ({$user->role->name}) {$user->name}.", "action_url" => "/form/filledForm/{$filledForm->id}/edit"]);
                    $filledForm->alert()->save($alert);
                    $alert->users()->sync([$validated['form_filler']['id']]);
                }
            });


            // $roleIds = array_map(fn ($role) => $role['id'], $validated['roles']);
            // $raisedForm->users()->with('role:id')->get()->each(function ($user) use ($raisedForm, $roleIds) {
            //     if (!in_array($user->role->id, $roleIds)) {
            //         $filledForm = new FilledForm(['raised_form_id' => $raisedForm->id, 'submitted' => false, 'verified_remarks' => 'as']);
            //         $user->filledForms()->save($filledForm);

            //         $alert = new Alert(["alert_period" => "Weekly", "message" => "A form is in pending! Please fill out the {$raisedForm->name}.", "action_url" => "/form/filledForm/{$filledForm->id}/edit"]);
            //         $filledForm->alert()->save($alert);
            //         $alert->users()->sync([$user->id]);
            //     }
            // });
        });

        return redirect()->route('form.raisedForm.index')->with('success', 'Form raised successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(RaisedForm $raisedForm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(RaisedForm $raisedForm)
    {
        Gate::authorize('update', [RaisedForm::class, $raisedForm]);

        return inertia('Form/EditRaisedForm', [
            'raisedForm' => $raisedForm
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, RaisedForm $raisedForm)
    {
        Gate::authorize('update', [RaisedForm::class, $raisedForm]);

        $validated = $request->validate([
            'name' => 'required'
        ]);

        $raisedForm->update($validated);

        return redirect()->route('form.raisedForm.index')->with('success', 'Raised Form updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(RaisedForm $raisedForm)
    {
        //
    }

    public function destroyMany(Request $request)
    {
        Gate::authorize('deleteMany', RaisedForm::class);

        DB::transaction(function () use ($request) {
            $selected = $request->all()['selected'];

            RaisedForm::whereIn('id', $selected)->get()->each(function ($raisedForm) {
                $raisedForm->delete();
            });

            return redirect()->route('department.department.index')->with('success', 'Raised Form(s) deleted successfuly!');
        });
    }
}
