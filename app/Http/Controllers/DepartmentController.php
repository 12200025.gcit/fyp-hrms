<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class DepartmentController extends Controller
{
    public function index()
    {
        Gate::authorize('viewAny', Department::class);

        return inertia('Department/Departments', [
            'departments' => Department::with('hod')->paginate(10),
        ]);
    }

    public function create()
    {
        Gate::authorize('create', Department::class);

        return inertia('Department/CreateDepartment', [
            'users' => User::with('role:id,name')
                ->doesntHave('department')
                ->select('id', 'name', 'cid', 'role_id')
                ->get()
        ]);
    }

    public function store(Request $request)
    {
        Gate::authorize('create', Department::class);

        $validatedData = $request->validate([
            'name' => 'required|unique:App\Models\Department,name',
            'description' => 'nullable',
            'hod' => 'required'
        ]);
        Department::create([...$request->except('hod'), 'hod_id' => $validatedData['hod']['id']]);

        return redirect()->route('department.department.index')->with('success', 'Department created successfully.');
    }

    public function edit(Department $department)
    {
        Gate::authorize('update', [Department::class, $department]);

        $department->load('hod', 'hod.role:id,name');

        return inertia('Department/EditDepartment', [
            'department' => $department,
            'users' => User::with('role:id,name')
                ->doesntHave('department')
                ->orWhere('id', $department->hod_id)
                ->select('id', 'name', 'cid', 'role_id')
                ->get()
        ]);
    }

    public function update(Request $request, Department $department)
    {
        Gate::authorize('update', [Department::class, $department]);

        $validatedData = $request->validate([
            'name' => ['required', Rule::unique('departments')->ignore($department->name, 'name')],
            'description' => 'nullable',
            'hod' => 'required'
        ]);

        $department->update([...$request->except('hod'), 'hod_id' => $validatedData['hod']['id']]);

        return redirect()->route('department.department.index')->with('success', 'Department updated successfully.');
    }

    public function destroy(Department $department)
    {
        Gate::authorize('delete', [Department::class, $department]);

        $department->delete();

        return redirect()->route('Department.index')->with('success', 'Department deleted successfully.');
    }

    public function destroyMany(Request $request)
    {
        Gate::authorize('deleteMany', Department::class);

        DB::transaction(function () use ($request) {
            $selected = $request->all()['selected'];

            Department::whereIn('id', $selected)->get()->each(function ($department) {
                $department->delete();
            });

            return redirect()->route('department.department.index')->with('success', 'Department(s) deleted successfuly!');
        });
    }
}
