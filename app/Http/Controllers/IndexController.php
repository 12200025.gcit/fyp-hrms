<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        return inertia('Index/Index');
    }

    public function dashboard_adminDashboard(){
        return inertia('Dashboard/AdminDashboard');
    }

    public function employees_Users(){
        return inertia('Employee/Users');
    }

    public function attendance_report(){
        return inertia('Attendance/Report');
    }
    public function departments(){
        return inertia('Department/Departments');
    }
    
    public function employees_Roles(){
        return inertia('Employee/Roles');
    }

    public function employees_CreateUser(){
        return inertia('Employee/CreateUser');
    }

    public function profile_personalDetails(){
        return inertia('Profile/PersonalDetails');
    }

    public function profile_changePassword(){
        return inertia('Profile/ChangePassword');
    }

     public function roster_departmentRoster(){
        return inertia('Roster/DepartmentRoster');
    }

    public function roster_departmentShift(){
        return inertia('Roster/DepartmentShift');
    }
    public function dashboard_emdashboard(){
        return inertia('Dashboard/EMDashboard');
    }

    public function dashboard_hoddashboard(){
        return inertia('Dashboard/HODDashboard');
    }

    public function dashboard_ictdashboard(){
        return inertia('Dashboard/ICTDashboard');
    }

    public function dashboard_employeedashboard(){
        return inertia('Dashboard/EmployeeDashboard');
    }

    public function employees_EditUser(){
        return inertia('Employee/EditUser');
    }

    public function employees_CreateRole(){
        return inertia('Employee/CreateRole');
    }
    
    public function employees_EditRole(){
        return inertia('Employee/EditRole');
    }
}

