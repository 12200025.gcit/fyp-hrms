<?php

namespace App\Http\Controllers;

use App\Models\LeaveApplication;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $now = Carbon::now();
        $now7 = Carbon::now()->addDays(7);
        $bday = Carbon::now()->addDays(30);
        $anniday = Carbon::now()->addDays(90);

        $user = $request->user();
        $user->balances = $user->role->leaveTypes->map(function ($leaveType, $key) use ($now, $user) {
            $leaveStayed = 0;
            $la = $user->leaveApplications()->whereRelation('leaveType', 'type', $leaveType->type)->where('approval_status', 'Approved')->get()->each(function ($la) use (&$leaveStayed, $now) {
                $sdt = new DateTime($la->start_date);
                $edt = new DateTime($la->end_date);

                if ($sdt->format('Y') == $now->year && $edt->format('Y') == $now->year) {
                    $leaveStayed += $edt->diff($sdt)->days + 1;
                } else if ($sdt->format('Y') == $now->year) {
                    $leaveStayed += $sdt->diff(Carbon::create($now->year, 1, 1)->endOfyear->toDateTime())->days + 1;
                } else if ($edt->format('Y') == $now->year) {
                    $leaveStayed += Carbon::create($now->year, 1, 1)->toDateTime()->diff($edt)->days + 1;
                }
            });
            return ["leave_type" => $leaveType, "balance" => ($leaveType->monthly_balance_accumulation_rate * $now->month) - $leaveStayed];
        });

        return inertia('Dashboard/Dashboard', [
            'user' => $user,
            'leaveApplications' => LeaveApplication::with('user.role:id,name', 'leaveType')
                ->where('approval_status', 'Approved')
                ->whereBetween('start_date', [$now, $now7])
                ->orWhereBetween('end_date', [$now, $now7])
                ->get(),
            'bdayUsers' => User::with('role:id,name')
                ->whereRaw('extract(month from dob) * 100 + extract(day from dob) between ? and ?', [
                    intval($now->format('md')),
                    intval($bday->format('md'))
                ])
                ->get(),
            'anniUsers' => User::with('role:id,name')
                ->whereRaw('extract(month from doj) * 100 + extract(day from doj) between ? and ?', [
                    intval($now->format('md')),
                    intval($anniday->format('md'))
                ])
                ->get(),
            'userCount' => User::count(),
            'maleCount' => User::where('gender', 'Male')->get()->count(),
            'femaleCount' => User::where('gender', 'Female')->get()->count()
        ]);
    }
}
