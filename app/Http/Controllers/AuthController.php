<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function create(){
        return inertia(
            'Auth/Login'
        );
    }

    public function store(Request $request){        
        $result = Auth::attempt($request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]), true);


        if ($result){
            $request->session()->regenerate();

            return redirect()->intended('/');
        }else{
            return back()->withErrors([
                'email' => 'The provided credentials were not found.',
            ])->onlyInput('email');
        }
    }

    public function destroy(Request $request){
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/login');
    }

    public function back(Request $request){
        $request->session()->forget('success');

        return redirect()->back();
    }
}
