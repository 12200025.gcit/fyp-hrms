<?php

namespace App\Http\Controllers;

use App\Models\ObservationNote;
use App\Models\User;
use App\Notifications\ObservationMade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class ObservationNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        Gate::authorize('viewAnyObservations', ObservationNote::class);

        return inertia('Performance/Observations', [
            'observationNotes' => $request->user()
                ->observationNotesAsObserver()
                ->with('subject.role:id,name', 'observer.role:id,name')
                ->orderBy('updated_at', 'desc')
                ->paginate(10),
            'type' => 'observer'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        Gate::authorize('create', ObservationNote::class);

        return inertia('Performance/CreateObservation', [
            'users' => User::with('role:id,name')
                ->whereRelation('role', 'performance_eligibility', true)
                ->select('id', 'name', 'cid', 'role_id')
                ->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Gate::authorize('create', ObservationNote::class);

        $validated = $request->validate([
            'type' => 'required|in:Appreciation Note,Incident Report',
            'note' => 'required|string',
            'subject' => 'required'
        ]);

        DB::transaction(function () use ($request, $validated) {
            $on = new ObservationNote([...$request->except('subject'), 'subject_id' => $validated['subject']['id']]);
            $request->user()->observationNotesAsObserver()->save($on);

            User::find($validated['subject']['id'])->notify(new ObservationMade($on));
        });

        return redirect()->route('performance.observationNote.index')->with('success', 'Observation Note created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user ,ObservationNote $observationNote)
    {
        Gate::authorize('view', $observationNote);

        $observationNote->load('observer.role:id,name', 'subject.role:id,name');

        return inertia('Performance/ViewObservation', [
            'observationNote' => $observationNote
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ObservationNote $observationNote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ObservationNote $observationNote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ObservationNote $observationNote)
    {
        //
    }

    public function destroyMany(Request $request)
    {
        Gate::authorize('deleteMany', ObservationNote::class);

        DB::transaction(function () use ($request) {
            $selected = $request->all()['selected'];

            ObservationNote::whereIn('id', $selected)->get()->each(function ($on) {
                $on->delete();
            });
        });

        return redirect()->route('performance.observationNote.index')->with('success', 'Observation Note(s) deleted successfuly!');
    }

    public function indexPerformance(Request $request)
    {
        Gate::authorize('viewAnyPerformance', ObservationNote::class);

        return inertia('Performance/Observations', [
            'observationNotes' => $request->user()
                ->observationNotesAsSubject()
                ->with('subject.role:id,name', 'observer.role:id,name')
                ->orderBy('updated_at', 'desc')
                ->paginate(10),
            'type' => 'subject'
        ]);
    }
}
