<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Shift;
use App\Models\Roster;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Models\AssignedShift;
use App\Models\LeaveApplication;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class RosterController extends Controller
{
    public function store(Request $request)
    {
        Gate::allowIf(fn (User $user) => $user->role->manage_overall_roster || $user->department ? True : False);
        // Gate::authorize('create', Department::class);
        $validated = $request->validate([
            'date' => 'required|date',
            'assigned_shifts' => 'array',
            'assigned_shifts.*.shift' => 'required',
            'assigned_shifts.*.user' => 'required',
            'delete_assigned_shifts' => 'array'
        ]);

        DB::transaction(function () use ($validated) {
            foreach ($validated['assigned_shifts'] as $assignedShift) {
                AssignedShift::create(['shift_id' => $assignedShift['shift']['id'], 'user_id' => $assignedShift['user']['id'], 'date' => $validated['date']]);
            }

            //delete marked assignedshifts
            foreach ($validated['delete_assigned_shifts'] as $id) {
                AssignedShift::find($id)?->delete();
            }
        });

        return redirect()->back()->with('success', 'Roster updated successfully.');
    }

    public function index(Request $request)
    {
        Gate::allowIf(fn (User $user) => $user->role->view_overall_roster || $user->department ? True : False);

        $now = Carbon::now();
        $now7 = Carbon::now()->addDays(7);

        $all = $request->all();
        $year = $all['year'] ?? Carbon::now()->year;
        $type = $request->user()->role->view_overall_roster ? 'Overall' : 'HOD';
        $departmentId = null;
        if ($type === 'Overall') {
            $departmentId = $all['department_id'] ?? 'All Department';
        } else {
            $departmentId = $request->user()->department->id;
        }
        return inertia('Roster/Rosters', [
            'users' => $departmentId === 'All Department' ? User::with('role:id,name')
                ->whereRelation('role', 'roster_eligibility', true)
                ->select('id', 'name', 'cid', 'role_id')
                ->get() : User::with('role:id,name')
                ->whereRelation('departmentStaffDetail', 'department_id', $departmentId)
                ->whereRelation('role', 'roster_eligibility', true)
                ->select('id', 'name', 'cid', 'role_id')
                ->get(),
            'departments' => Department::all(),
            'shifts' => $departmentId === 'All Department' ? Shift::with('department')->get() : Shift::with('department')->whereRelation('department', 'id', $departmentId)->get(),
            'assignedShifts' => $departmentId === 'All Department' ? AssignedShift::with('shift.department', 'user.role:id,name')
                ->whereYear('date', $year)
                ->get() : AssignedShift::with('shift.department', 'user.role:id,name')
                ->whereRelation('shift.department', 'id', $departmentId)
                ->whereYear('date', $year)
                ->get(),
            'year' => $year,
            'department' => $departmentId,
            'leaveApplications' => LeaveApplication::with('user.role:id,name', 'user.departmentStaffDetail', 'leaveType')
                ->where('approval_status', 'Approved')
                ->whereBetween('start_date', [$now, $now7])
                ->orWhereBetween('end_date', [$now, $now7])
                ->get(),
            'type' => $type
        ]);
    }

    public function myRoster(Request $request)
    {
        Gate::allowIf(fn (User $user) => $user->role->roster_eligibility);

        $all = $request->all();
        $year = $all['year'] ?? Carbon::now()->year;

        return inertia('Roster/Rosters', [
            'assignedShifts' => AssignedShift::with('shift.department', 'user.role:id,name')
                ->whereRelation('user', 'id', $request->user()->id)
                ->whereYear('date', $year)
                ->get(),
            'year' => $year,
            'type' => 'MyRoster'
        ]);
    }

    public function download(Request $request)
    {
        Gate::allowIf(fn (User $user) => $user->role->view_overall_roster || $user->department ? True : False);

        $all = $request->all();
        $year = $all['year'] ?? Carbon::now()->year;
        $type = $request->user()->role->view_overall_roster ? 'Overall' : 'HOD';
        $departmentId = null;
        if ($type === 'Overall') {
            $departmentId = $all['department_id'] ?? 'All Department';
        } else {
            $departmentId = $request->user()->department->id;
        }

        $data = [
            'title' => 'Test',
            'assignedShifts' => $departmentId === 'All Department' ? AssignedShift::with('shift.department', 'user.role:id,name')
                ->whereYear('date', $year)
                ->get() : AssignedShift::with('shift.department', 'user.role:id,name')
                ->whereRelation('shift.department', 'id', $departmentId)
                ->whereYear('date', $year)
                ->get(),
            'year' => $year,
            'department' => $departmentId === 'All Department' ? $departmentId : Department::find($departmentId)->name,
        ];
        // dd($data);
        $pdf = Pdf::loadView('roster', $data);
        $pdf->setPaper('A4', 'landscape');
        
        // dd($pdf);
        
        return $pdf->download("{$year}_Roster.pdf");
    }
}
