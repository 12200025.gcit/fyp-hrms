<?php

namespace App\Http\Controllers;

use App\Models\Alert as ModelsAlert;
use App\Models\FieldFile;
use App\Models\FilledForm;
use App\Models\FilledFormField;
use App\Models\RaisedForm;
use App\Models\User;
use App\Notifications\Alert;
use App\Notifications\FormFilledOut;
use App\Notifications\FormShared;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\File;

class FilledFormController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        Gate::authorize('viewAny', FilledForm::class);

        $uffs = $request->user()
            ->filledForms()
            ->where('submitted', false)
            ->whereRelation('raisedForm', 'self_fill_out', true)
            ->get();
        $ffs = new Collection();
        $request->user()
            ->fillables->each(function (RaisedForm $raisedForm) use (&$ffs) {
                $raisedForm->filledForms()->where('submitted', false)->get()->each(function ($ff) use (&$ffs) {
                    $ffs->push($ff);
                });
            });
        $filledForms = $uffs->merge($ffs);

        return inertia('Form/PendingForms', [
            'filledForms' => $filledForms->isNotEmpty() ? $filledForms->toQuery()
                ->with('raisedForm', 'user.role:id,name')
                ->orderBy('updated_at', 'desc')
                ->paginate(10) :
                $request->user()
                ->filledForms()
                ->where('id', -1)
                ->paginate(10)

        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user, FilledForm $filledForm)
    {
        Gate::authorize('view', [FilledForm::class, $filledForm]);

        $filledForm->load('raisedForm', 'filledFormFields.formField.field.options', 'filledFormFields.selectedOptions', 'filledFormFields.file', 'approvals.user.role:id,name');

        return inertia('Form/ViewForm', [
            'filledForm' => $filledForm
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FilledForm $filledForm)
    {
        Gate::authorize('update', [FilledForm::class, $filledForm]);
        $filledForm->load('raisedForm.formTemplate.formFields.field.options', 'approvals.user.role:id,name');

        return inertia('Form/SubmitForm', [
            'filledForm' => $filledForm
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, FilledForm $filledForm)
    {
        Gate::authorize('update', [FilledForm::class, $filledForm]);

        $validated = $request->validate([
            'filled_form_fields' => 'array'
        ]);

        //validating role fields with a validator
        $requiredformFields = array_filter($validated['filled_form_fields'], function ($filledFormField) {
            return $filledFormField['formField']['field']['required'];
        });
        $newRequiredformFields = [];
        $requiredformFieldsValidator = [];
        $validationMessages = [];
        foreach ($requiredformFields as $filledFormField) {
            // dd($filledFormField);
            $newId = $filledFormField['formField']['id'] . "id";
            $newRequiredformFields[$newId] = $filledFormField['value'];
            if ($filledFormField['formField']['field']['type'] !== 'Title' && $filledFormField['formField']['field']['type'] !== 'Description') {
                if ($filledFormField['formField']['field']['type'] !== 'File') {
                    $requiredformFieldsValidator[$newId] = 'required';
                    $validationMessages[$newId . '.required'] = 'The ' . $filledFormField['formField']['field']['name'] . ' field is required!';
                } else {
                    $requiredformFieldsValidator[$newId] = ['required', File::types(['jpg', 'jpeg', 'png', 'bmp', 'webp', 'pdf', 'docx', 'doc'])
                        ->max('5mb')];
                    $validationMessages[$newId . '.required'] = 'The ' . $filledFormField['formField']['field']['name'] . ' field is required!';
                    $validationMessages[$newId . '.mimes'] = 'The ' . $filledFormField['formField']['field']['name'] . ' field must be a file of type: jpg, jpeg, png, bmp, webp, pdf, docx, doc.';
                    $validationMessages[$newId . '.max'] = 'The ' . $filledFormField['formField']['field']['name'] . ' field must not be greater than 5000 kilobytes.';
                }
            }
        }
        $formFieldValidator = Validator::make($newRequiredformFields, $requiredformFieldsValidator, $validationMessages);
        if ($formFieldValidator->fails()) {
            return back()
                ->withErrors($formFieldValidator)
                ->withInput();
        }

        DB::transaction(function () use ($filledForm, $validated, $request) {
            foreach ($validated['filled_form_fields'] as $filledFormField) {
                $frf = null;
                if ($filledFormField['formField']['field']['type'] === 'Checkbox') {
                    $frf = FilledFormField::create(['form_field_id' => $filledFormField['formField']['id'], 'filled_form_id' => $filledForm->id]);
                    $frf->selectedOptions()->sync($filledFormField['value']);
                } else if ($filledFormField['formField']['field']['type'] === 'File') {
                    $frf = FilledFormField::create(['form_field_id' => $filledFormField['formField']['id'], 'filled_form_id' => $filledForm->id]);
                    $path = $filledFormField['value']->store('fieldFiles', 'public');
                    $frf->file()->save(new FieldFile(['filename' => $path]));
                } else if ($filledFormField['formField']['field']['type'] === 'Date') {
                    $frf = FilledFormField::create(['date_value' => $filledFormField['value'], 'form_field_id' => $filledFormField['formField']['id'], 'filled_form_id' => $filledForm->id]);
                } else {
                    $frf = FilledFormField::create(['value' => $filledFormField['value'], 'form_field_id' => $filledFormField['formField']['id'], 'filled_form_id' => $filledForm->id]);
                }
            }

            $approvals = $filledForm->approvals->sortBy('stage');
            if ($approvals->isNotEmpty()) {
                $firstStage = $approvals->first()->stage;

                $user = $filledForm->user;
                $message = "A {$filledForm->raisedForm->name} filled out by ({$user->role->name}) {$user->name} is ready for approval.";
                $action_url = "/form/formRequest/{$filledForm->id}";

                $approvals->each(function ($approval) use ($message, $action_url, $firstStage, $request) {
                    if ($approval->stage === $firstStage) {
                        $user = $approval->user;

                        if ($request->user()->id !== $user->id) {
                            $user->notify(new Alert($message, $action_url, true));
                        }

                        $alert = new ModelsAlert(["alert_period" => "Twice a Day", "message" => $message, "action_url" => $action_url]);
                        $approval->alert()->save($alert);
                        $user->alerts()->attach($alert->id);
                    }
                });
            }

            $filledForm->update(['submitted' => true]);
            $filledForm->alert?->delete();

            //notify
            Notification::send($filledForm->raisedForm->notificationSubjects, new FormFilledOut($filledForm));
        });

        return redirect()->route('form.filledForm.index')->with('success', 'Form filled out successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(FilledForm $filledForm)
    {
        //
    }

    public function share(Request $request, FilledForm $filledForm)
    {
        Gate::authorize('share', $filledForm);

        $validated = $request->validate([
            'users' => 'required|array'
        ]);

        DB::transaction(function () use ($filledForm, $validated) {
            $filledForm->sharedUsers()->attach(array_map(fn ($user) => $user['id'], $validated['users']));

            foreach ($validated['users'] as $user) {
                User::find($user['id'])->notify(new FormShared($filledForm));
            }
        });

        return redirect()->back()->with('success', 'Form shared successfuly!');
    }

    public function sharedIndex(Request $request)
    {
        Gate::authorize('viewAnyShared', FilledForm::class);

        return inertia('Form/PendingForms', [
            'filledForms' => $request
                ->user()
                ->sharedFilledForms()
                ->with('raisedForm', 'user.role:id,name')
                ->paginate(10),
            'type' => 'shared'
        ]);
    }

    public function myForms(Request $request)
    {
        Gate::authorize('viewAny', FilledForm::class);

        return inertia('Form/PendingForms', [
            'filledForms' => $request->user()
                ->filledForms()
                ->where('submitted', true)
                ->with('raisedForm', 'user.role:id,name')
                ->orderBy('updated_at', 'desc')
                ->paginate(10),
            'type' => 'myForm'
        ]);
    }
}
