<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $now = Carbon::now()->toDateString();
        $filters = $request->validate([
            'department' => 'string',
            'date' => "nullable|date|before:$now"
        ]);

        return inertia('Attendance/Report', [
            "attendances" => Attendance::orderBy('date', 'desc')
                ->when($filters['date'] ?? False, function (Builder $query) use ($filters) {
                    $query->where('date', $filters['date']);
                })
                ->when($filters['department'] ?? False ? $filters['department'] !== 'All' : False, function (Builder $query) use ($filters) {
                    $query->where('dpt_code', $filters['department']);
                })
                ->paginate(10)
                ->withQueryString(),
            "filters" => $filters
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Attendance $attendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Attendance $attendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Attendance $attendance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Attendance $attendance)
    {
        //
    }
}
