<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Shift;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class ShiftController extends Controller
{
    public function index(Request $request)
    {
        Gate::authorize('viewAny', Shift::class);

        $filters = $request->all();
        return inertia('Roster/DepartmentShift', [
            'shifts' => Shift::with('department')->orderBy('updated_at', 'desc')
                ->byName($filters['name'] ?? '')
                ->byDepartment($filters['department'] ?? 'All Department')
                ->paginate(10),
            'departments' => Department::all(),
            'filters' => $filters,
        ]);
    }

    public function create(Request $request)
    {
        Gate::authorize('create', Shift::class);

        return inertia('Roster/CreateShift', [
            'departments' => Department::all()
        ]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'start_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i',
            'department' => 'required'
        ]);

        $shift = new Shift([...$request->except('department'), 'department_id' => $validatedData['department']['id']]);
        Gate::authorize('store', [Shift::class, Department::find($validatedData['department']['id'])]);
        $shift->save();

        return redirect()->route('roster.shift.index')->with('success', 'Shift created successfully.');
    }

    public function edit(Request $request, Shift $shift)
    {
        Gate::authorize('update', [Shift::class, $shift]);

        $shift->load('department');

        return inertia('Roster/EditShift', [
            'shift' => $shift,
            'departments' => Department::all()
        ]);
    }

    public function update(Request $request, Shift $shift)
    {
        Gate::authorize('update', [Shift::class, $shift]);

        $validatedData = $request->validate([
            'name' => 'required|string',
            'start_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i',
            'department' => 'required'
        ]);

        $shift->update([...$request->except('department'), 'department_id' => $validatedData['department']['id']]);

        return redirect()->route('roster.shift.index')->with('success', 'Shift updated successfully.');
    }

    public function destroyMany(Request $request)
    {
        DB::transaction(function () use ($request) {
            $selected = $request->all()['selected'];

            Shift::whereIn('id', $selected)->get()->each(function ($shift) {
                Gate::authorize('delete', [Shift::class, $shift]);
                $shift->delete();
            });
        });

        return redirect()->route('roster.shift.index')->with('success', 'Shift(s) deleted successfuly!');
    }
}
