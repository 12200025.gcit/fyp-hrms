<?php

namespace App\Http\Controllers;

use App\Models\Alert as ModelsAlert;
use App\Models\FilledForm;
use App\Notifications\Alert;
use App\Notifications\FormApproved;
use App\Notifications\FormRejected;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class FormRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(FilledForm $filledForm)
    {
        Gate::authorize('approve', $filledForm);

        $filledForm->load('raisedForm', 'filledFormFields.formField.field.options', 'filledFormFields.selectedOptions', 'filledFormFields.file', 'approvals.user.role:id,name');

        return inertia('Form/ViewForm', [
            'filledForm' => $filledForm,
            'type' => 'approvingForm'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FilledForm $filledForm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, FilledForm $filledForm)
    {
        Gate::authorize('approve', $filledForm);

        $validated = $request->validate([
            'status' => 'required|in:Approved,Rejected',
            'remarks' => 'nullable'
        ]);

        DB::transaction(function () use ($request, $validated, $filledForm) {
            $user = $request->user();
            $approvals = $filledForm->approvals->sortBy('stage');

            $approval = $approvals->where('status', 'Pending')->where('user_id', $user->id)->first();
            $approval->update($validated);
            //action taken
            $approval->alert()?->delete();

            //update application status
            if ($validated['status'] === 'Rejected') {
                $filledForm->approval_status = 'Rejected';
                $filledForm->alert?->delete();

                $filledForm->user->notify(new FormRejected($filledForm));
            } else if ($approvals->where('status', '!=', 'Approved')->isEmpty()) {
                $filledForm->approval_status = 'Approved';

                $filledForm->user->notify(new FormApproved($filledForm));
            }


            //update current stage if approved
            if ($validated['status'] === 'Approved') {
                $la = $approvals->where('status', 'Pending')->first();
                if ($la && $la->stage > $filledForm->current_stage) {
                    $filledForm->current_stage = $la->stage;
                    $message = "A {$filledForm->raisedForm->name} filled out by ({$user->role->name}) {$user->name} is ready for approval.";
                    $action_url = "/form/formRequest/{$filledForm->id}";

                    $approvals->where('stage', $la->stage)->where('status', 'Pending')->each(function ($approval) use ($message, $action_url, $request) {
                        $user = $approval->user;
                        if ($user) {
                            if ($request->user()->id !== $user->id) {
                                $user->notify(new Alert($message, $action_url, true));
                            }

                            $alert = new ModelsAlert(["alert_period" => "Twice a Day", "message" => $message, "action_url" => $action_url]);
                            $approval->alert()->save($alert);
                            $user->alerts()->attach($alert->id);
                        }
                    });
                }
            }

            $filledForm->save();
        });

        return redirect('/alert')->with('success', 'Form request action taken successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(FilledForm $filledForm)
    {
        //
    }
}
