<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class AlertController extends Controller
{
    public function index(Request $request)
    {
        $laAlerts = $request->user()->alerts()->get();
        $frfAlerts = new Collection();
        $request->user()->alertables->each(function ($roleField) use (&$frfAlerts) {
            $roleField->filledRoleFields()->has('alert')->with('alert')->get()->each(function ($frf) use (&$frfAlerts) {
                $frfAlerts->push($frf->alert);
            });
        });
        $alerts = $frfAlerts->merge($laAlerts);

        return inertia('Others/Alert', [
            'alerts' => $alerts->isNotEmpty() ? $alerts->toQuery()
                ->orderBy('updated_at', 'desc')
                ->paginate(10) :
                $request->user()
                ->alerts()
                ->orderBy('updated_at', 'desc')
                ->paginate(10)
        ]);
    }
}
