<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\FieldFile;
use App\Models\FilledRoleField;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\File;

class IssuanceController extends Controller
{
    public function index(Request $request)
    {
        Gate::authorize('viewAny', FilledRoleField::class);

        $roles = [];
        $request->user()->issuables()->with('role')->get()->each(function ($roleField) use (&$roles) {
            if (in_array($roleField->role->id, $roles) === false) {
                array_push($roles, $roleField->role->id);
            }
        });

        $filters = $request->all();

        return inertia('Issuance/IssueStaff', [
            'users' => User::with('role:id,name', 'departmentStaffDetail.department:id,name', 'department:id,name,hod_id')->whereHas('role', function (Builder $query) use ($roles) {
                $query->whereIn('id', $roles);
            })
                ->byName($filters['name'] ?? '')
                ->byDepartment($filters['department'] ?? 'All Department')
                ->orderBy('updated_at', 'desc')
                ->paginate(10),
            'departments' => Department::all(),
            'filters' => $filters,
        ]);
    }

    public function edit(Request $request, User $user)
    {
        $frfs = $user->filledRoleFields()->whereHas('roleField', function (Builder $query) use ($request) {
            $query->whereRelation('issuers', 'users.id', $request->user()->id);
        })->with('roleField.field', 'selectedOptions', 'file')->get();

        $frfs->each(function ($frf) {
            Gate::authorize('update', [FilledRoleField::class, $frf]);
        });

        $user->load('departmentStaffDetail', 'filledRoleFields', 'filledRoleFields.selectedOptions', 'filledRoleFields.roleField', 'filledRoleFields.roleField.field', 'filledRoleFields.file');

        return inertia('Issuance/IssueFields', [
            'filledRoleFields' => $frfs,
            'user' => $user
        ]);
    }

    public function update(Request $request, User $user)
    {
        $validated = $request->validate([
            'filled_role_fields' => 'array'
        ]);

        //validating role fields with a validator
        $requiredRoleFields = array_filter($validated['filled_role_fields'], function ($filledRoleField) {
            return $filledRoleField['roleField']['field']['required'];
        });
        $newRequiredRoleFields = [];
        $requiredRoleFieldsValidator = [];
        $validationMessages = [];
        foreach ($requiredRoleFields as $filledRoleField) {
            if (($filledRoleField['roleField']['field']['type'] === 'File' && (($filledRoleField['file'] && $filledRoleField['delete']) || !$filledRoleField['file'])) || $filledRoleField['roleField']['field']['type'] !== 'File') {
                $newId = $filledRoleField['roleField']['id'] . "id";
                $newRequiredRoleFields[$newId] = $filledRoleField['value'];

                if ($filledRoleField['roleField']['field']['type'] !== 'File') {
                    $requiredRoleFieldsValidator[$newId] = 'required';
                    $validationMessages[$newId . '.required'] = 'The ' . $filledRoleField['roleField']['field']['name'] . ' field is required!';
                } else {
                    $requiredRoleFieldsValidator[$newId] = ['required', File::types(['jpg', 'jpeg', 'png', 'bmp', 'webp', 'pdf', 'docx', 'doc'])
                        ->max('5mb')];
                    $validationMessages[$newId . '.required'] = 'The ' . $filledRoleField['roleField']['field']['name'] . ' field is required!';
                    $validationMessages[$newId . '.mimes'] = 'The ' . $filledRoleField['roleField']['field']['name'] . ' field must be a file of type: jpg, jpeg, png, bmp, webp, pdf, docx, doc.';
                    $validationMessages[$newId . '.max'] = 'The ' . $filledRoleField['roleField']['field']['name'] . ' field must not be greater than 5000 kilobytes.';
                }
            }
        }
        $roleFieldValidator = Validator::make($newRequiredRoleFields, $requiredRoleFieldsValidator, $validationMessages);
        if ($roleFieldValidator->fails()) {
            return back()
                ->withErrors($roleFieldValidator)
                ->withInput();
        }

        DB::transaction(function () use ($validated, $user) {
            foreach ($validated['filled_role_fields'] as $filledRoleField) {
                $frf = FilledRoleField::find($filledRoleField['id']);
                Gate::authorize('update', [FilledRoleField::class, $frf]);
                if ($filledRoleField['roleField']['field']['type'] === 'Checkbox') {
                    $frf->selectedOptions()->sync($filledRoleField['value'] ?? []);
                } else if ($filledRoleField['roleField']['field']['type'] === 'File') {
                    if ($filledRoleField['delete']) {
                        $frf->file?->delete();
                    }

                    if ($filledRoleField['value']) {
                        $frf->file?->delete();

                        $path = $filledRoleField['value']->store('fieldFiles', 'public');
                        $frf->file()->save(new FieldFile(['filename' => $path]));
                    }
                } else if ($filledRoleField['roleField']['field']['type'] === 'Date') {
                    $frf->update(['date_value' => $filledRoleField['value'], 'role_field_id' => $filledRoleField['roleField']['id'], 'user_id' => $user->id]);
                } else {
                    $frf->update(['value' => $filledRoleField['value'], 'role_field_id' => $filledRoleField['roleField']['id'], 'user_id' => $user->id]);
                }
            }
        });

        return redirect()->route('issuance.staff.index')->with('success', 'Field issued successfuly!');
    }
}
