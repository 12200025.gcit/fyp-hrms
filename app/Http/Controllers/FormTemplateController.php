<?php

namespace App\Http\Controllers;

use App\Models\Field;
use App\Models\FormField;
use App\Models\FormTemplate;
use App\Models\Option;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class FormTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        Gate::authorize('viewAny', FormTemplate::class);

        return inertia('Form/FormTemplates', [
            'formTemplates' => FormTemplate::orderBy('updated_at', 'desc')->paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        Gate::authorize('create', FormTemplate::class);

        return inertia('Form/CreateFormTemplate', [
            'users' => User::with('role:id,name')
                ->select('id', 'name', 'cid', 'role_id')
                ->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Gate::authorize('create', FormTemplate::class);

        $validated = $request->validate([
            'name' => 'required',
            'description' => 'nullable',
            'raisers' => 'array',
            'form_fields' => 'array'
        ]);

        DB::transaction(function () use ($request, $validated) {
            $formTemplate = FormTemplate::create([...$request->except('raisers', 'form_fields')]);

            $formTemplate->users()->sync(array_map(fn ($user) => $user['id'], $validated['raisers']));

            //form fields
            $form_fields = $validated['form_fields'];
            foreach ($form_fields as $index => $form_field) {
                //create field
                $field = Field::create(['name' => $form_field['name'], 'type' => $form_field['type'], 'required' => $form_field['required'], 'index' => $index]);
                FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

                //create options
                if (in_array($form_field['type'], ['Checkbox', 'Radio Button', 'Dropdown'])) {
                    $options = [];
                    foreach ($form_field['options'] as $optionIndex => $option) {
                        array_push($options, new Option(['value' => $option, 'index' => $optionIndex]));
                    }
                    $field->options()->saveMany($options);
                }
            }
        });

        return redirect()->route('form.formTemplate.index')->with('success', 'Form Template created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(FormTemplate $formTemplate)
    {
        Gate::authorize('view', [FormTemplate::class, $formTemplate]);

        $formTemplate->load('formFields.field.options');

        return inertia('Form/ViewFormTemplate', [
            'formTemplate' => $formTemplate,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FormTemplate $formTemplate)
    {
        Gate::authorize('update', [FormTemplate::class, $formTemplate]);

        $formTemplate->load('users', 'users.role:id,name');

        return inertia('Form/EditFormTemplate', [
            'formTemplate' => $formTemplate,
            'users' => User::with('role:id,name')
                ->select('id', 'name', 'cid', 'role_id')
                ->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, FormTemplate $formTemplate)
    {
        Gate::authorize('update', [FormTemplate::class, $formTemplate]);

        $validatedData = $request->validate([
            'name' => 'required',
            'description' => 'nullable',
            'raisers' => 'array'
        ]);

        DB::transaction(function () use ($request, $formTemplate, $validatedData) {
            $formTemplate->update([...$request->except('raisers')]);

            $formTemplate->users()->sync(array_map(fn ($user) => $user['id'], $validatedData['raisers']));
        });

        return redirect()->route('form.formTemplate.index')->with('success', 'Form Template updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(FormTemplate $formTemplate)
    {
    }

    public function destroyMany(Request $request)
    {
        Gate::authorize('deleteMany', FormTemplate::class);

        DB::transaction(function () use ($request) {
            $selected = $request->all()['selected'];

            FormTemplate::whereIn('id', $selected)->get()->each(function ($formTemplate) {
                $formTemplate->delete();
            });

            return redirect()->route('form.formTemplate.index')->with('success', 'Form Template(s) deleted successfuly!');
        });
    }
}
