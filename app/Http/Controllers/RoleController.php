<?php

namespace App\Http\Controllers;

use App\Models\Alert;
use App\Models\Role;
use App\Models\User;
use App\Models\Field;
use App\Models\FilledRoleField;
use App\Models\LeaveType;
use App\Models\Option;
use App\Models\RoleField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        Gate::authorize('viewAny', Role::class);

        return inertia('Employee/Roles', [
            'roles' => Role::orderBy('updated_at', 'desc')->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        Gate::authorize('create', Role::class);

        return inertia('Employee/CreateRole', [
            'users' => User::with('role:id,name')->select('id', 'name', 'cid', 'role_id')->get(),
            'leaveTypes' => LeaveType::select('id', 'type')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Gate::authorize('create', Role::class);

        $validated = $request->validate([
            'name' => 'required',
            'description' => 'nullable',
            'create_role' => 'required|boolean',
            'read_role' => 'required|boolean',
            'update_role' => 'required|boolean',
            'delete_role' => 'required|boolean',
            'create_user' => 'required|boolean',
            'read_user' => 'required|boolean',
            'update_user' => 'required|boolean',
            'delete_user' => 'required|boolean',
            'create_leave_type' => 'required|boolean',
            'read_leave_type' => 'required|boolean',
            'update_leave_type' => 'required|boolean',
            'delete_leave_type' => 'required|boolean',
            'create_department' => 'required|boolean',
            'read_department' => 'required|boolean',
            'update_department' => 'required|boolean',
            'delete_department' => 'required|boolean',
            'create_form_template' => 'required|boolean',
            'read_form_template' => 'required|boolean',
            'update_form_template' => 'required|boolean',
            'delete_form_template' => 'required|boolean',
            'observer_eligibility' => 'required|boolean',
            'roster_eligibility' => 'required|boolean',
            'view_overall_roster' => 'required|boolean',
            'manage_overall_roster' => 'required|boolean',
            'manage_department_shift' => 'required|boolean',
            'admin_dashboard_eligibility' => 'required|boolean',
            'performance_eligibility' => 'required|boolean',
            'role_fields' => 'array',
            'eligible_leaves' => 'array'
        ]);

        // dd($request->all()['role_fields']);
        DB::transaction(function () use ($request, $validated) {
            $role = new Role($request->except(['role_fields', 'eligible_leaves']));
            $role->save();
            $role->leaveTypes()->sync(array_map(fn ($leaveType) => $leaveType['id'], $validated['eligible_leaves']));
            $role_fields = $validated['role_fields'];
            foreach ($role_fields as $index => $role_field) {
                //create field
                $field = Field::create(['name' => $role_field['name'], 'type' => $role_field['type'], 'required' => $role_field['required'], 'index' => $index]);
                $roleField = RoleField::create(['issuable' => $role_field['issuable'], 'alertable' => $role_field['alertable'], 'alert_period' => $role_field['alert_period'], 'foreigner_field' => $role_field['foreigner_field'], 'field_id' => $field->id, 'role_id' => $role->id]);

                //create options
                if (in_array($role_field['type'], ['Checkbox', 'Radio Button', 'Dropdown'])) {
                    $options = [];
                    foreach ($role_field['options'] as $optionIndex => $option) {
                        array_push($options, new Option(['value' => $option, 'index' => $optionIndex]));
                    }
                    $field->options()->saveMany($options);
                }

                //alert relationship
                if ($role_field['alertable']) {
                    $roleField->alertSubjects()->sync(array_map(fn ($subject) => $subject['id'], $role_field['alert_subjects']));
                }

                //issue relationship
                if ($role_field['issuable']) {
                    $roleField->issuers()->sync(array_map(fn ($issuer) => $issuer['id'], $role_field['issuers']));
                }
            }
        });

        return redirect()->route('employee.role.index')->with('success', 'Role created successfuly!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Role $role)
    {
        Gate::authorize('view', [Role::class, $role]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Role $role)
    {
        Gate::authorize('update', [Role::class, $role]);

        $role->load('fields', 'fields.issuers', 'fields.issuers.role', 'fields.alertSubjects', 'fields.alertSubjects.role', 'fields.field', 'fields.field.options', 'leaveTypes:id,type');

        return inertia('Employee/EditRole', [
            'users' => User::with('role:id,name')->select('id', 'name', 'cid', 'role_id')->get(),
            'leaveTypes' => LeaveType::select('id', 'type')->get(),
            'role' => $role
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Role $role)
    {
        Gate::authorize('update', [Role::class, $role]);

        $validated = $request->validate([
            'name' => 'required',
            'description' => 'nullable',
            'create_role' => 'required|boolean',
            'read_role' => 'required|boolean',
            'update_role' => 'required|boolean',
            'delete_role' => 'required|boolean',
            'create_user' => 'required|boolean',
            'read_user' => 'required|boolean',
            'update_user' => 'required|boolean',
            'delete_user' => 'required|boolean',
            'create_leave_type' => 'required|boolean',
            'read_leave_type' => 'required|boolean',
            'update_leave_type' => 'required|boolean',
            'delete_leave_type' => 'required|boolean',
            'create_department' => 'required|boolean',
            'read_department' => 'required|boolean',
            'update_department' => 'required|boolean',
            'delete_department' => 'required|boolean',
            'create_form_template' => 'required|boolean',
            'read_form_template' => 'required|boolean',
            'update_form_template' => 'required|boolean',
            'delete_form_template' => 'required|boolean',
            'observer_eligibility' => 'required|boolean',
            'roster_eligibility' => 'required|boolean',
            'view_overall_roster' => 'required|boolean',
            'manage_overall_roster' => 'required|boolean',
            'manage_department_shift' => 'required|boolean',
            'admin_dashboard_eligibility' => 'required|boolean',
            'performance_eligibility' => 'required|boolean',
            'role_fields' => 'array',
            'eligible_leaves' => 'array'
        ]);

        DB::transaction(function () use ($request, $role, $validated) {
            $role->update($request->except(['role_fields', 'eligible_leaves']));
            $role->leaveTypes()->sync(array_map(fn ($leaveType) => $leaveType['id'], $validated['eligible_leaves']));
            $role_fields = $request->all()['role_fields'];

            //deleting removed RoleFields
            $role->fields()->whereNotIn('id', array_map(fn ($role_field) => $role_field['id'], $role_fields))->get()->each(function ($role_field) {
                $role_field->delete();
            });

            foreach ($role_fields as $index => $role_field) {
                if ($role_field['id']) {
                    //update exisiting RoleField
                    $roleField = RoleField::find($role_field['id']);
                    if ($roleField->alertable && !$role_field['alertable']) {
                        $roleField->filledRoleFields->each(function ($frf) {
                            $frf->alert()->delete();
                        });
                    } else {
                        $fn = $role_field['name'];
                        $role->users->each(function ($user) use ($role, $fn, $roleField, $role_field) {
                            $frf = $user->filledRoleFields()->whereRelation('roleField', 'id', $roleField->id)->first();
                            if (!$frf) {
                                $frf = FilledRoleField::create(['role_field_id' => $roleField->id, 'user_id' => $user->id]);
                            }

                            if ($role_field['alertable']) {
                                $frf->alert()?->delete();
                                $alert = new Alert(["alert_period" => "Daily", "message" => "({$role->name}) {$user->name}'s {$fn} is missing!", "action_url" => "/employee/user/{$user->id}/edit"]);
                                $frf->alert()->save($alert);
                            }
                        });
                    }

                    $roleField->update(['issuable' => $role_field['issuable'], 'alertable' => $role_field['alertable'], 'alert_period' => $role_field['alert_period'], 'foreigner_field' => $role_field['foreigner_field']]);

                    //alert relationship
                    if ($role_field['alertable']) {
                        $roleField->alertSubjects()->sync(array_map(fn ($subject) => $subject['id'], $role_field['alert_subjects']));
                    } else {
                        $roleField->alertSubjects()->detach();
                    }

                    //issue relationship
                    if ($role_field['issuable']) {
                        $roleField->issuers()->sync(array_map(fn ($issuer) => $issuer['id'], $role_field['issuers']));
                    } else {
                        $roleField->issuers()->detach();
                    }

                    //do not mass update Field, update with save() inorder for it to fire the updating event
                    $field = $roleField->field;
                    $field->name = $role_field['name'];
                    $field->type = $role_field['type'];
                    $field->required = $role_field['required'];
                    $field->index = $index;
                    $field->save();


                    if (in_array($role_field['type'], ['Checkbox', 'Radio Button', 'Dropdown'])) {
                        //deleting removed options
                        $field->options()->whereNotIn('id', array_map(fn ($option) => $option['id'], $role_field['options']))->get()->each(function ($option) {
                            $option->delete();
                        });

                        //updating or creating options
                        $options = [];
                        foreach ($role_field['options'] as $optionIndex => $option) {
                            if ($option['id']) {
                                Option::find($option['id'])->update(['value' => $option['value'], 'index' => $optionIndex]);
                            } else {
                                array_push($options, new Option(['value' => $option['value'], 'index' => $optionIndex]));
                            }
                        }
                        $field->options()->saveMany($options);
                    }
                } else {
                    //create field
                    $field = Field::create(['name' => $role_field['name'], 'type' => $role_field['type'], 'required' => $role_field['required'], 'index' => $index]);
                    $roleField = RoleField::create(['issuable' => $role_field['issuable'], 'alertable' => $role_field['alertable'], 'alert_period' => $role_field['alert_period'], 'foreigner_field' => $role_field['foreigner_field'], 'field_id' => $field->id, 'role_id' => $role->id]);

                    //create options
                    if (in_array($role_field['type'], ['Checkbox', 'Radio Button', 'Dropdown'])) {
                        $options = [];
                        foreach ($role_field['options'] as $optionIndex => $option) {
                            array_push($options, new Option(['value' => $option['value'], 'index' => $optionIndex]));
                        }
                        $field->options()->saveMany($options);
                    }

                    //alert relationship
                    if ($role_field['alertable']) {
                        $roleField->alertSubjects()->sync(array_map(fn ($subject) => $subject['id'], $role_field['alert_subjects']));
                    }

                    $fn = $role_field['name'];
                    $role->users->each(function ($user) use ($role, $fn, $roleField, $role_field) {
                        $frf = FilledRoleField::create(['role_field_id' => $roleField->id, 'user_id' => $user->id]);

                        if ($role_field['alertable']) {
                            $alert = new Alert(["alert_period" => "Daily", "message" => "({$role->name}) {$user->name}'s {$fn} is missing!", "action_url" => "/employee/user/{$user->id}/edit"]);
                            $frf->alert()->save($alert);
                        }
                    });


                    //issue relationship
                    if ($role_field['issuable']) {
                        $roleField->issuers()->sync(array_map(fn ($issuer) => $issuer['id'], $role_field['issuers']));
                    }
                }
            }
        });

        return redirect()->route('employee.role.index')->with('success', 'Role updated successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Role $role)
    {
        Gate::authorize('delete', [Role::class, $role]);
    }

    public function destroyMany(Request $request)
    {
        Gate::authorize('deleteMany', Role::class);

        DB::transaction(function () use ($request) {
            $selected_roles = $request->all()['selected_roles'];

            Role::whereIn('id', $selected_roles)->get()->each(function ($role) {
                $role->delete();
            });

            return redirect()->route('employee.role.index')->with('success', 'Role(s) deleted successfuly!');
        });
    }
}
