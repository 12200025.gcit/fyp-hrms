<?php

namespace App\Helper;

use App\Models\Alert as ModelsAlert;
use App\Models\User;
use App\Notifications\Alert;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Notification;

class Helper
{
  static public function getLeaveBalance(User $user, $leaveTypeId = null)
  {
    $now = Carbon::now();

    return $user->role->leaveTypes->filter(fn ($lt) => $leaveTypeId ? ($lt->id === $leaveTypeId) : True)->map(function ($leaveType, $key) use ($now, $user) {
      $leaveStayed = 0;
      $phs = [];
      if ($leaveType->balance_type === 'Public Holiday') {
        $user
          ->leaveApplications()
          ->where('approval_status', 'Approved')
          ->whereRelation('leaveType', 'balance_type', 'Public Holiday')
          ->whereYear('created_at', $now->year)->get()->each(function ($la) use (&$phs) {
            $phs = [...$phs, ...array_map(fn ($laph) => $laph['name'], $la->public_holidays)];
          });
      } else {
        $user->leaveApplications()->whereRelation('leaveType', 'type', $leaveType->type)->where('approval_status', 'Approved')->get()->each(function ($la) use (&$leaveStayed, $now, $leaveType) {
          $sdt = new DateTime($la->start_date);
          $edt = new DateTime($la->end_date);

          if ($leaveType->balance_type === 'Number' && $leaveType->carry_forward) {
            $leaveStayed += $edt->diff($sdt)->days + 1;
          } else {
            if ($sdt->format('Y') == $now->year && $edt->format('Y') == $now->year) {
              $leaveStayed += $edt->diff($sdt)->days + 1;
            } else if ($sdt->format('Y') == $now->year) {
              $leaveStayed += $sdt->diff(Carbon::create($now->year, 1, 1)->endOfyear->toDateTime())->days + 1;
            } else if ($edt->format('Y') == $now->year) {
              $leaveStayed += Carbon::create($now->year, 1, 1)->toDateTime()->diff($edt)->days + 1;
            }
          }
        });
      }

      if ($leaveType->balance_type === 'Fixed') {
        return ["leave_type" => $leaveType, "balance" => ($leaveType->fixed_balance) - $leaveStayed];
      } else if ($leaveType->balance_type === 'Public Holiday') {
        return ["leave_type" => $leaveType, "balance" => (count($leaveType->public_holidays)) - count($phs)];
      } else if ($leaveType->balance_type === 'Unlimited') {
        return ["leave_type" => $leaveType, "balance" => "Unlimited. {$leaveStayed} Stayed"];
      } else if ($leaveType->balance_type === 'Number' && $leaveType->carry_forward) {
        $date1 = $user->created_at;
        $date2 = $now->toDateString();

        $ts1 = strtotime($date1);
        $ts2 = strtotime($date2);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

        // dd($diff, $date1, $date2);

        return ["leave_type" => $leaveType, "balance" => ($leaveType->monthly_balance_accumulation_rate * $diff ) - $leaveStayed + ($user->carry_forward_starting_balances[$leaveType->id] ?? 0)];
      } else {
        return ["leave_type" => $leaveType, "balance" => ($leaveType->monthly_balance_accumulation_rate * $now->month) - $leaveStayed];
      }
    });
  }

  public static function sendAlert($alert_period)
  {
      ModelsAlert::all()->each(function (ModelsAlert $alert) use ($alert_period) {
          if ($alert->alertable_type === 'App\Models\Approval' || $alert->alertable_type === 'App\Models\FilledForm') {
              if ($alert->alert_period === $alert_period || $alert_period === 'Any') {
                  Notification::send($alert->users, new Alert($alert->message, $alert->action_url));
              }
          } else if ($alert->alertable_type === 'App\Models\FilledRoleField') {
              $frf = $alert->alertable;
              if ($frf->alert_period === $alert_period || $alert_period === 'Any') {
                  Notification::send($frf->roleField->alertSubjects, new Alert($alert->message, $alert->action_url));
              }
          }
      });
  }
}
