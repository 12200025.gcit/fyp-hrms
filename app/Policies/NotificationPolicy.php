<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Notifications\DatabaseNotification;

class NotificationPolicy
{
    public function read(User $user, DatabaseNotification $databaseNotification): bool
    {
        return $user->id == $databaseNotification->notifiable_id;
    }
}
