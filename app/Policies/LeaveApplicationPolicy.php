<?php

namespace App\Policies;

use App\Models\LeaveApplication;
use App\Models\LeaveType;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class LeaveApplicationPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        if ($user->role->leaveTypes->isNotEmpty() || $user->leaveApplications->isNotEmpty()) {
            return true;
        } else {
            return false;
        }
    }


    public function viewLeaveRequests(User $user): bool
    {
        if ($user->approvalStages->isNotEmpty() || $user->approvals->isNotEmpty() || $user->department ? True : False) {
            return true;
        } else {
            return false;
        }
    }


    public function approveLeave(User $user, LeaveApplication $leaveApplication): bool
    {
        if ($leaveApplication->approvals()->where('user_id', $user->id)->where('status', 'Pending')->get()->isNotEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, LeaveApplication $leaveApplication): bool
    {
        return $user->id === $leaveApplication->user->id || in_array($user->id, $leaveApplication->approvals->map(fn ($la) => $la->user_id)->toArray());
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        if ($user->role->leaveTypes->isNotEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public function store(User $user, $leaveTypeId): bool
    {
        if ($user->role->leaveTypes->contains($leaveTypeId)) {
            return true;
        } else {
            return false;
        }
    }
}
