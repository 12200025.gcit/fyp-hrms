<?php

namespace App\Policies;

use App\Models\ObservationNote;
use App\Models\User;

class ObservationNotePolicy
{
    public function viewAnyObservations(User $user): bool
    {
        return $user->role->observer_eligibility;
    }

    public function viewAnyPerformance(User $user): bool
    {
        return $user->role->performance_eligibility;
    }

    public function view(User $user, ObservationNote $observationNote): bool
    {
        return $observationNote->observer->id === $user->id || $observationNote->subject->id === $user->id || $user->role->read_user;
    }

    public function create(User $user): bool
    {
        return $user->role->observer_eligibility;
    }

    public function deleteMany(User $user): bool
    {
        return $user->role->observer_eligibility;
    }
}
