<?php

namespace App\Policies;

use App\Models\FilledForm;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class FilledFormPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return $user->filledForms->isNotEmpty() || $user->fillables->isNotEmpty();
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, FilledForm $filledForm): bool
    {
        // return $filledForm->user->id === $user->id;
        return $user->role->read_user || $filledForm->user->id === $user->id || $filledForm->raisedForm->user?->id === $user->id || $filledForm->sharedUsers->where('id', $user->id)->isNotEmpty();
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, FilledForm $filledForm): bool
    {
        return ($filledForm->user->id === $user->id || $filledForm->raisedForm->user?->id === $user->id) && !$filledForm->submitted;
    }

    public function share(User $user, FilledForm $filledForm): bool
    {
        return $user->role->read_user;
    }

    public function viewAnyShared(User $user): bool
    {
        return $user->sharedFilledForms->isNotEmpty();
    }

    public function approve(User $user, FilledForm $filledForm): bool
    {
        if ($filledForm->approvals()->where('user_id', $user->id)->where('status', 'Pending')->get()->isNotEmpty()) {
            return true;
        } else {
            return false;
        }
    }
}
