<?php

namespace App\Policies;

use App\Models\RaisedForm;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class RaisedFormPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        // return $user->role->read_raised_form;
        return $user->formTemplates->isNotEmpty();
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, RaisedForm $raisedForm): bool
    {
        return $raisedForm->formTemplate->users()->get()->where('id', $user->id)->isNotEmpty();
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return $user->formTemplates->isNotEmpty();
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, RaisedForm $raisedForm): bool
    {
        return $raisedForm->formTemplate->users()->get()->where('id', $user->id)->isNotEmpty();
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, RaisedForm $raisedForm): bool
    {
        return $raisedForm->formTemplate->users()->get()->where('id', $user->id)->isNotEmpty();
    }


    public function deleteMany(User $user): bool
    {
        return $user->formTemplates->isNotEmpty();
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, RaisedForm $raisedForm): bool
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, RaisedForm $raisedForm): bool
    {
        return false;
    }
}
