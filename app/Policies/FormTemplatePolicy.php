<?php

namespace App\Policies;

use App\Models\FormTemplate;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class FormTemplatePolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return $user->role->read_form_template;
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, FormTemplate $formTemplate): bool
    {
        return $user->role->read_form_template;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return $user->role->create_form_template;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, FormTemplate $formTemplate): bool
    {
        return $user->role->update_form_template;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, FormTemplate $formTemplate): bool
    {
        return $user->role->delete_form_template;
    }


    public function deleteMany(User $user): bool
    {
        return $user->role->delete_form_template;
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, FormTemplate $formTemplate): bool
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, FormTemplate $formTemplate): bool
    {
        return false;
    }
}
