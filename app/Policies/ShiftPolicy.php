<?php

namespace App\Policies;

use App\Models\Department;
use App\Models\Shift;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class ShiftPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        // return $user->department ? True : False;
        return $user->role->manage_department_shift;
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Shift $shift): bool
    {
        // return $user->department?->id === $shift->department->id;
        return $user->role->manage_department_shift;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        // return $user->department ? True : False;
        return $user->role->manage_department_shift;
    }


    public function store(User $user, Department $department): bool
    {
        // return $user->department?->id === $department->id;
        return $user->role->manage_department_shift;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Shift $shift): bool
    {
        // return $user->department?->id === $shift->department->id;
        return $user->role->manage_department_shift;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Shift $shift): bool
    {
        // return $user->department?->id === $shift->department->id;
        return $user->role->manage_department_shift;
    }
}
