<?php

namespace App\Policies;

use App\Models\FilledRoleField;
use App\Models\User;

class FilledRoleFieldPolicy
{
    /**
     * Create a new policy instance.
     */
    public function viewAny(User $user): bool
    {
        return $user->issuables->isNotEmpty();
    }

    public function update(User $user, FilledRoleField $frf): bool
    {
        return $frf->roleField->issuers()->where('users.id', $user->id)->get()->isNotEmpty();
    }
}
