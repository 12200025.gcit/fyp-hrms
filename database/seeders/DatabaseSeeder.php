<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\DepartmentStaffDetail;
use App\Models\Role;
use App\Models\User;
use App\Models\Field;
use App\Models\FilledRoleField;
use App\Models\FormTemplate;
use App\Models\ApprovalStage;
use App\Models\FormField;
use App\Models\LeaveType;
use App\Models\Option;
use App\Models\RoleField;
use App\Models\Shift;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Row;

class DatabaseSeeder extends Seeder
{
    public function createMasterDetails(Role $role)
    {
        $mastelDetails = [];
        //Offer Letter
        $field = Field::create(['name' => 'Offer Letter', 'type' => 'File', 'required' => false, 'index' => 0]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Bond Document (signed with legal stamp) and signature of guarantor
        $field = Field::create(['name' => 'Bond Document', 'type' => 'File', 'required' => false, 'index' => 1]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Employment Form (with passport photo)
        $field = Field::create(['name' => 'Employment Form', 'type' => 'File', 'required' => false, 'index' => 2]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Locker Undertaking Form - signed
        $field = Field::create(['name' => 'Locker Undertaking Form', 'type' => 'File', 'required' => false, 'index' => 3]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Non-Disclosure of Information Form – signed
        $field = Field::create(['name' => 'Non-Disclosure of Information Form', 'type' => 'File', 'required' => false, 'index' => 4]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Medical Certificate – Fit to Work (1 week after joining)
        $field = Field::create(['name' => 'Medical Certificate', 'type' => 'File', 'required' => false, 'index' => 5]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Copy of CID/passport
        $field = Field::create(['name' => 'Copy of CID/Passport', 'type' => 'File', 'required' => false, 'index' => 6]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Work permit copy (for expatriate employees)
        $field = Field::create(['name' => 'Work Permit Copy', 'type' => 'File', 'required' => false, 'index' => 7]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => true, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Transcript copies and certificates/reference letters
        $field = Field::create(['name' => 'Transcript copies and certificates/reference letters', 'type' => 'File', 'required' => false, 'index' => 8]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //CV
        $field = Field::create(['name' => 'CV', 'type' => 'File', 'required' => false, 'index' => 9]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //CID/Passport


        //Work Permit Number
        $field = Field::create(['name' => 'Work Permit Number', 'type' => 'Number', 'required' => false, 'index' => 10]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => true, 'alert_period' => 'Week', 'foreigner_field' => true, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Work Permit Date of Issue
        $field = Field::create(['name' => 'Work Permit Date of Issue', 'type' => 'Date', 'required' => false, 'index' => 11]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => true, 'alert_period' => 'Week', 'foreigner_field' => true, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Work Permit Date of Expiry
        $field = Field::create(['name' => 'Work Permit Date of Expiry', 'type' => 'Date', 'required' => false, 'index' => 12]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => true, 'alert_period' => 'Week', 'foreigner_field' => true, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Education
        $field = Field::create(['name' => 'Education', 'type' => 'Text Area', 'required' => false, 'index' => 13]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Prior Work Experience
        $field = Field::create(['name' => 'Prior Work Experience', 'type' => 'Text Area', 'required' => false, 'index' => 14]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Health conditions
        $field = Field::create(['name' => 'Health conditions', 'type' => 'Text Area', 'required' => false, 'index' => 15]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        //Marital status
        $field = Field::create(['name' => 'Marital Status', 'type' => 'Radio Button', 'required' => false, 'index' => 16]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        $options = [];
        array_push($options, new Option(['value' => 'Married', 'index' => 0]));
        array_push($options, new Option(['value' => 'Single', 'index' => 1]));
        array_push($options, new Option(['value' => 'Divorced', 'index' => 2]));
        $field->options()->saveMany($options);

        //Mobile phone
        $field = Field::create(['name' => 'Mobile number', 'type' => 'Text Area', 'required' => false, 'index' => 17]);
        $roleField = RoleField::create(['issuable' => false, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        $field = Field::create(['name' => 'Hook Kira', 'type' => 'Text Area', 'required' => false, 'index' => 18]);
        $roleField = RoleField::create(['issuable' => true, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        $field = Field::create(['name' => 'Plain Kira', 'type' => 'Text Area', 'required' => false, 'index' => 19]);
        $roleField = RoleField::create(['issuable' => true, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        $field = Field::create(['name' => 'Euro Tego', 'type' => 'Text Area', 'required' => false, 'index' => 20]);
        $roleField = RoleField::create(['issuable' => true, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        $field = Field::create(['name' => 'Dragon Tego', 'type' => 'Text Area', 'required' => false, 'index' => 21]);
        $roleField = RoleField::create(['issuable' => true, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        $field = Field::create(['name' => 'Shoe Pair', 'type' => 'Text Area', 'required' => false, 'index' => 22]);
        $roleField = RoleField::create(['issuable' => true, 'alertable' => false, 'alert_period' => null, 'foreigner_field' => false, 'field_id' => $field->id, 'role_id' => $role->id]);
        array_push($mastelDetails, $roleField);

        $role->fields()->saveMany($mastelDetails);
    }
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        //ROLES
        //super admin
        // Role::factory(1)->create([
        //     'name' => 'Super Admin',

        //     'create_role' => TRUE,
        //     'read_role' => TRUE,
        //     'update_role' => TRUE,
        //     'delete_role' => TRUE,

        //     'create_user' => TRUE,
        //     'read_user' => TRUE,
        //     'update_user' => TRUE,
        //     'delete_user' => TRUE,

        //     'create_leave_type' => TRUE,
        //     'read_leave_type' => TRUE,
        //     'update_leave_type' => TRUE,
        //     'delete_leave_type' => TRUE,

        //     'create_department' => TRUE,
        //     'read_department' => TRUE,
        //     'update_department' => TRUE,
        //     'delete_department' => TRUE,

        //     'create_form_template' => TRUE,
        //     'read_form_template' => TRUE,
        //     'update_form_template' => TRUE,
        //     'delete_form_template' => TRUE,

        //     'roster_eligibility' => TRUE,
        //     'view_overall_roster' => TRUE,
        //     'manage_overall_roster' => TRUE,
        //     'manage_department_shift' => TRUE,
        //     'admin_dashboard_eligibility' => TRUE,
        //     'performance_eligibility' => TRUE,
        //     'observer_eligibility' => TRUE,
        //     'view_attendance' => TRUE,
        // ]);

        //EM
        Role::factory(1)->create([
            'name' => 'Executive Manager',

            'create_role' => FALSE,
            'read_role' => TRUE,
            'update_role' => FALSE,
            'delete_role' => FALSE,

            'create_user' => FALSE,
            'read_user' => TRUE,
            'update_user' => FALSE,
            'delete_user' => FALSE,

            'create_leave_type' => FALSE,
            'read_leave_type' => FALSE,
            'update_leave_type' => FALSE,
            'delete_leave_type' => FALSE,

            'create_department' => FALSE,
            'read_department' => FALSE,
            'update_department' => FALSE,
            'delete_department' => FALSE,

            'create_form_template' => FALSE,
            'read_form_template' => FALSE,
            'update_form_template' => FALSE,
            'delete_form_template' => FALSE,

            'roster_eligibility' => FALSE,
            'view_overall_roster' => TRUE,
            'manage_overall_roster' => FALSE,
            'manage_department_shift' => FALSE,
            'admin_dashboard_eligibility' => TRUE,
            'performance_eligibility' => FALSE,
            'observer_eligibility' => TRUE,
            'view_attendance' => TRUE,
        ]);

        //ICT
        Role::factory(1)->create([
            'name' => 'ICT',

            'create_role' => TRUE,
            'read_role' => TRUE,
            'update_role' => TRUE,
            'delete_role' => TRUE,

            'create_user' => TRUE,
            'read_user' => TRUE,
            'update_user' => TRUE,
            'delete_user' => TRUE,

            'create_leave_type' => TRUE,
            'read_leave_type' => TRUE,
            'update_leave_type' => TRUE,
            'delete_leave_type' => TRUE,

            'create_department' => TRUE,
            'read_department' => TRUE,
            'update_department' => TRUE,
            'delete_department' => TRUE,

            'create_form_template' => TRUE,
            'read_form_template' => TRUE,
            'update_form_template' => TRUE,
            'delete_form_template' => TRUE,

            'roster_eligibility' => TRUE,
            'view_overall_roster' => FALSE,
            'manage_overall_roster' => FALSE,
            'manage_department_shift' => TRUE,
            'admin_dashboard_eligibility' => FALSE,
            'performance_eligibility' => TRUE,
            'observer_eligibility' => FALSE,
            'view_attendance' => FALSE
        ])->each(function (Role $role) {
            $this->createMasterDetails($role);
        });

        //HR
        Role::factory(1)->create([
            'name' => 'Human Resource',

            'create_role' => FALSE,
            'read_role' => FALSE,
            'update_role' => FALSE,
            'delete_role' => FALSE,

            'create_user' => TRUE,
            'read_user' => TRUE,
            'update_user' => TRUE,
            'delete_user' => TRUE,

            'create_leave_type' => FALSE,
            'read_leave_type' => FALSE,
            'update_leave_type' => FALSE,
            'delete_leave_type' => FALSE,

            'create_department' => FALSE,
            'read_department' => FALSE,
            'update_department' => FALSE,
            'delete_department' => FALSE,

            'create_form_template' => FALSE,
            'read_form_template' => FALSE,
            'update_form_template' => FALSE,
            'delete_form_template' => FALSE,

            'roster_eligibility' => TRUE,
            'view_overall_roster' => FALSE,
            'manage_overall_roster' => FALSE,
            'manage_department_shift' => TRUE,
            'admin_dashboard_eligibility' => FALSE,
            'performance_eligibility' => TRUE,
            'observer_eligibility' => TRUE,
            'view_attendance' => TRUE,
        ])->each(function (Role $role) {
            $this->createMasterDetails($role);
        });

        //hod
        Role::factory(1)->create([
            'name' => 'HOD',

            'create_role' => FALSE,
            'read_role' => FALSE,
            'update_role' => FALSE,
            'delete_role' => FALSE,

            'create_user' => FALSE,
            'read_user' => FALSE,
            'update_user' => FALSE,
            'delete_user' => FALSE,

            'create_leave_type' => FALSE,
            'read_leave_type' => FALSE,
            'update_leave_type' => FALSE,
            'delete_leave_type' => FALSE,

            'create_department' => FALSE,
            'read_department' => FALSE,
            'update_department' => FALSE,
            'delete_department' => FALSE,

            'create_form_template' => FALSE,
            'read_form_template' => FALSE,
            'update_form_template' => FALSE,
            'delete_form_template' => FALSE,

            'roster_eligibility' => TRUE,
            'view_overall_roster' => FALSE,
            'manage_overall_roster' => FALSE,
            'manage_department_shift' => TRUE,
            'admin_dashboard_eligibility' => FALSE,
            'performance_eligibility' => TRUE,
            'observer_eligibility' => TRUE,
            'view_attendance' => TRUE,
        ])->each(function (Role $role) {
            $this->createMasterDetails($role);
        });

        //employee
        Role::factory(1)->create([
            'name' => 'Employee',

            'create_role' => FALSE,
            'read_role' => FALSE,
            'update_role' => FALSE,
            'delete_role' => FALSE,

            'create_user' => FALSE,
            'read_user' => FALSE,
            'update_user' => FALSE,
            'delete_user' => FALSE,

            'create_leave_type' => FALSE,
            'read_leave_type' => FALSE,
            'update_leave_type' => FALSE,
            'delete_leave_type' => FALSE,

            'create_department' => FALSE,
            'read_department' => FALSE,
            'update_department' => FALSE,
            'delete_department' => FALSE,

            'create_form_template' => FALSE,
            'read_form_template' => FALSE,
            'update_form_template' => FALSE,
            'delete_form_template' => FALSE,

            'roster_eligibility' => TRUE,
            'view_overall_roster' => FALSE,
            'manage_overall_roster' => FALSE,
            'manage_department_shift' => FALSE,
            'admin_dashboard_eligibility' => FALSE,
            'performance_eligibility' => TRUE,
            'observer_eligibility' => FALSE,
            'view_attendance' => FALSE,
        ])->each(function (Role $role) {
            $this->createMasterDetails($role);
        });

        //employee contract
        Role::factory(1)->create([
            'name' => 'Employee Contract',

            'create_role' => FALSE,
            'read_role' => FALSE,
            'update_role' => FALSE,
            'delete_role' => FALSE,

            'create_user' => FALSE,
            'read_user' => FALSE,
            'update_user' => FALSE,
            'delete_user' => FALSE,

            'create_leave_type' => FALSE,
            'read_leave_type' => FALSE,
            'update_leave_type' => FALSE,
            'delete_leave_type' => FALSE,

            'create_department' => FALSE,
            'read_department' => FALSE,
            'update_department' => FALSE,
            'delete_department' => FALSE,

            'create_form_template' => FALSE,
            'read_form_template' => FALSE,
            'update_form_template' => FALSE,
            'delete_form_template' => FALSE,

            'roster_eligibility' => FALSE,
            'view_overall_roster' => FALSE,
            'manage_overall_roster' => FALSE,
            'manage_department_shift' => FALSE,
            'admin_dashboard_eligibility' => FALSE,
            'performance_eligibility' => FALSE,
            'observer_eligibility' => FALSE,
            'view_attendance' => FALSE,
        ]);

        //USERS
        //superadmin
        // User::factory(1)->create([
        //     'email' => 'test@example.com',
        //     'password' => 'password',
        //     'carry_forward_starting_balances' => [],
        //     'role_id' => Role::where('name', 'Super Admin')->get()->first()->id
        // ]);

        //EM
        User::factory(1)->create([
            'name' => "Pema Dorjee",
            'email' => 'em@example.com',
            'password' => 'password',
            'carry_forward_starting_balances' => [],
            'role_id' => Role::where('name', 'Executive Manager')->get()->first()->id
        ])->each(function (User $user) {
            $user->role->fields->each(function (RoleField $rf) use ($user) {
                FilledRoleField::create(['role_field_id' => $rf->id, 'user_id' => $user->id]);
            });
        });

        //ICT
        User::factory(1)->create([
            'name' => "Namgay Wangchuk",
            'email' => 'ict@example.com',
            'password' => 'password',
            'carry_forward_starting_balances' => [],
            'role_id' => Role::where('name', 'ICT')->get()->first()->id
        ])->each(function (User $user) {
            $user->role->fields->each(function (RoleField $rf) use ($user) {
                FilledRoleField::create(['role_field_id' => $rf->id, 'user_id' => $user->id]);
            });
        });

        //HR
        User::factory(1)->create([
            'name' => 'Chandra Chhetri',
            'email' => 'hr@example.com',
            'password' => 'password',
            'carry_forward_starting_balances' => [],
            'role_id' => Role::where('name', 'Human Resource')->get()->first()->id
        ])->each(function (User $user) {
            $user->role->fields->each(function (RoleField $rf) use ($user) {
                FilledRoleField::create(['role_field_id' => $rf->id, 'user_id' => $user->id]);
            });

            Department::create(['name' => "Administration and Finance (AFD)", 'hod_id' => $user->id]);
        });

        //CONTRACT
        User::factory(1)->create([
            'email' => 'contract@example.com',
            'password' => 'password',
            'carry_forward_starting_balances' => [],
            'role_id' => Role::where('name', 'Employee Contract')->get()->first()->id
        ]);

        $departments = ['Front Office (FO)', "House Keeping (HK)", "Food & Beverage Service (FNB)", "Food & Beverage Production (FBP)"];
        $email = ['fo', 'hk', 'fnb', 'fbp'];
        $name = ['Hem Mongar', 'Zam', 'Bishnu', 'Dhan Kumar Alley'];
        User::factory(4)->create([
            'carry_forward_starting_balances' => [],
            'role_id' => Role::where('name', 'HOD')->get()->first()->id
        ])->each(function (User $user, $key) use ($departments, $email, $name) {
            $user->name = $name[$key];
            $user->email = $email[$key] . "@example.com";
            $user->save();
            Department::create(['name' => $departments[$key], 'hod_id' => $user->id]);

            $user->role->fields->each(function (RoleField $rf) use ($user) {
                FilledRoleField::create(['role_field_id' => $rf->id, 'user_id' => $user->id]);
            });
        });

        //employees under some department
        User::factory(40)->create([
            'role_id' => Role::where('name', 'Employee')->get()->first()->id,
            'password' => 'password',
            'carry_forward_starting_balances' => []
        ])->each(function (User $user, $key) {
            $user->update(['email' => "employee{$key}@example.com"]);
            $user->departmentStaffDetail()->save(new DepartmentStaffDetail(['designation' => 'some designation', 'department_id' => Department::inRandomOrder()->first()->id]));

            $user->role->fields->each(function (RoleField $rf) use ($user) {
                FilledRoleField::create(['role_field_id' => $rf->id, 'user_id' => $user->id]);
            });
        });

        Role::where('name', 'Test Case Role')->first()?->delete();

        //LEAVE TYPES
        LeaveType::create(['type' => 'Medical Leave (Employee)', 'requires_doc' => True, 'requires_hod_approval' => True, 'balance_type' => 'Number', 'monthly_balance_accumulation_rate' => 1.5, 'carry_forward' => False, 'public_holidays' => "[]"])
            ->approvalStages()->saveMany([
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Human Resource')->first()->id, 'stage' => 1])
            ]);
        LeaveType::create(['type' => 'Casual Leave (Employee)', 'requires_doc' => False, 'requires_hod_approval' => False, 'balance_type' => 'Number', 'monthly_balance_accumulation_rate' => 1.5, 'carry_forward' => False, 'public_holidays' => "[]"])
            ->approvalStages()->saveMany([
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Human Resource')->first()->id, 'stage' => 1]),
            ]);
        LeaveType::create(['type' => 'Earned Leave (Employee)', 'requires_doc' => False, 'requires_hod_approval' => False, 'balance_type' => 'Number', 'monthly_balance_accumulation_rate' => 1.5, 'carry_forward' => False, 'public_holidays' => "[]"])
            ->approvalStages()->saveMany([
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Human Resource')->first()->id, 'stage' => 1]),
            ]);
        LeaveType::create(['type' => 'LWP (Employee)', 'requires_doc' => False, 'requires_hod_approval' => False, 'balance_type' => 'Unlimited', 'carry_forward' => False, 'public_holidays' => "[]"])
            ->approvalStages()->saveMany([
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Human Resource')->first()->id, 'stage' => 1]),
            ]);
        LeaveType::create([
            'type' => 'PH (Employee)', 'requires_doc' => False, 'requires_hod_approval' => False, 'balance_type' => 'Public Holiday', 'monthly_balance_accumulation_rate' => 1.5, 'carry_forward' => False,
            'public_holidays' => [
                ["name" => "Nyilo", "date" => "2024-05-02"],
                ["name" => "Birth Anniversary of His Majesty The King", "date" => "2024-02-21"],
                ["name" => "Zhabdrung Kuchoe", "date" => "2024-04-18"],
                ["name" => "Birth Anniversary of 3rd Druk Gyalpo", "date" => "2024-05-02"],
                ["name" => "Lord Buddha's Parinirvana", "date" => "2024-05-23"],
                ["name" => "First Sermon of Lord Buddha", "date" => "2024-07-10"],
                ["name" => "Blessed Rainy Day", "date" => "2024-09-23"],
                ["name" => "Dassain", "date" => "2024-10-22"],
                ["name" => "Birth Anniversary of 4th Druk Gyalpo", "date" => "2024-11-11"],
                ["name" => "National Day", "date" => "2024-12-17"]
            ]
        ])
            ->approvalStages()->saveMany([
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Human Resource')->first()->id, 'stage' => 1]),
            ]);

        LeaveType::create(['type' => 'Medical Leave (HOD)', 'requires_doc' => True, 'requires_hod_approval' => False, 'balance_type' => 'Number', 'monthly_balance_accumulation_rate' => 1.5, 'carry_forward' => False, 'public_holidays' => "[]"])
            ->approvalStages()->saveMany([
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Executive Manager')->first()->id, 'stage' => 1]),
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Human Resource')->first()->id, 'stage' => 2])
            ]);
        LeaveType::create(['type' => 'Casual Leave (HOD)', 'requires_doc' => False, 'requires_hod_approval' => False, 'balance_type' => 'Number', 'monthly_balance_accumulation_rate' => 1.5, 'carry_forward' => False, 'public_holidays' => "[]"])
            ->approvalStages()->saveMany([
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Executive Manager')->first()->id, 'stage' => 1]),
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Human Resource')->first()->id, 'stage' => 2])
            ]);

        LeaveType::create(['type' => 'Earned Leave (HOD)', 'requires_doc' => False, 'requires_hod_approval' => False, 'balance_type' => 'Number', 'monthly_balance_accumulation_rate' => 1.5, 'carry_forward' => True, 'public_holidays' => "[]"])
            ->approvalStages()->saveMany([
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Executive Manager')->first()->id, 'stage' => 1]),
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Human Resource')->first()->id, 'stage' => 2])
            ]);

        LeaveType::create(['type' => 'LWP (HOD)', 'requires_doc' => False, 'requires_hod_approval' => False, 'balance_type' => 'Unlimited', 'carry_forward' => False, 'public_holidays' => "[]"])
            ->approvalStages()->saveMany([
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Executive Manager')->first()->id, 'stage' => 1]),
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Human Resource')->first()->id, 'stage' => 2])
            ]);

        LeaveType::create([
            'type' => 'PH (HOD)', 'requires_doc' => False, 'requires_hod_approval' => False, 'balance_type' => 'Public Holiday', 'monthly_balance_accumulation_rate' => 1.5, 'carry_forward' => False,
            'public_holidays' => [
                ["name" => "Nyilo", "date" => "2024-05-02"],
                ["name" => "Birth Anniversary of His Majesty The King", "date" => "2024-02-21"],
                ["name" => "Zhabdrung Kuchoe", "date" => "2024-04-18"],
                ["name" => "Birth Anniversary of 3rd Druk Gyalpo", "date" => "2024-05-02"],
                ["name" => "Lord Buddha's Parinirvana", "date" => "2024-05-23"],
                ["name" => "First Sermon of Lord Buddha", "date" => "2024-07-10"],
                ["name" => "Blessed Rainy Day", "date" => "2024-09-23"],
                ["name" => "Dassain", "date" => "2024-10-22"],
                ["name" => "Birth Anniversary of 4th Druk Gyalpo", "date" => "2024-11-11"],
                ["name" => "National Day", "date" => "2024-12-17"]
            ]
        ])
            ->approvalStages()->saveMany([
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Executive Manager')->first()->id, 'stage' => 1]),
                new ApprovalStage(['user_id' => User::whereRelation('role', 'name', 'Human Resource')->first()->id, 'stage' => 2])
            ]);

        //LEAVE ELIGIBLITY
        Role::where('name', 'ICT')->first()->leaveTypes()->sync(
            LeaveType::whereIn('type', ['Medical Leave (HOD)', 'Casual Leave (HOD)', 'Earned Leave (HOD)', 'PH (HOD)', 'BL (HOD)', 'LWP (HOD)'])->get()->map(fn ($lt) => $lt->id)
        );

        Role::where('name', 'Human Resource')->first()->leaveTypes()->sync(
            LeaveType::whereIn('type', ['Medical Leave (HOD)', 'Casual Leave (HOD)', 'Earned Leave (HOD)', 'PH (HOD)', 'BL (HOD)', 'LWP (HOD)'])->get()->map(fn ($lt) => $lt->id)
        );

        Role::where('name', 'HOD')->first()->leaveTypes()->sync(
            LeaveType::whereIn('type', ['Medical Leave (HOD)', 'Casual Leave (HOD)', 'Earned Leave (HOD)', 'PH (HOD)', 'BL (HOD)', 'LWP (HOD)'])->get()->map(fn ($lt) => $lt->id)
        );

        Role::where('name', 'Employee')->first()->leaveTypes()->sync(
            LeaveType::whereIn('type', ['Medical Leave (Employee)', 'Casual Leave (Employee)', 'Earned Leave (Employee)', 'LWP (Employee)', 'PH (Employee)'])->get()->map(fn ($lt) => $lt->id)
        );

        //ISSUANCE
        Role::whereIn('name', ['HOD', 'Human Resource', 'Employee', 'ICT'])->get()->each(function (Role $role) {
            $hk = Department::where('name', 'House Keeping (HK)')->first()->hod;

            $role->fields()->where('issuable', true)->get()->each(function (RoleField $rf) use ($hk) {
                $rf->issuers()->attach($hk->id);
            });
        });

        //ROSTER AND SHIFTS
        if (true) {
            Shift::create([
                'name' => 'Morning Shift',
                'start_time' => '05:00',
                'end_time' => '12:00',
                'department_id' => Department::where('name', 'Administration and Finance (AFD)')->first()->id,
                'color' => '#ff9029'
            ]);
            Shift::create([
                'name' => 'Afternoon Shift',
                'start_time' => '12:00',
                'end_time' => '20:00',
                'department_id' => Department::where('name', 'Administration and Finance (AFD)')->first()->id,
                'color' => '#ff9029'
            ]);
            Shift::create([
                'name' => 'Evening Shift',
                'start_time' => '20:00',
                'end_time' => '05:00',
                'department_id' => Department::where('name', 'Administration and Finance (AFD)')->first()->id,
                'color' => '#ff9029'
            ]);

            Shift::create([
                'name' => 'Morning Shift',
                'start_time' => '05:00',
                'end_time' => '12:00',
                'department_id' => Department::where('name', 'House Keeping (HK)')->first()->id,
                'color' => '#ff9029'
            ]);
            Shift::create([
                'name' => 'Afternoon Shift',
                'start_time' => '12:00',
                'end_time' => '20:00',
                'department_id' => Department::where('name', 'House Keeping (HK)')->first()->id,
                'color' => '#ff9029'
            ]);
            Shift::create([
                'name' => 'Evening Shift',
                'start_time' => '20:00',
                'end_time' => '05:00',
                'department_id' => Department::where('name', 'House Keeping (HK)')->first()->id,
                'color' => '#ff9029'
            ]);

            Shift::create([
                'name' => 'Morning Shift',
                'start_time' => '05:00',
                'end_time' => '12:00',
                'department_id' => Department::where('name', "Food & Beverage Service (FNB)")->first()->id,
                'color' => '#ff9029'
            ]);
            Shift::create([
                'name' => 'Afternoon Shift',
                'start_time' => '12:00',
                'end_time' => '20:00',
                'department_id' => Department::where('name', "Food & Beverage Service (FNB)")->first()->id,
                'color' => '#ff9029'
            ]);
            Shift::create([
                'name' => 'Evening Shift',
                'start_time' => '20:00',
                'end_time' => '05:00',
                'department_id' => Department::where('name', "Food & Beverage Service (FNB)")->first()->id,
                'color' => '#ff9029'
            ]);

            Shift::create([
                'name' => 'Morning Shift',
                'start_time' => '05:00',
                'end_time' => '12:00',
                'department_id' => Department::where('name', "Food & Beverage Production (FBP)")->first()->id,
                'color' => '#ff9029'
            ]);
            Shift::create([
                'name' => 'Afternoon Shift',
                'start_time' => '12:00',
                'end_time' => '20:00',
                'department_id' => Department::where('name', "Food & Beverage Production (FBP)")->first()->id,
                'color' => '#ff9029'
            ]);
            Shift::create([
                'name' => 'Evening Shift',
                'start_time' => '20:00',
                'end_time' => '05:00',
                'department_id' => Department::where('name', "Food & Beverage Production (FBP)")->first()->id,
                'color' => '#ff9029'
            ]);

            Shift::create([
                'name' => 'Morning Shift',
                'start_time' => '05:00',
                'end_time' => '12:00',
                'department_id' => Department::where('name', "Front Office (FO)")->first()->id,
                'color' => '#ff9029'
            ]);
            Shift::create([
                'name' => 'Afternoon Shift',
                'start_time' => '12:00',
                'end_time' => '20:00',
                'department_id' => Department::where('name', "Front Office (FO)")->first()->id,
                'color' => '#ff9029'
            ]);
            Shift::create([
                'name' => 'Evening Shift',
                'start_time' => '20:00',
                'end_time' => '05:00',
                'department_id' => Department::where('name', "Front Office (FO)")->first()->id,
                'color' => '#ff9029'
            ]);
        }

        // FORM TEMPLATES
        // Test Form Template
        if (true) {
            $formTemplate = FormTemplate::create(['name' => 'Test Form Template']);

            $field = Field::create([
                'name' => 'Name:',
                'type' => 'Text',
                'required' => false,
                'index' => 0
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'CID:',
                'type' => 'Number',
                'required' => true,
                'index' => 1
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Description',
                'type' => 'Text Area',
                'required' => false,
                'index' => 2 // Changed index from 1 to 3 to maintain proper ordering
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Date',
                'type' => 'Date',
                'required' => true,
                'index' => 3 // Changed index from 3 to 4 to maintain proper ordering
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'File',
                'type' => 'File', // Changed type from 'number' to 'string'
                'required' => true,
                'index' => 4 // Changed index from 4 to 5 to maintain proper ordering
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Radio Button',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 5
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Checkbox',
                'type' => 'Checkbox',
                'required' => true,
                'index' => 6
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);
        }

        // Intern Evaluation Form Template
        if (true) {
            $formTemplate = FormTemplate::create(['name' => 'Intern Evaluation Form Template']);

            $field = Field::create([
                'name' => 'Intern’s Name:',
                'type' => 'Text',
                'required' => false,
                'index' => 0
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'CID:',
                'type' => 'Number',
                'required' => true,
                'index' => 1
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Department:',
                'type' => 'Text',
                'required' => false,
                'index' => 2 // Changed index from 1 to 3 to maintain proper ordering
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Position:',
                'type' => 'Text',
                'required' => true,
                'index' => 3 // Changed index from 3 to 4 to maintain proper ordering
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Contact Number:',
                'type' => 'Text', // Changed type from 'number' to 'string'
                'required' => true,
                'index' => 4 // Changed index from 4 to 5 to maintain proper ordering
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Email:',
                'type' => 'Text',
                'required' => false,
                'index' => 5 // Changed index from 5 to 6 to maintain proper ordering
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Evaluation Period starting date:',
                'type' => 'Date',
                'required' => true,
                'index' => 6
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Evaluation Period end date:',
                'type' => 'Date',
                'required' => true,
                'index' => 7
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Evaluator',
                'type' => 'Title',
                'required' => true,
                'index' => 8
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "Manager's Name:",
                'type' => 'Text',
                'required' => true,
                'index' => 9
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "Position",
                'type' => 'Text',
                'required' => true,
                'index' => 10
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "Department",
                'type' => 'Text',
                'required' => true,
                'index' => 11
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "The questions on this evaluation align with standard Career Readiness Competencies critical for a successful transition from School/ College/ Institution to the workplace.",
                'type' => 'Description',
                'required' => true,
                'index' => 12
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "This evaluation is intended to help interns learn more about their strengths and the areas they might need to focus on in order to be maximally successful in the workforce after graduation. Managers are encouraged to complete the form and review it with their intern in a scheduled one-to-one meeting.",
                'type' => 'Description',
                'required' => true,
                'index' => 13
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "Please rate your intern in the following areas by checking (V) in the appropriate boxes:",
                'type' => 'Title',
                'required' => true,
                'index' => 14
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => "1. Attendance and Punctuality",
                'type' => 'Title',
                'required' => true,
                'index' => 15
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => ' Arrives on time consistently',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 16
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Adheres to the assigned schedule',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 17
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Notifies supervisor in advance of any absences or tardiness',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 18
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "2. Work Quality",
                'type' => 'Title',
                'required' => true,
                'index' => 19
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Demonstrates attention to detail in tasks',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 20
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Demonstrates attention to detail in tasks',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 21
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Shows sincere interest in understanding the organization, their role and their assigned tasks',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 22
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Produces work that meets or exceeds expectations',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 23
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Shows improvement over the course of the internship',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 24
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => "3. Communication Skills",
                'type' => 'Title',
                'required' => true,
                'index' => 25
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Communicates effectively with colleagues and supervisors',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 26
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Listens actively and asks, clarifying questions when necessary and when unsure how to proceed with their tasks',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 27
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Conveys ideas clearly and professionally, both verbally and in writing',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 28
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "4. Teamwork and Collaboration",
                'type' => 'Title',
                'required' => true,
                'index' => 29
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Works well with others in a team environment',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 30
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Collaborates effectively to collaborative work; shares knowledge and resources with colleagues',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 31
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Contributes positively to team morale and growth',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 32
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "5. Initiative and Problem-Solving",
                'type' => 'Title',
                'required' => true,
                'index' => 33
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Takes initiative in completing tasks without constant supervision',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 34
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '    Identifies and addresses issues proactively',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 35
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '   Takes initiative and seeks opportunities where they can contribute further besides their assigned tasks',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 36
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '  Offers creative solutions to challenges encountered',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 37
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "6. Professionalism/Work Ethic",
                'type' => 'Title',
                'required' => true,
                'index' => 38
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '  Demonstrates respect for organizational staff, policies and norms',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 39
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Organizes and prioritizes work, manages time, and sees tasks through from start to finish ',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 40
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Accepts constructive feedback fromothers and is able to learn frommistakes ',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 41
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Represents the hotel in a positive manner to guests and visitors ',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 42
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Strongly Agree',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Agree',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Disagree',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Strongly Disagree',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'N/A',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "Overall",
                'type' => 'Title',
                'required' => true,
                'index' => 43
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "What do you perceive to be the intern's greatest strenghts that could be assets to The Pema?",
                'type' => 'Text Area',
                'required' => true,
                'index' => 44
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "What areas of growth could improve the intern's success in this field(The Pema and the relevant Department)?",
                'type' => 'Text Area',
                'required' => true,
                'index' => 45
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "Would you recommend this intern for future employment at The Pema? Please explain.",
                'type' => 'Text Area',
                'required' => true,
                'index' => 46
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);
        }

        // PERFORMANCE EVALUATION
        if (true) {
            $formTemplate = FormTemplate::create(['name' => 'Performance Evaluation Form Template']);

            $field = Field::create([
                'name' => 'Date of Appraisal',
                'type' => 'Date',
                'required' => true,
                'index' => 0
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Date of Last Appraisal',
                'type' => 'Date',
                'required' => true,
                'index' => 1
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => ' Type of Appraisal',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 2
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'End of Training',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'End of Probation',
                    'index' => 1
                ]),
                new Option([
                    'value' => 'Annual',
                    'index' => 2
                ]),
                new Option([
                    'value' => 'Promotion',
                    'index' => 3
                ]),
                new Option([
                    'value' => 'Other',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Staff Details',
                'type' => 'Title',
                'required' => true,
                'index' => 3
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Name',
                'type' => 'Text',
                'required' => true,
                'index' => 4
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Position',
                'type' => 'Text',
                'required' => true,
                'index' => 5
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'CID',
                'type' => 'Number',
                'required' => true,
                'index' => 6
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Department',
                'type' => 'Text',
                'required' => true,
                'index' => 7
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Date of joining THE PEMA BY REALM',
                'type' => 'Date',
                'required' => true,
                'index' => 8
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Appraisal period (From)',
                'type' => 'Date',
                'required' => true,
                'index' => 9
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Appraisal period (To)',
                'type' => 'Date',
                'required' => true,
                'index' => 10
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Evaluator Name',
                'type' => 'Text',
                'required' => true,
                'index' => 11
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Evaluator Position',
                'type' => 'Text',
                'required' => true,
                'index' => 12
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'HR MANAGER, EXECUTIVE, COORDINATOR',
                'type' => 'Text',
                'required' => true,
                'index' => 13
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'WORKING PERFORMANCE RECORDS OR OBSERVATIONS (TO BE FILLED BY HR)',
                'type' => 'Title',
                'required' => true,
                'index' => 14
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'From',
                'type' => 'Date',
                'required' => true,
                'index' => 15
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'To',
                'type' => 'Date',
                'required' => true,
                'index' => 16
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'ABSENT',
                'type' => 'Description',
                'required' => true,
                'index' => 17
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'QTY',
                'type' => 'Number',
                'required' => true,
                'index' => 18
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'REMARKS',
                'type' => 'Text Area',
                'required' => true,
                'index' => 19
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'VERBAL WARNING',
                'type' => 'Description',
                'required' => true,
                'index' => 20
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'QTY',
                'type' => 'Number',
                'required' => true,
                'index' => 21
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'REMARKS',
                'type' => 'Text Area',
                'required' => true,
                'index' => 22
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'WRITTEN WARNING',
                'type' => 'Description',
                'required' => true,
                'index' => 23
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'QTY',
                'type' => 'Number',
                'required' => true,
                'index' => 24
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'REMARKS',
                'type' => 'Text Area',
                'required' => true,
                'index' => 25
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'FINAL WARNING',
                'type' => 'Description',
                'required' => true,
                'index' => 26
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'QTY',
                'type' => 'Number',
                'required' => true,
                'index' => 27
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'REMARKS',
                'type' => 'Text Area',
                'required' => true,
                'index' => 28
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'SALARY DEDUCTION',
                'type' => 'Description',
                'required' => true,
                'index' => 29
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'QTY',
                'type' => 'Number',
                'required' => true,
                'index' => 30
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'REMARKS',
                'type' => 'Text Area',
                'required' => true,
                'index' => 31
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'OBSERVATIONS',
                'type' => 'Description',
                'required' => true,
                'index' => 32
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'QTY',
                'type' => 'Number',
                'required' => true,
                'index' => 33
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'REMARKS',
                'type' => 'Text Area',
                'required' => true,
                'index' => 34
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '*OBSERVATIONS - this includes recorded observations made of staff when rules have not been adhered to; prior to any verbal, written and final warnings or disciplinary action taken',
                'type' => 'Description',
                'required' => true,
                'index' => 35
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'CRITERIA OR INDICATOR',
                'type' => 'Title',
                'required' => true,
                'index' => 36
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'MARK 5 - GRADE OUTSTANDING - Star performance. Possesses leadership qualities. Fantastic communication skills. Result-oriented. Inspirational and motivates others towards high performance.',
                'type' => 'Description',
                'required' => true,
                'index' => 37
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'MARK 4 - EXCEEDS EXPECTATION - Consistently overperforming. Takes initiative to deliver more than asked for and provides assistance without being told. Provides suggestions for improvement. Consistent willingness to accept extra work.',
                'type' => 'Description',
                'required' => true,
                'index' => 38
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'MARK 3 - MEETS EXPECTATION - Consistently meets the expectations of the performance objective/job responsibilities. Works independently. Cooperative attitude. Possesses full knowledge of job functions/duties.',
                'type' => 'Description',
                'required' => true,
                'index' => 39
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'MARK 2 - NEEDS IMPROVEMENT - Partially achieved expected results on the performance objective. Work results are inconsistent. Requires more supervision. Improvement needed in key areas.',
                'type' => 'Description',
                'required' => true,
                'index' => 40
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'MARK 1 - UNDER PERFORMANCE - Consistently fail to meet expectations and job description requirements. Lack of motivation and initiative. Works at a level of minimum standards. Immediate correction action is necessary.',
                'type' => 'Description',
                'required' => true,
                'index' => 41
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'SELF APPRAISAL RATING(select rating)',
                'type' => 'Title',
                'required' => true,
                'index' => 42
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => "ATTENDANCE OR DISCIPLINE. Reports to work on time and complies with the company's rules and policies. Employee is usually not absent from work for any reason, and is never absent without permission for reasons beyond his/her control. Respectful of peers, supervisors and customers.",
                'type' => 'Radio Button',
                'required' => true,
                'index' => 43
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '5',
                    'index' => 0
                ]),
                new Option([
                    'value' => '4',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '2',
                    'index' => 3
                ]),
                new Option([
                    'value' => '1',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "FLEXIBLITY. Available for overtime/working beyond scheduled hours when called upon to do so or workload requires. Ability to share additional responsibility when required.",
                'type' => 'Radio Button',
                'required' => true,
                'index' => 44
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '5',
                    'index' => 0
                ]),
                new Option([
                    'value' => '4',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '2',
                    'index' => 3
                ]),
                new Option([
                    'value' => '1',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => "JOB KNOWLEDGE. Knowledge and understanding of job responsibilites, expectations and department works. This includes knowledge of methods, processes and standard operating procedures.",
                'type' => 'Radio Button',
                'required' => true,
                'index' => 45
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '5',
                    'index' => 0
                ]),
                new Option([
                    'value' => '4',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '2',
                    'index' => 3
                ]),
                new Option([
                    'value' => '1',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => "QUALITY. Ensures works are completed as per the standards set (quality) and on time. Requires minimum supervision.",
                'type' => 'Radio Button',
                'required' => true,
                'index' => 46
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '5',
                    'index' => 0
                ]),
                new Option([
                    'value' => '4',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '2',
                    'index' => 3
                ]),
                new Option([
                    'value' => '1',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => "ASSET MANAGEMENT. Takes extra care of company property and assets. Shows care to promote proper handling of company property to minimize damage and takes initiative in the mainetnance of assets.",
                'type' => 'Radio Button',
                'required' => true,
                'index' => 47
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '5',
                    'index' => 0
                ]),
                new Option([
                    'value' => '4',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '2',
                    'index' => 3
                ]),
                new Option([
                    'value' => '1',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => "CUSTOMER CARE. Competency on the job and promptness in resolving customer concerns/needs within a reasonable time frame. Successfully completes tasks assigned, meets standards set, with minimum errors and problems.",
                'type' => 'Radio Button',
                'required' => true,
                'index' => 48
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '5',
                    'index' => 0
                ]),
                new Option([
                    'value' => '4',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '2',
                    'index' => 3
                ]),
                new Option([
                    'value' => '1',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => "CREATIVITY. Takes initiative to suggest new ideas to improve work performance. Open to new ideas, and learning capability for newer techniques, use of technology and methods to achieve company goals.",
                'type' => 'Radio Button',
                'required' => true,
                'index' => 49
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '5',
                    'index' => 0
                ]),
                new Option([
                    'value' => '4',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '2',
                    'index' => 3
                ]),
                new Option([
                    'value' => '1',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => "COMMUNICATION. Communicates with clarity with customers and with peers to help support business growth. Conveys information openly and effectively in a timely manner. Is an active listener and promotes effective inter-department communication through sharing of information.",
                'type' => 'Radio Button',
                'required' => true,
                'index' => 50
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '5',
                    'index' => 0
                ]),
                new Option([
                    'value' => '4',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '2',
                    'index' => 3
                ]),
                new Option([
                    'value' => '1',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => "ETHICS/ACCOUNTABILITY. Ethical and moral behavior. Takes responsibility for actions and displays integrity.",
                'type' => 'Radio Button',
                'required' => true,
                'index' => 51
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '5',
                    'index' => 0
                ]),
                new Option([
                    'value' => '4',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '2',
                    'index' => 3
                ]),
                new Option([
                    'value' => '1',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => "Ability to promote and maintain postive and productive relationships with peers, supervisors and customers. Active in promoting cooperation and teamwork setting a good example to peers.",
                'type' => 'Radio Button',
                'required' => true,
                'index' => 52
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '5',
                    'index' => 0
                ]),
                new Option([
                    'value' => '4',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '2',
                    'index' => 3
                ]),
                new Option([
                    'value' => '1',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => "Total Marks",
                'type' => 'Number',
                'required' => true,
                'index' => 53
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => "Average Mark (Total Marks/10)",
                'type' => 'Number',
                'required' => true,
                'index' => 54
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => "Select which is applicable:",
                'type' => 'Radio Button',
                'required' => true,
                'index' => 55
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => 'Average mark > 3',
                    'index' => 0
                ]),
                new Option([
                    'value' => 'Average mark < 3',
                    'index' => 1
                ]),
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);
        }

        // Clearance Certificate form template
        if (true) {
            $formTemplate = FormTemplate::create(['name' => 'Clearance Certificate Form Template']);

            $field = Field::create([
                'name' => 'Housekeeping Manager particular',
                'type' => 'Text Area',
                'required' => true,
                'index' => 0
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Housekeeping Manager remarks',
                'type' => 'Text Area',
                'required' => true,
                'index' => 1
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Front Office Manager particular',
                'type' => 'Text Area',
                'required' => true,
                'index' => 2
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Front Office Manager remarks',
                'type' => 'Text Area',
                'required' => true,
                'index' => 3
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Food &amp; Beverage Service Manager particular',
                'type' => 'Text Area',
                'required' => true,
                'index' => 4
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Food &amp; Beverage Service Manager remarks',
                'type' => 'Text Area',
                'required' => true,
                'index' => 5
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Food &amp; Beverage Production Manager/Executive Chef particular',
                'type' => 'Text Area',
                'required' => true,
                'index' => 6
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Food &amp; Beverage Production Manager/Executive Chef remarks',
                'type' => 'Text Area',
                'required' => true,
                'index' => 7
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);


            $field = Field::create([
                'name' => 'Administration &amp; Finance Manager particular',
                'type' => 'Text Area',
                'required' => true,
                'index' => 8
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Administration &amp; Finance Manager remarks',
                'type' => 'Text Area',
                'required' => true,
                'index' => 9
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);
        }

        //interview appraisal form
        if (True) {
            $formTemplate = FormTemplate::create(['name' => 'Interview Appraisal Form Template']);
            $field = Field::create([
                'name' => 'Candidate Name',
                'type' => 'Text',
                'required' => true,
                'index' => 0
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Position applied for',
                'type' => 'Text',
                'required' => true,
                'index' => 1
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Personal Details',
                'type' => 'Title',
                'required' => false,
                'index' => 2
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Date of Birth',
                'type' => 'Date',
                'required' => true,
                'index' => 3
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Gender',
                'type' => 'Text',
                'required' => true,
                'index' => 4
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Marital Status',
                'type' => 'Text',
                'required' => true,
                'index' => 5
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Current Position',
                'type' => 'Text',
                'required' => true,
                'index' => 6
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Salary Drawn in Nu.',
                'type' => 'Number',
                'required' => true,
                'index' => 7
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'FOR OFFICE USE ONLY, Rating: Below-average-1, Average-2, Good-3, Outstanding-4, and Excellent-5',
                'type' => 'Title',
                'required' => true,
                'index' => 8
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Grooming & Personality',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 9
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '1',
                    'index' => 0
                ]),
                new Option([
                    'value' => '2',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '4',
                    'index' => 3
                ]),
                new Option([
                    'value' => '5',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Communication Skills',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 10
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '1',
                    'index' => 0
                ]),
                new Option([
                    'value' => '2',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '4',
                    'index' => 3
                ]),
                new Option([
                    'value' => '5',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'General Status of Health',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 11
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '1',
                    'index' => 0
                ]),
                new Option([
                    'value' => '2',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '4',
                    'index' => 3
                ]),
                new Option([
                    'value' => '5',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'General Knowledge',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 12
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '1',
                    'index' => 0
                ]),
                new Option([
                    'value' => '2',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '4',
                    'index' => 3
                ]),
                new Option([
                    'value' => '5',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Job Knowledge',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 13
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '1',
                    'index' => 0
                ]),
                new Option([
                    'value' => '2',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '4',
                    'index' => 3
                ]),
                new Option([
                    'value' => '5',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Attitude',
                'type' => 'Radio Button',
                'required' => true,
                'index' => 14
            ]);
            $field->options()->saveMany([
                new Option([
                    'value' => '1',
                    'index' => 0
                ]),
                new Option([
                    'value' => '2',
                    'index' => 1
                ]),
                new Option([
                    'value' => '3',
                    'index' => 2
                ]),
                new Option([
                    'value' => '4',
                    'index' => 3
                ]),
                new Option([
                    'value' => '5',
                    'index' => 4
                ])
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Total Scores',
                'type' => 'Number',
                'required' => true,
                'index' => 15
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Other Observations',
                'type' => 'Text Area',
                'required' => true,
                'index' => 16
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Remarks',
                'type' => 'Text Area',
                'required' => true,
                'index' => 17
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Assessed by',
                'type' => 'Text',
                'required' => true,
                'index' => 18
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Date',
                'type' => 'Date',
                'required' => true,
                'index' => 19
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);
        }

        //staff locker undertaking
        if (True) {
            $formTemplate = FormTemplate::create(['name' => 'Staff Locker Undertaking Form Template']);
            $field = Field::create([
                'name' => 'Date',
                'type' => 'Date',
                'required' => true,
                'index' => 0
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'The HR Manager,',
                'type' => 'Description',
                'required' => true,
                'index' => 1
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'The Pema by Realm',
                'type' => 'Description',
                'required' => false,
                'index' => 2
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Subject: Staff Locker Undertaking',
                'type' => 'Title',
                'required' => true,
                'index' => 3
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'Dear Sir/Madam',
                'type' => 'Description',
                'required' => true,
                'index' => 4
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'I acknowledge that I have taken the possession of Locker No.',
                'type' => 'Text',
                'required' => true,
                'index' => 5
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'which is allotted to me for my use w. e. f',
                'type' => 'Text',
                'required' => true,
                'index' => 6
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => 'I have taken the Locker as mentioned above under the following terms and conditions',
                'type' => 'Title',
                'required' => true,
                'index' => 7
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '1. I am aware that the locker has been given to me solely for the purpose of keeping my uniform and my personal clothing during my duty hours I will not keep food, alcoholic drinks, perishables, inflammable materials and soiled cloths or such articles specifically prohibited',
                'type' => 'Description',
                'required' => true,
                'index' => 8
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '2. The locker shall be kept clean at all times.',
                'type' => 'Description',
                'required' => true,
                'index' => 9
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '3. I understand that the management reserves the right to inspect the locker at any time, at their discretion without my knowledge.',
                'type' => 'Description',
                'required' => true,
                'index' => 10
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '4. I am aware that the locker facilities can be withdrawn at the discretion of the management at any time.',
                'type' => 'Description',
                'required' => true,
                'index' => 11
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '5. I will surrender the locker at the termination of my service or on my leaving the services, before my other dues are cleared.',
                'type' => 'Description',
                'required' => true,
                'index' => 12
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '6. I am aware that the management is not responsible for the safekeeping, theft or loss of any items that is kept in the locker by me. However, management has taken every precautionary step to avoid any incident.',
                'type' => 'Description',
                'required' => true,
                'index' => 13
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '7. I have a copy of this letter for my information and a copy of the same is being placed in my personal file.',
                'type' => 'Description',
                'required' => true,
                'index' => 14
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '8. I am aware that if the locker is damaged due to my negligence, the cost of repair/replacement will be recovered from my benefits.',
                'type' => 'Description',
                'required' => true,
                'index' => 15
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);

            $field = Field::create([
                'name' => '9. I shall share the locker if so required by the management.',
                'type' => 'Description',
                'required' => true,
                'index' => 16
            ]);
            FormField::create(['field_id' => $field->id, 'form_template_id' => $formTemplate->id]);
        }
    }
}
