<?php

namespace Database\Factories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * The current password being used by the factory.
     */
    protected static ?string $password;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $role_id = Role::where('name', 'Test Case Role')->first() ? Role::where('name', 'Test Case Role')->first()->id : Role::create(['name' => 'Test Case Role'])->id;
        return [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => static::$password ??= Hash::make('password'),
            'remember_token' => Str::random(10),
            'gender' => fake()->randomElement(['Male', 'Female']),
            'dob' => fake()->date(),
            'cid' => fake()->numberBetween(99999999, 1000000000),
            'nationality' => fake()->randomElement(['Bhutanese', 'Foreigner']),
            'doj' => fake()->date(),
            'carry_forward_starting_balances' => [],
            'role_id' => $role_id
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}
