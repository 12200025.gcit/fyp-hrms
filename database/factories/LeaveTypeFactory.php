<?php

namespace Database\Factories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class LeaveTypeFactory extends Factory
{
    /**
     * The current password being used by the factory.
     */
    protected static ?string $password;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'type' => 'Test Case Leave Type',
            'requires_doc' => false,
            'requires_hod_approval' => false,
            'balance_type' => 'Fixed',
            'public_holidays' => [],
            'monthly_balance_accumulation_rate' => 0,
            'fixed_balance' => 10,
            'carry_forward' => false
        ];
    }
}
