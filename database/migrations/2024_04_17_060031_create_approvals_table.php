<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('approvals', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->integer('stage');
            $table->string('remarks')->nullable();
            $table->enum('status', ["Pending", "Approved", "Rejected"]);
            
            $table->foreignId('user_id')                                                                                                                    ;

            $table->integer('approvalable_id');
            $table->string('approvalable_type');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('approvals');
    }
};
