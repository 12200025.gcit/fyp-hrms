<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->text('name');
            $table->enum('type', ['Text', 'Checkbox', 'Radio Button', 'Number', 'Date', 'Text Area', 'Dropdown', 'File', 'Title', 'Description']);
            $table->integer('index');
            $table->boolean('required');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('fields');
    }
};
