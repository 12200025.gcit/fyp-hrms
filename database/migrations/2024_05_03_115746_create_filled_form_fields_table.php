<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('filled_form_fields', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->text('value')->nullable();
            $table->date('date_value')->nullable();

            $table->foreignId('form_field_id')->constrained()->onDelete('cascade');
            $table->foreignId('filled_form_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('filled_form_fields');
    }
};
