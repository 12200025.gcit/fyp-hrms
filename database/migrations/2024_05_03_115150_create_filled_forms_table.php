<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('filled_forms', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->boolean('submitted');
            $table->string('verified_remarks')->nullable();

            $table->integer('current_stage')->nullable();
            $table->enum('approval_status', ["Pending", "Approved", "Rejected"])->nullable();

            $table->foreignId('raised_form_id')->constrained()->onDelete('cascade');
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('filled_forms');
    }
};
