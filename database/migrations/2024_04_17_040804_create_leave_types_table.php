<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('leave_types', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('type')->unique();
            $table->boolean('requires_doc');
            $table->boolean('requires_hod_approval');
            $table->enum('balance_type', ['Number', 'Public Holiday', 'Unlimited', 'Fixed']);
            $table->json('public_holidays');
            $table->float('monthly_balance_accumulation_rate')->nullable();
            $table->integer('fixed_balance')->nullable();
            $table->boolean('carry_forward');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('leave_types');
    }
};
