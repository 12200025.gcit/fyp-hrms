<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('leave_applications', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->date('start_date');
            $table->date('end_date');
            $table->integer('current_stage')->nullable();
            $table->string('description');
            $table->boolean('requires_doc');
            $table->json('doc_approvals')->nullable();
            $table->json('public_holidays');
            $table->enum('approval_status', ["Pending", "Approved", "Rejected"]);

            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->foreignId('leave_type_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('leave_applications');
    }
};
