<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('field_files', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('filename');

            $table->integer('field_fileable_id');
            $table->string('field_fileable_type');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('field_files');
    }
};
