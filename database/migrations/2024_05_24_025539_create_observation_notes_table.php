<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('observation_notes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->enum('type', ['Appreciation Note', 'Incident Report']);
            $table->string('note');

            $table->unsignedBigInteger('observer_id');
            $table->unsignedBigInteger('subject_id');

            $table->foreign('observer_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('subject_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('observation_notes');
    }
};
