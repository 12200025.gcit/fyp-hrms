<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name')->unique();
            $table->string('description')->nullable();

            $table->boolean('create_role')->default(false);
            $table->boolean('read_role')->default(false);
            $table->boolean('update_role')->default(false);
            $table->boolean('delete_role')->default(false);

            $table->boolean('create_user')->default(false);
            $table->boolean('read_user')->default(false);
            $table->boolean('update_user')->default(false);
            $table->boolean('delete_user')->default(false);

            $table->boolean('create_leave_type')->default(false);
            $table->boolean('read_leave_type')->default(false);
            $table->boolean('update_leave_type')->default(false);
            $table->boolean('delete_leave_type')->default(false);

            $table->boolean('create_department')->default(false);
            $table->boolean('read_department')->default(false);
            $table->boolean('update_department')->default(false);
            $table->boolean('delete_department')->default(false);

            $table->boolean('create_form_template')->default(false);
            $table->boolean('read_form_template')->default(false);
            $table->boolean('update_form_template')->default(false);
            $table->boolean('delete_form_template')->default(false);

            $table->boolean('roster_eligibility')->default(false);
            $table->boolean('observer_eligibility')->default(false);
            $table->boolean('performance_eligibility')->default(false);
            $table->boolean('view_overall_roster')->default(false);
            $table->boolean('manage_overall_roster')->default(false);
            $table->boolean('manage_department_shift')->default(false);
            $table->boolean('admin_dashboard_eligibility')->default(false);
            $table->boolean('view_attendance')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('roles');
    }
};
