<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('alerts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->enum('alert_period', ["Twice a Day", "Daily", "Weekly", "Monthly"]);
            $table->string('message');
            $table->string('action_url');

            $table->integer('alertable_id');
            $table->string('alertable_type');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('alerts');
    }
};
