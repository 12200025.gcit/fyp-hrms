<!-- resources/views/departments/edit.blade.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Department</title>
</head>
<body>
    <h1>Edit Department</h1>

    <!-- Edit Department Form -->
    <form action="{{ route('departments.update', $department->id) }}" method="post">
        @csrf
        @method('PUT')
        <label for="name">Name:</label>
        <input type="text" name="name" id="name" value="{{ $department->name }}" required>
        <label for="description">Description:</label>
        <input type="text" name="description" id="description" value="{{ $department->description }}">
        <button type="submit">Update Department</button>
    </form>
</body>
</html>
