<!-- resources/views/rosters/create.blade.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create New Roster</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <h1>Create New Shifts</h1>

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <h2>Shifts</h2>
        <ul>
            @foreach ($shifts as $shift)
                <li>{{ $shift->name }} - Start: {{ $shift->start_time }}, End: {{ $shift->end_time }}</li>
            @endforeach
        </ul>
            <h2>Create New Shift</h2>
            <form action="{{ route('shifts.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" id="name" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="start_time">Start Time:</label>
                    <input type="time" name="start_time" id="start_time" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="end_time">End Time:</label>
                    <input type="time" name="end_time" id="end_time" class="form-control" required>
                </div>
                <button type="submit" class="btn btn-primary">Create Shift</button>
            </form>
    

        <h2>Rosters</h2>
        <ul>
            @foreach ($rosters as $roster)
                <li>Date: {{ $roster->date }}, Department: {{ $roster->department->name }}</li>
            @endforeach
        </ul>
        <form action="{{ route('rosters.store') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="department_id">Department:</label>
                <select name="department_id" id="department_id" class="form-control">
                    @foreach ($departments as $department)
                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="date">Date:</label>
                <input type="date" name="date" id="date" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary">Create Roster</button>
        </form>
    </div>

    <div class="container">
        <h1>Assign Shifts</h1>

       

        <hr>

        <h2>Create New Roster</h2>
        <form action="{{ route('assign_shifts.store') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="shift_id">Shift:</label>
                <select name="shift_id" id="shift_id" class="form-control">
                    @foreach ($shifts as $shift)
                        <option value="{{ $shift->_id }}">{{ $shift->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="department_id">Department:</label>
                <select name="department_id" id="department_id" class="form-control">
                    @foreach ($departments as $department)
                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="user_id">User:</label>
                <select name="user_id" id="user_id" class="form-control">
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="date">Date:</label>
                <input type="date" name="date" id="date" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary">Assign Shift</button>
        </form>
    </div>

    

    <!-- Bootstrap JS (optional, only required if you need Bootstrap features) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.3/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
