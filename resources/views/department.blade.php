<!-- resources/views/departments/index.blade.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Departments</title>
</head>
<body>
    <h1>Departments</h1>

    <!-- Add Department Form -->
    <form action="{{ route('departments.store') }}" method="post">
        @csrf
        <label for="name">Name:</label>
        <input type="text" name="name" id="name" required>
        <label for="description">Description:</label>
        <input type="text" name="description" id="description">
        <button type="submit">Add Department</button>
    </form>

    <!-- Departments List -->
    <ul>
        @foreach ($departments as $department)
            <li>
                {{ $department->name }} - {{ $department->description }}
                <form action="{{ route('departments.destroy', $department->id) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Delete</button>
                </form>
                <a href="{{ route('departments.edit', $department->id) }}">Edit</a>
            </li>
        @endforeach
    </ul>
</body>
</html>
