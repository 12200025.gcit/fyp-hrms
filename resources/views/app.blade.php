<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Human Resource Management System</title>
        @vite('resources/js/app.js')
        @inertiaHead
    </head>
    <body id="body" class="bg-red-100 text-xl text-gray-800">
    @inertia
    </body>
</html>
