<!DOCTYPE html>
<html>

<head>
    <title>User Details PDF</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .profile-pic {
            width: 150px;
            height: 150px;
            border-radius: 50%;
            object-fit: cover;
        }

        .page-break {
            page-break-after: always;
        }

        .box {
            border: 1px solid #a8a8a8;
            padding: 10px;
            min-height: 100px;
        }

        .card-header h4 {
            margin-bottom: 0;
        }
    </style>
</head>

<body>
    <div class="container mt-4">
        <div class="text-center mb-4">
            <h1>{{ $user->role->name }}: {{ $user->name }}</h1>
        </div>
        <div class="text-center mb-4">
            {{-- <img src="{{ $user->profilePicture ? $user->profilePicture->src : public_path('user.jpeg') }}"
                alt="Profile Picture" class="profile-pic mb-2"> --}}
            <p><strong>{{ $user->name }}</strong></p>
            <p>{{ $user->role->name }}</p>
            <div class="page-break"></div>
        </div>

        <div class="card mb-4">
            <div class="card-header">
                <h4>Basic Info</h4>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <tr>
                        <th>Name</th>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <th>CID</th>
                        <td>{{ $user->cid }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <th>DOJ</th>
                        <td>{{ $user->doj }}</td>
                    </tr>
                    <tr>
                        <th>Role Type</th>
                        <td>{{ $user->role->name }}</td>
                    </tr>
                    @if ($user->department)
                        <tr>
                            <th>Department Head For</th>
                            <td>{{ $user->department->name }}</td>
                        </tr>
                    @endif
                    @if ($user->departmentStaffDetail)
                        <tr>
                            <th>Department</th>
                            <td>{{ $user->departmentStaffDetail->department->name }}</td>
                        </tr>
                        <tr>
                            <th>Designation</th>
                            <td>{{ $user->departmentStaffDetail->designation }}</td>
                        </tr>
                    @endif
                </table>
            </div>
            <div class="page-break"></div>
        </div>

        <div class="card mb-4">
            <div class="card-header">
                <h4>{{ $user['role']['name'] }} Details</h4>
            </div>
            <div class="card-body">
                @foreach ($user->filledRoleFields->sortBy(fn($frf) => $frf->roleField->field->index) as $frf)
                    @if ($frf->roleField->field->type !== 'File')
                        <div class="mb-3">
                            <span class="font-weight-bold">{{ $frf->roleField->field->name }}:</span>
                            @switch($frf->roleField->field->type)
                                @case('Checkbox')
                                    {{ implode(', ', $frf->selected_options->pluck('value')->toArray()) }}
                                @break
                                @case('Text Area')
                                    <div class="box">{{ $frf->value }}</div>
                                @break
                                @case('Date')
                                    {{ $frf->value }} <i class="text-gray-500 fa-solid fa-calendar"></i> {{ $frf->date_value }}
                                @break
                                @default
                                    {{ $frf->value }}
                            @endswitch
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="page-break"></div>
        </div>

        <div class="card mb-4">
            <div class="card-header">
                <h4>Leave Balance</h4>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    @foreach ($user->balances as $balance)
                        <tr>
                            <th>{{ $balance['leave_type']['type'] }}</th>
                            <td>{{ $balance['balance'] }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="page-break"></div>
        </div>

        <div class="card mb-4">
            <div class="card-header">
                <h4>Documents</h4>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    @foreach ($user->filledRoleFields->sortBy(fn($frf) => $frf->roleField->field->index) as $frf)
                        @if ($frf->roleField->field->type === 'File')
                            <tr>
                                <th>{{ $frf->roleField->field->name }}</th>
                                <td>
                                    @if ($frf->file)
                                        <span class="italic">Available</span>
                                    @else
                                        <span class="italic">Empty</span>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>
            <div class="page-break"></div>
        </div>

        <div class="card mb-4">
            <div class="card-header">
                <h4>Filled Forms</h4>
            </div>
            <div class="card-body">
                @foreach ($user->filledForms->where('submitted', true) as $filledForm)
                    <div class="box mb-3">
                        <div class="d-md-flex justify-content-between">
                            <div class="text-muted font-weight-bold">
                                {{ $filledForm['raisedForm']['name'] }}
                            </div>
                            <div class="text-muted">
                                <i class="fas fa-clock me-2"></i>Submitted on {{ \Carbon\Carbon::parse($filledForm['raisedForm']['updated_at'])->toDayDateTimeString() }}
                            </div>
                        </div>
                        <div class="mt-3">
                            @foreach ($filledForm->filledFormFields->sortBy(fn($frf) => $frf->formField->field->index) as $fff)
                                <div class="mb-2">
                                    @if ($fff['formField']['field']['type'] === 'Title')
                                        <label class="text-muted font-italic">{{ $fff['formField']['field']['name'] }}</label>
                                    @elseif ($fff['formField']['field']['type'] === 'Description')
                                        <label class="text-muted">{{ $fff['formField']['field']['name'] }}</label>
                                    @else
                                        <div>
                                            <span class="font-weight-bold">{{ $fff['formField']['field']['name'] }}:</span>
                                            @if ($fff['formField']['field']['required'])
                                                <span class="text-warning">*</span>
                                            @endif
                                            <div>
                                                @if ($fff['formField']['field']['type'] === 'Text')
                                                    <input type="text" class="form-control-plaintext" value="{{ $fff['value'] }}" readonly>
                                                @elseif ($fff['formField']['field']['type'] === 'Number')
                                                    <input type="number" class="form-control-plaintext" value="{{ $fff['value'] }}" readonly>
                                                @elseif ($fff['formField']['field']['type'] === 'Date')
                                                    <input type="date" class="form-control-plaintext" value="{{ $fff['date_value'] }}" readonly>
                                                @elseif ($fff['formField']['field']['type'] === 'File')
                                                    @if ($fff['file'])
                                                        <a href="{{ $fff['file']['src'] }}" class="d-block font-italic text-muted">
                                                            <i class="fas fa-download"></i> Download
                                                        </a>
                                                    @else
                                                        <span class="font-italic">Empty</span>
                                                    @endif
                                                @elseif ($fff['formField']['field']['type'] === 'Text Area')
                                                    <textarea class="form-control-plaintext" readonly>{{ $fff['value'] }}</textarea>
                                                @elseif ($fff['formField']['field']['type'] === 'Checkbox')
                                                    @foreach ($fff['formField']['field']['options'] as $option)
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" value="{{ $option['value'] }}" @if (in_array($option['value'], array_column($fff['selected_options'], 'value'))) checked @endif disabled>
                                                            <label class="form-check-label">{{ $option['value'] }}</label>
                                                        </div>
                                                    @endforeach
                                                @elseif ($fff['formField']['field']['type'] === 'Radio Button')
                                                    @foreach ($fff['formField']['field']['options'] as $option)
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="{{ $option['value'] }}" @if ($fff['value'] === $option['value']) checked @endif disabled>
                                                            <label class="form-check-label">{{ $option['value'] }}</label>
                                                        </div>
                                                    @endforeach
                                                @elseif ($fff['formField']['field']['type'] === 'Dropdown')
                                                    <select class="form-control-plaintext" disabled>
                                                        @foreach ($fff['formField']['field']['options'] as $option)
                                                            <option @if ($fff['value'] === $option['value']) selected @endif>{{ $option['value'] }}</option>
                                                        @endforeach
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
                <div class="page-break"></div>
            </div>
        </div>

        <div class="card mb-4">
            <div class="card-header">
                <h4>Appreciation Notes</h4>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    @foreach ($user->observationNotesAsSubject->filter(fn($on) => $on->type === 'Appreciation Note') as $on)
                        <tr>
                            <td>On {{ $on->created_at->format('Y-m-d') }}</td>
                            <th>By {{ $on->observer->name }} ({{ $on->observer->role->name }})</th>
                            <td>{{ $on->note }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="page-break"></div>
        </div>

        <div class="card mb-4">
            <div class="card-header">
                <h4>Incident Reports</h4>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    @foreach ($user->observationNotesAsSubject->filter(fn($on) => $on->type === 'Incident Report') as $on)
                        <tr>
                            <td>On {{ $on->created_at->format('Y-m-d') }}</td>
                            <th>By {{ $on->observer->name }} ({{ $on->observer->role->name }})</th>
                            <td>{{ $on->note }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</body>

</html>
