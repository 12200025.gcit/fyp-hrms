import { createApp, h } from 'vue'
import { createInertiaApp } from '@inertiajs/vue3'
import MainLayout from './Layouts/MainLayout.vue'
import AuthLayout from './Layouts/AuthLayout.vue'
import Toast from "vue-toastification";
import Multiselect from 'vue-multiselect'
import './bootstrap';

import "vue-toastification/dist/index.css";
import '../css/app.css'
import '../css/vue-multiselect.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
  faHouse, faBars, faBell, faXmark, faPeopleGroup, faHandHoldingHand, faClipboard, faGears, faCalendarCheck,
  faPersonCircleCheck, faBuilding, faRunning, faPenToSquare, faRightFromBracket, faBookOpen, faClipboardUser, faFileWaveform, faHandHoldingDroplet,
  faLaptop, faCog, faHouseChimney, faBusinessTime, faCircleUser, faTrash, faEye, faPlusCircle, faCirclePlus, faEllipsis, faFilter, faTrashCan, faCircleArrowDown, faCaretRight, faCaretDown, faEllipsisVertical,
} from '@fortawesome/free-solid-svg-icons'
library.add(faHouse, faBars, faBell, faXmark, faPeopleGroup, faPersonCircleCheck, faBuilding, faCalendarCheck, faHouseChimney, faBookOpen, faClipboardUser, faHandHoldingDroplet, faFileWaveform,
  faRunning, faLaptop, faBusinessTime, faCircleUser, faHandHoldingHand, faClipboard, faGears, faPenToSquare, faEye, faCirclePlus,
  faEllipsis, faFilter, faTrashCan, faCircleArrowDown, faRightFromBracket, faCaretRight, faCaretDown, faEllipsisVertical, faCog)


import { faCakeCandles, faUserGroup, faIdCardClip, faReply, faTableList } from '@fortawesome/free-solid-svg-icons'
library.add(faCakeCandles, faUserGroup, faIdCardClip, faReply, faTableList)

import { faPlus, faCircleChevronLeft, faCircleExclamation, faFile, faGear, faUser, faBriefcase, faStreetView, faTriangleExclamation } from '@fortawesome/free-solid-svg-icons'
library.add(faPlus, faCircleChevronLeft, faCircleExclamation, faFile, faGear, faUser, faBriefcase, faStreetView, faTriangleExclamation)


import {  faChartLine, faCalendarDays,faSortUp,faSort, faSortDown,
  faLessThan, faDownload, faSpinner, faCheck} from '@fortawesome/free-solid-svg-icons'
library.add( faChartLine, faCalendarDays, faSortUp, faSort, faSortDown,
  faLessThan, faDownload, faSpinner, faCheck)

  // faSpinner
  // faCheck
  // faXmark


createInertiaApp({
  resolve: name => {
    const pages = import.meta.glob('./Pages/**/*.vue', { eager: true })
    let page = pages[`./Pages/${name}.vue`]

    if (!name.startsWith('Auth/')) {
      page.default.layout = page.default.layout || MainLayout
    }else {
      page.default.layout = page.default.layout || AuthLayout
    }

    return page
  },
  setup({ el, App, props, plugin }) {
    createApp({ render: () => h(App, props) })
      .use(plugin)
      .use(Toast)
      .component('Icon', FontAwesomeIcon)
      .component('multiselect', Multiselect)
      .mount(el)
  },
  progress: {
    color: '#61ad10',
  },
})